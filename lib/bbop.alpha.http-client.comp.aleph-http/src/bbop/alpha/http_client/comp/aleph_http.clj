(ns bbop.alpha.http-client.comp.aleph-http
  (:require
   [bbop.alpha.http-client.protocol :as protocol]
   [bbop.alpha.core.system :as system]
   [aleph.http :as aleph-http]
   [muuntaja.core :as muuntaja]
   [byte-streams :as bytes]
   [manifold.deferred :as m.defer]
   [manifold.stream :as m.stream])
  (:import [java.io ByteArrayInputStream]
           [manifold.stream BufferedStream]))


(defn build-request [req]
  (let [q? (partial contains? req)]
    (cond-> (select-keys req [:url :query-params :form-params :headers])
      (q? :method) (assoc :request-method (:method req))
      (q? :as)     (assoc :content-type (:as req)
                          :accept (:as req)))))


(def m-instance
  (muuntaja/create
   (assoc-in muuntaja/default-options
             [:formats "application/json" :opts :decode-key-fn]
             true)))


(defn decode-response-body [response]
  (as-> response $
    (assoc $ :body @(m.defer/chain
                     (:body response)
                     #(m.stream/map bytes/to-byte-array %)
                     #(m.stream/reduce conj [] %)
                     bytes/to-string))
    (muuntaja/decode-response-body m-instance $)))


(defn build-response
  [response]
  (-> response
      (select-keys [:status :headers :request-time :response-time])
      (assoc :body (decode-response-body response))))


(defn fetch [opts {:keys [conn-pool]}]
  @(m.defer/chain
    (build-request opts)
    #(merge % {:pool conn-pool})
    #(aleph-http/request %)
    #(build-response %)))


(defrecord AlephHttpClient [conn-pool]
  protocol/IHttpClient
  (-fetch [this request]
    (fetch request {:conn-pool (:conn-pool this)})))


(defn component-start [_]
  (let [pool (aleph-http/connection-pool {:connection-options {:raw-stream? true}})]
    (map->AlephHttpClient {:conn-pool pool})))


(def component
  (system/lifecycle component-start))
