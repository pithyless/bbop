(ns bbop.alpha.kitchensink.preload
  "Due to race-conditions on extending protocols, etc...
  Some namespaces need to be loaded early in the boot process.

  TODO: move logic elsewhere and make this more modular."
  (:require
   [bbop.alpha.core.preload]
   ;; [bbop.alpha.pathom.preload]
   ;; [bbop.alpha.datomic-conn.preload]
   ))
