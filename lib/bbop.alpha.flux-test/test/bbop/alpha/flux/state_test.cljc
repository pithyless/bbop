(ns bbop.alpha.flux.state-test
  "Adapted from codebase https://github.com/nubank/state-flow/"
  (:require
   [bbop.alpha.test.api :as test :refer [specification component behavior assertions is is= is?]]
   [bbop.alpha.flux.state :as state]
   [cats.core :as cats]))


(specification "primitive steps"
  (behavior "return correct values"
    (let [seed 2]
      (is? [2 2]
           (state/run (state/deref) seed))
      (is? [3 2]
           (state/run (state/apply inc) seed))
      (is? [2 3]
           (state/run (state/swap! inc) seed))
      (is? [42 2]
           (state/run (state/return 42) seed))
      (is? [2 3]
           (state/run (state/reset! 3) seed))
      (is? ["elo" 2]
           (state/run (state/call-fn (constantly "elo")) seed))))

  (behavior "all are states"
    (is (state/state?
         (state/deref)))
    (is (state/state?
         (state/apply inc)))
    (is (state/state?
         (state/swap! inc)))
    (is (state/state?
         (state/return 42)))
    (is (state/state?
         (state/reset! 3)))
    (is (state/state?
         (state/call-fn (constantly "elo"))))))


(specification "exception handling"
  (let [double-state (state/swap! * 2)
        failing-step (state/swap! (fn [_] (throw (#?(:clj Throwable. :cljs js/Error.) "My exception"))))
        seed         2]
    (behavior "catches exception and returns a failure"
      (let [[res state] (state/run
                          (cats/>> double-state
                                   double-state
                                   failing-step
                                   double-state)
                          seed)]
        (is (state/failure? res))
        (is= 8 state)))

    (behavior "catches fmap exceptions and returns a failure"
      (let [[res state] (state/run
                          (cats/fmap inc
                                     (cats/>> double-state
                                              double-state
                                              failing-step
                                              double-state))
                          seed)]
        (is (state/failure? res))
        (is= 8 state)))

    (behavior "catches nested fmap exceptions and returns a failure"
      (let [[res state] (state/run
                          (cats/>> (cats/fmap failing-step
                                              (cats/>> double-state
                                                       double-state))
                                   double-state)
                          seed)]
        (is (state/failure? res))
        (is= 8 state)))))
