(ns bbop.alpha.reitit-http.interceptor.cors
  (:require
   [ring.middleware.cors :as middleware.cors]
   [clojure.spec.alpha :as s]))

;; https://github.com/metosin/reitit/issues/236#issuecomment-553133315

(s/def ::allow-origin string?)
(s/def ::allow-methods (s/coll-of keyword? :kind set?))
(s/def ::allow-credentials boolean?)
(s/def ::allow-headers (s/coll-of string? :kind set?))
(s/def ::expose-headers (s/coll-of string? :kind set?))
(s/def ::max-age nat-int?)
(s/def ::access-control
  (s/keys :opt-un [::allow-origin
                   ::allow-methods
                   ::allow-credentials
                   ::allow-headers
                   ::expose-headers
                   ::max-age]))

(s/def ::cors-interceptor
  (s/keys :opt-un [::access-control]))


(defn cors-interceptor []
  {:name    ::cors
   :spec    ::access-control
   :compile (fn [{:keys [access-control]} _]
              (when access-control
                (let [access-control (middleware.cors/normalize-config (mapcat identity access-control))]
                  {:enter (fn cors-interceptor-enter
                            [{:keys [request] :as ctx}]
                            (if (and (middleware.cors/preflight? request)
                                     (middleware.cors/allow-request? request access-control))
                              (let [resp (middleware.cors/add-access-control
                                          request
                                          access-control
                                          middleware.cors/preflight-complete-response)]
                                (assoc ctx
                                       :response resp
                                       :queue nil))
                              ctx))
                   :leave (fn cors-interceptor-leave
                            [{:keys [request response] :as ctx}]
                            (cond-> ctx
                              (and (middleware.cors/origin request)
                                   (middleware.cors/allow-request? request access-control)
                                   response)
                              (assoc :response
                                     (middleware.cors/add-access-control
                                      request
                                      access-control
                                      response))))})))})
