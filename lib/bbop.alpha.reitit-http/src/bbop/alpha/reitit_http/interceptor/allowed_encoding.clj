(ns bbop.alpha.reitit-http.interceptor.allowed-encoding
  (:require
   [bbop.alpha.reitit-http.api :as reitit-http]
   [bbop.alpha.core.log :as log]
   [bbop.alpha.core.contrib :as contrib]
   ))


(defn to-encoding-header [v]
  (case v
    :json         "application/json"
    :transit+json "application/transit+json"
    :edn          "application/edn"
    nil))


(defn allowed-encoding-interceptor-enter [ctx]
  (let [request (:request ctx)
        method  (:request-method request)

        {:strs [accept content-type]} (:headers request)

        {:keys [::reitit-http/allowed-accept
                ::reitit-http/allowed-content-type]}
        (get-in request [:reitit.core/match :result method :data])

        allowed-accept       (set (keep to-encoding-header (last allowed-accept)))
        allowed-content-type (set (keep to-encoding-header (last allowed-content-type)))]
    (contrib/cond
      (not (seq allowed-accept))
      (throw (ex-info "Missing ::reitit-http/allowed-accept" {}))

      (not (seq allowed-content-type))
      (throw (ex-info "Missing ::reitit-http/allowed-content-type" {}))

      (not (contains? allowed-accept accept))
      (assoc ctx
             :response {:status  400
                        :headers {"Content-Type" "text/plain"}
                        :body    (str "Unsupported Accept header: <"
                                      accept
                                      ">. Allowed: <" allowed-accept ">")}
             :queue nil)

      (not (contains? allowed-content-type content-type))
      (assoc ctx
             :response {:status  400
                        :headers {"Content-Type" "text/plain"}
                        :body    (str "Unsupported Content-Type header: <"
                                      content-type
                                      ">. Allowed: <" allowed-content-type ">")}
             :queue nil)

      :else ctx)))


(defn allowed-encoding-interceptor []
  {:name    ::params-mspec-interceptor
   :compile (fn [_ _]
              {:enter allowed-encoding-interceptor-enter})})
