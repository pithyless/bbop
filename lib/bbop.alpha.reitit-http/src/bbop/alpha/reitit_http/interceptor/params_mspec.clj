(ns bbop.alpha.reitit-http.interceptor.params-mspec
  (:require
   [bbop.alpha.reitit-http.api :as reitit-http]
   [bbop.alpha.core.malli :as malli]
   [bbop.alpha.core.mspec :as mspec]
   [bbop.alpha.core.cspec :as cspec]
   [bbop.alpha.core.log :as log]
   [clojure.walk :as walk]
   [clojure.spec.alpha :as s]))


(defn params-mspec-interceptor-enter [ctx]
  (let [request   (:request ctx)
        method    (:request-method request)
        req-specs (get-in request [:reitit.core/match :result
                                   method :data
                                   ::reitit-http/req-specs])
        req-data  (select-keys request [:headers
                                        :body-params
                                        :query-params
                                        :path-params])]
    (if-not req-specs
      ctx
      (let [;; TODO - move this validation to router component-start
            _    (malli/assert
                  [:vector mspec/malli-schema-vector]
                  req-specs)
            spec (reduce malli/merge any? req-specs)]
        (if (malli/validate spec req-data)
          ctx
          (do
            (log/info ::req-does-not-match-spec (malli/explain-msg spec req-data))
            (assoc ctx
                   :response {:status 400
                              :body   (pr-str (malli/explain-msg
                                               spec req-data ))}
                   :queue nil  ;; TODO - is this necessary?
                   )))))))


(defn params-mspec-interceptor []
  {:name    ::params-mspec-interceptor
   :compile (fn [_ _]
              {:enter params-mspec-interceptor-enter})})
