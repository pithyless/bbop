(ns bbop.alpha.reitit-http.api
  (:require
   [bbop.alpha.core.cspec :as cspec]
   [clojure.spec.alpha :as s]
   [bbop.alpha.core.malli :as malli]
   [bbop.alpha.core.mspec :as mspec]))


(s/def ::env
  (s/map-of keyword? any?))

(s/def ::env-specs
  (cspec/vec-of ::mspec/malli-schema-vector))

(s/def ::req-specs
  (cspec/vec-of ::mspec/malli-schema-vector))


(defn route-env [{:keys [:request-method] :as req}]
  (let [req-data  (get-in req [:reitit.core/match :result request-method :data])
        env-specs (:bbop.alpha.reitit-http.api/env-specs req-data)
        _         (malli/assert [:vector mspec/malli-schema-vector] env-specs)
        spec      (reduce malli/merge any? env-specs)
        env       (:bbop.alpha.reitit-http.api/env req-data)
        _         (malli/assert spec env)]
    env))
