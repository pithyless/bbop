(ns bbop.alpha.reitit-http.comp.router
  (:require
   [muuntaja.core :as muuntaja]
   [reitit.dev.pretty :as reitit.pretty]
   [reitit.http.interceptors.exception :as interceptor.exception]
   [reitit.http.interceptors.muuntaja :as interceptor.muuntaja]
   [reitit.http.interceptors.parameters :as interceptor.parameters]
   [reitit.http.interceptors.dev :as interceptor.dev]
   [bbop.alpha.reitit-http.interceptor.cors :as interceptor.cors]
   [bbop.alpha.reitit-http.interceptor.allowed-encoding :as interceptor.allowed-encoding ]
   [bbop.alpha.reitit-http.interceptor.params-mspec :as interceptor.params-mspec]
   [reitit.http]
   ;; [reitit.http.coercion :as reitit-http.coercion]
   ;; [reitit.coercion.spec]
   [reitit.spec]
   [bbop.alpha.core.log :as log]
   [bbop.alpha.core.system :as system]
   [bbop.alpha.reitit-http.api :as reitit-http]
   ;; [spec-tools.spell :as spec.spell]
   [clojure.spec.alpha :as s]))


(s/def ::router-options
  (s/merge :reitit.spec/default-data
           (s/keys :opt [::reitit-http/env
                         ::reitit-http/env-specs
                         ::reitit-http/req-specs])))


(defn ex-handler [message exception request]
  (let [error {:status  500
               :headers {"Content-Type" "text/plain"}
               :body    (pr-str {:message   message
                                 :exception (str exception)
                                 :uri       (:uri request)})}]
    (log/error ::ex-handler {:message   message
                             :type      (str exception)
                             :uri       (:uri request)
                             :exception exception})
    error))


(defn exception-interceptor []
  (interceptor.exception/exception-interceptor
   (merge
    interceptor.exception/default-handlers
    {::interceptor.exception/default (partial ex-handler "default")})))


(def m-instance
  (muuntaja/create muuntaja/default-options))


(defn router-options [env]
  (let [interceptors [(interceptor.cors/cors-interceptor)
                      (exception-interceptor)
                      (interceptor.allowed-encoding/allowed-encoding-interceptor)
                      (interceptor.parameters/parameters-interceptor)
                      (interceptor.muuntaja/format-negotiate-interceptor)
                      (interceptor.muuntaja/format-response-interceptor)
                      (interceptor.muuntaja/format-request-interceptor)
                      ;; (reitit-http.coercion/coerce-response-interceptor)
                      (interceptor.params-mspec/params-mspec-interceptor)
                      ]]
    {;:reitit.interceptor/transform interceptor.dev/print-context-diffs

     :exception reitit.pretty/exception
     :validate  reitit.spec/validate
     ;; :reitit.spec/wrap spec.spell/closed
     :spec      ::router-options
     :data      {:muuntaja         m-instance
                 :interceptors     interceptors
                 ;; :coercion         reitit.coercion.spec/coercion
                 :access-control   {:access-control-allow-origin      [#"http://localhost:.*"]
                                    :access-control-allow-methods     #{:get :post}
                                    :access-control-allow-credentials true}
                 ;; ::reitit-http/req-specs []
                 ::reitit-http/env env
                 }}))


(defn component-start [{:keys [:routes] :as env}]
  ;; TODO - validate routes
  (reitit.http/router routes (router-options (dissoc env :routes))))


(def component
  (system/lifecycle component-start))
