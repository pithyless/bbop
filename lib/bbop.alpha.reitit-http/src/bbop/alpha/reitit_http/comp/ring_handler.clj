(ns bbop.alpha.reitit-http.comp.ring-handler
  (:require
   [bbop.alpha.core.system :as system]
   [reitit.http]
   [reitit.ring]
   [reitit.interceptor.sieppari :as reitit.sieppari]))


(defn component-start [{:keys [:router]}]
  ;; TODO - validate router
  (reitit.http/ring-handler
   router
   (reitit.ring/create-default-handler)
   {:executor reitit.sieppari/executor}))


(def component
  (system/lifecycle component-start))
