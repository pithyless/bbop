(ns bbop.alpha.rad.crud.action-test
  (:require
   [bbop.alpha.core.system :as system]
   [bbop.alpha.test.api :as test :refer [specification component behavior assertions]]
   [bbop.alpha.rad.crud.action :as crud.action]
   [bbop.alpha.rad.database.api :as rad.db]
   [bbop.alpha.fulcro.algo :as algo]
   [clojure.test :refer [is]]

   [bbop.alpha.core.uuid :as uuid]
   [com.fulcrologic.rad.attributes :as attr]
   [com.fulcrologic.rad.form :as form]))


(def ops
  {:simple
   {::form/delta
    {[:demo.office.admin.model.user/uid
      #uuid "ffffffff-ffff-ffff-ffff-000000000001"]
     {:demo.office.admin.model.user/first-name
      {:before "Michael", :after "Mike"}}},
    ::crud.action/pk-attr
    :demo.office.admin.model.user/uid}})


(specification "keys-in-delta"
  (assertions
   (crud.action/keys-in-delta (get-in ops [:simple ::form/delta]))
   =>
   #{:demo.office.admin.model.user/uid
     :demo.office.admin.model.user/first-name}))


(specification "ref->ident"
  (assertions
   (crud.action/ref->ident [:primary-id 123]) => [:primary-id 123]
   (crud.action/ref->ident nil) => nil
   (crud.action/ref->ident {:db/id 123}) => nil))


(def test-schema
  [(attr/new-attribute ::user-id :uuid
                       {::attr/identity? true
                        ::rad.db/alias   :primary-test})

   (attr/new-attribute ::name :string
                       {::rad.db/alias :primary-test})

   (attr/new-attribute ::emails :string
                       {::rad.db/alias     :primary-test
                        ::attr/cardinality :many})

   (attr/new-attribute ::role :ref
                       {::rad.db/alias :primary-test
                        ::attr/target  ::uid})

   (attr/new-attribute ::job :keyword
                       {::rad.db/alias   :primary-test
                        ::attr/identity? true})

   (attr/new-attribute ::gender :enum
                       {::rad.db/alias           :primary-test
                        ::attr/enumerated-values #{:bbop.alpha.rad.crud.action-test.gender/male
                                                   :bbop.alpha.rad.crud.action-test.gender/female
                                                   :bbop.alpha.rad.crud.action-test.gender/unspecified}})

   (attr/new-attribute ::tag-id :uuid
                       {::rad.db/alias :primary-test
                        ::attr/identity? true})

   (attr/new-attribute ::tag-name :string
                       {::rad.db/alias   :primary-test})

   (attr/new-attribute ::tags :ref
                       {::rad.db/alias     :primary-test
                        ::attr/target      ::tag-id
                        ::attr/cardinality :many})])

(def qid->attr
  (into {} (map (juxt ::attr/qualified-key identity) test-schema)))


(specification "qid->attr"
  (assertions
   (into {} (::name qid->attr))
   => {:bbop.alpha.rad.database.api/alias            :primary-test,
       :com.fulcrologic.rad.attributes/type          :string,
       :com.fulcrologic.rad.attributes/qualified-key :bbop.alpha.rad.crud.action-test/name}))


(specification "rdb-delta-txes"
  (let [user-1 (uuid/fake-uuid 1)
        tag-1  (uuid/fake-uuid 2)
        ;; tag-2  (algo/tempid) ;; TODO: needs separate test
        tag-2  (uuid/fake-uuid 3)
        f      (partial crud.action/rdb-delta-txes qid->attr)]
    (assertions
     (f {[::user-id user-1] {::user-id {:after user-1}
                             ::name    {:before "fake" :after "test"}
                             ::tags    {:before [[::tag-id tag-1]] :after [[::tag-id tag-2]]}}
         [::tag-id tag-2]   {::tag-id   {:after tag-2}
                             ::tag-name {:after "other-tag"}}
         })
     =>
     {:tempid->uuid {}
      :txes
      {:primary-test [{:db/id (str user-1), ::user-id user-1}
                      {:db/id (str tag-2), ::tag-id tag-2}
                      [:db/add     (str user-1) ::user-id user-1]
                      [:db/add     (str user-1) ::name "test"]
                      [:db/retract (str user-1) ::tags [::tag-id tag-1]]
                      [:db/add     (str user-1) ::tags [::tag-id tag-2]]
                      [:db/add     (str tag-2) ::tag-id   tag-2]
                      [:db/add     (str tag-2) ::tag-name "other-tag"]]}}
     )))


(defn test-config []
  (system/read-config {:filename "bbop.alpha.rad-test/config.edn"
                       :profile  :test}))


(specification "Save delta with Datomic"
  (test/with-system [system [(test-config) [:comp/rad-db-conn]]]
    (let [rdbconn (-> (system/state system) :comp/rad-db-conn)]
      (assertions

       "connects to rdbconn"
       (set (keys (:databases rdbconn)))
       => #{:primary-test}


       "migrate-all!"
       (-> (rad.db/migrate-all! rdbconn test-schema)
           :primary-test :db-after)
       =fn=> some?


       "connect-all"
       (some-> (rad.db/connect-all rdbconn {})
               :database-conns :primary-test :datomic-db-ref deref)
       =fn=> some?
       )

      (let [rdb  (rad.db/connect-all rdbconn {})
            rdb2 (rad.db/connect-all rdbconn {})]
        (assertions

         "transact!"
         (rad.db/transact! rdb :primary-test [{::user-id (uuid/fake-uuid 1)
                                               ::name    "Jack"
                                               ::tags    [{::tag-id   (uuid/fake-uuid 2)
                                                           ::tag-name "some-tag"}]}])
         =fn=> :tx-data


         "pull-by-attr (same conn as transact)"
         (rad.db/pull-by-attr rdb :primary-test ::user-id [(uuid/fake-uuid 1)] [::name {::tags [::tag-id ::tag-name]}])
         => [{::name "Jack"
              ::tags [{::tag-id (uuid/fake-uuid 2), ::tag-name "some-tag"}]}]


         "transact! via delta"
         (let [tag-3 (algo/tempid)]
           (rad.db/transact!
            rdb :primary-test
            (-> (crud.action/rdb-delta-txes
                 qid->attr
                 {[::user-id (uuid/fake-uuid 1)]
                  {::name {:before "Jack" :after "Jackie"}
                   ::tags {:before [[::tag-id (uuid/fake-uuid 2)]] :after [[::tag-id tag-3]]}}

                  [::tag-id tag-3]
                  {::tag-id   {:after tag-3}
                   ::tag-name {:after "other-tag"}}})
                :txes :primary-test)))
         =fn=> :tx-data


         "pull-by-attr after save"
         (rad.db/pull-by-attr rdb :primary-test ::user-id
                              [(uuid/fake-uuid 1)] [::name {::tags [::tag-name]}])
         => [{::name "Jackie"
              ::tags [{::tag-name "other-tag"}]}]
         )))))


(specification "kdiff->txn"
  (let [f (fn [v]
            (let [res (crud.action/kdiff->txn qid->attr "rootid" v)]
              (if (= res [])
                []
                (do
                  (is (= 1 (count res)))
                  (is (= #{:primary-test} (set (map :alias res))))
                  (get-in res [0 :tx])))))]
    (behavior "scalar string"
      (assertions

       (f [::name {:before nil :after "Mike"}])
       => [[:db/add "rootid" ::name "Mike"]]

       (f [::name {:before "Michael" :after "Mike"}])
       => [[:db/add "rootid" ::name "Mike"]]

       (f [::name {:before "Michael" :after nil}])
       => [[:db/retract "rootid" ::name "Michael"]]

       (f [::name {:before nil :after nil}])
       => []
       ))

    (behavior "scalar ref"
      (assertions

       (f [::job {:before nil :after nil}])
       => []

       (f [::job {:before [:role :boss] :after nil}])
       => [[:db/retract "rootid" ::job [:role :boss]]]

       (f [::job {:before nil :after [:role :boss]}])
       => [[:db/add "rootid" ::job [:role :boss]]]
       ))

    (behavior "single enum"
      (assertions

       (f [::gender {:before nil :after nil}])
       => []

       (f [::gender {:before :bbop.alpha.rad.crud.action-test.gender/male :after nil}])
       => [[:db/retract "rootid" ::gender :bbop.alpha.rad.crud.action-test.gender/male]]

       (f [::gender {:before nil :after :bbop.alpha.rad.crud.action-test.gender/male}])
       => [[:db/add "rootid" ::gender :bbop.alpha.rad.crud.action-test.gender/male]]

       (f [::gender {:before :bbop.alpha.rad.crud.action-test.gender/male :after :bbop.alpha.rad.crud.action-test.gender/female}])
       => [[:db/add "rootid" ::gender :bbop.alpha.rad.crud.action-test.gender/female]]
       ))

    (behavior "multiple string"
      (assertions

       (f [::emails {:before nil :after ["one@e.com"]}])
       => [[:db/add "rootid" ::emails "one@e.com"]]

       (f [::emails {:before nil :after ["one@e.com" "two@e.com"]}])
       => [[:db/add "rootid" ::emails "one@e.com"]
           [:db/add "rootid" ::emails "two@e.com"]]

       (f [::emails {:before ["one@e.com"] :after []}])
       => [[:db/retract "rootid" ::emails "one@e.com"]]

       (f [::emails {:before ["one@e.com"] :after nil}])
       => [[:db/retract "rootid" ::emails "one@e.com"]]

       (f [::emails {:before ["one@e.com"] :after ["one@e.com"]}])
       => []

       (f [::emails {:before ["one@e.com"] :after ["one@e.com" "two@e.com"]}])
       => [[:db/add "rootid" ::emails "two@e.com"]]

       (f [::emails {:before ["one@e.com"] :after ["two@e.com"]}])
       => [[:db/retract "rootid" ::emails "one@e.com"]
           [:db/add "rootid" ::emails "two@e.com"]]

       (f [::emails {:before ["one@e.com" "two@e.com"] :after []}])
       => [[:db/retract "rootid" ::emails "one@e.com"]
           [:db/retract "rootid" ::emails "two@e.com"]]

       (f [::emails {:before ["one@e.com" "two@e.com"] :after ["one@e.com"]}])
       => [[:db/retract "rootid" ::emails "two@e.com"]]

       (f [::emails {:before ["one@e.com" "two@e.com"] :after ["one@e.com" "two@e.com"]}])
       => []

       (f [::emails {:before ["one@e.com" "two@e.com"] :after ["one@e.com" "two@e.com" "three@e.com"]}])
       => [[:db/add "rootid" ::emails "three@e.com"]]

       (f [::emails {:before ["one@e.com" "two@e.com"] :after ["three@e.com"]}])
       => [[:db/retract "rootid" ::emails "one@e.com"]
           [:db/retract "rootid" ::emails "two@e.com"]
           [:db/add "rootid" ::emails "three@e.com"]]
       ))

    (behavior "multiple ref"
      (assertions

       (f [::tags {:before nil :after [[::tag-id (uuid/fake-uuid 1)]]}])
       => [[:db/add "rootid" ::tags [::tag-id (uuid/fake-uuid 1)]]]

       (f [::tags {:before nil :after [[::tag-id (uuid/fake-uuid 1)] [::tag-id (uuid/fake-uuid 2)]]}])
       => [[:db/add "rootid" ::tags [::tag-id (uuid/fake-uuid 1)]]
           [:db/add "rootid" ::tags [::tag-id (uuid/fake-uuid 2)]]]

       (f [::tags {:before [[::tag-id (uuid/fake-uuid 1)]] :after []}])
       => [[:db/retract "rootid" ::tags [::tag-id (uuid/fake-uuid 1)]]]

       (f [::tags {:before [[::tag-id (uuid/fake-uuid 1)]] :after [[::tag-id (uuid/fake-uuid 1)]]}])
       => []

       (f [::tags {:before [[::tag-id (uuid/fake-uuid 1)]] :after [[::tag-id (uuid/fake-uuid 1)] [::tag-id (uuid/fake-uuid 2)]]}])
       => [[:db/add "rootid" ::tags [::tag-id (uuid/fake-uuid 2)]]]

       (f [::tags {:before [[::tag-id (uuid/fake-uuid 1)]] :after [[::tag-id (uuid/fake-uuid 2)]]}])
       => [[:db/retract "rootid" ::tags [::tag-id (uuid/fake-uuid 1)]]
           [:db/add "rootid" ::tags [::tag-id (uuid/fake-uuid 2)]]]

       (f [::tags {:before [[::tag-id (uuid/fake-uuid 1)] [::tag-id (uuid/fake-uuid 2)]] :after []}])
       => [[:db/retract "rootid" ::tags [::tag-id (uuid/fake-uuid 1)]]
           [:db/retract "rootid" ::tags [::tag-id (uuid/fake-uuid 2)]]]

       (f [::tags {:before [[::tag-id (uuid/fake-uuid 1)] [::tag-id (uuid/fake-uuid 2)]] :after [[::tag-id (uuid/fake-uuid 1)]]}])
       => [[:db/retract "rootid" ::tags [::tag-id (uuid/fake-uuid 2)]]]

       (f [::tags {:before [[::tag-id (uuid/fake-uuid 1)] [::tag-id (uuid/fake-uuid 2)]] :after [[::tag-id (uuid/fake-uuid 1)] [::tag-id (uuid/fake-uuid 2)]]}])
       => []

       (f [::tags {:before [[::tag-id (uuid/fake-uuid 1)] [::tag-id (uuid/fake-uuid 2)]] :after [[::tag-id (uuid/fake-uuid 1)] [::tag-id (uuid/fake-uuid 2)] [::tag-id (uuid/fake-uuid 3)]]}])
       => [[:db/add "rootid" ::tags [::tag-id (uuid/fake-uuid 3)]]]

       (f [::tags {:before [[::tag-id (uuid/fake-uuid 1)] [::tag-id (uuid/fake-uuid 2)]] :after [[::tag-id (uuid/fake-uuid 3)]]}])
       => [[:db/retract "rootid" ::tags [::tag-id (uuid/fake-uuid 1)]]
           [:db/retract "rootid" ::tags [::tag-id (uuid/fake-uuid 2)]]
           [:db/add "rootid" ::tags [::tag-id (uuid/fake-uuid 3)]]]
       ))))
