(ns bbop.alpha.rad.database-adapter.datomic-schema-test
  (:require
   [bbop.alpha.test.api :as test :refer [specification component behavior assertions]]
   [bbop.alpha.rad.database-adapter.datomic.schema :as rad.db.datomic.schema]
   [bbop.alpha.rad.database.api :as rad.db]
   [com.fulcrologic.rad.attributes :as attr]))


(def scalar-types
  {:boolean :db.type/boolean
   :decimal :db.type/bigdec
   :double  :db.type/double
   :enum    :db.type/ref
   :instant :db.type/instant
   :keyword :db.type/keyword
   :long    :db.type/long
   :string  :db.type/string
   :symbol  :db.type/symbol
   :text    :db.type/string
   :uuid    :db.type/uuid})


(specification "Datomic attribute tx-data"
  (assertions

   "minimum data"
   (rad.db.datomic.schema/attr-tx-data
    (attr/new-attribute :test.user/name :string {}))
   =>  [#:db{:ident       :test.user/name
             :valueType   :db.type/string
             :cardinality :db.cardinality/one
             :index       true}]

   "support all scalars"
   (into []
         (comp
          (map #(attr/new-attribute ::example % {}))
          (map #(rad.db.datomic.schema/attr-tx-data %))
          (map (comp :db/valueType first)))
         (keys scalar-types))
   => (vals scalar-types)


   "no index"
   (rad.db.datomic.schema/attr-tx-data
    (attr/new-attribute :test.user/name :string
                        {:db/index false}))
   =>  [#:db{:ident       :test.user/name
             :valueType   :db.type/string
             :cardinality :db.cardinality/one
             :index       false}]

   "identity?"
   (rad.db.datomic.schema/attr-tx-data
    (attr/new-attribute :test.user/uid :uuid
                        {::attr/identity? true}))
   =>  [#:db{:ident       :test.user/uid
             :valueType   :db.type/uuid
             :cardinality :db.cardinality/one
             :unique      :db.unique/identity
             :index       true}]

   "one-to-many"
   (rad.db.datomic.schema/attr-tx-data
    {::attr/qualified-key :test.post/author
     ::attr/type          :ref})
   =>  [#:db{:ident       :test.post/author
             :valueType   :db.type/ref
             :cardinality :db.cardinality/one
             :index       true}]

   "many-to-many"
   (rad.db.datomic.schema/attr-tx-data
    {::attr/qualified-key :test.post/tags
     ::attr/type          :ref
     ::attr/cardinality   :many})
   =>  [#:db{:ident       :test.post/tags
             :valueType   :db.type/ref
             :cardinality :db.cardinality/many
             :index       true}]

   "enum with ident"
   (->
    (rad.db.datomic.schema/attr-tx-data
     {::attr/qualified-key     :test.user/gender
      ::attr/type              :enum
      ::attr/enumerated-values #{:test.user.gender/male
                                 :female
                                 {:db/ident :test.user.gender/unspecified
                                  :db/doc   "Unspecified"}}})
    set)
   =>  (set
        [#:db{:ident       :test.user/gender
              :valueType   :db.type/ref
              :cardinality :db.cardinality/one
              :index       true}
         #:db{:ident :test.user.gender/male}
         #:db{:ident :test.user.gender/female}
         #:db{:ident :test.user.gender/unspecified
              :doc   "Unspecified"}])

   ;; FIXME!
   ;; "alias for deprecated key"
   ;; (rad.db.datomic.schema/attr-tx-data
   ;;  (attr/new-attribute :test.user/name :string
   ;;                      {::rad.db/deprecated-ident :test.user/old-name}))
   ;; =>  [#:db{:ident       :test.user/old-name
   ;;           :valueType   :db.type/string
   ;;           :cardinality :db.cardinality/one
   ;;           :index       true}
   ;;      [:db/add :test.user/old-name :db/ident :test.user/name]]
   ))
