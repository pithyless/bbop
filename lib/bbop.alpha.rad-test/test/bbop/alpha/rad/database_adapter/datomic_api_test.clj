(ns bbop.alpha.rad.database-adapter.datomic-api-test
  (:require
   [bbop.alpha.core.system :as system]
   [bbop.alpha.test.api :as test :refer [specification component behavior assertions]]
   [bbop.alpha.rad.database.api :as rad.db]
   [bbop.alpha.core.uuid :as uuid]
   [com.fulcrologic.rad.attributes :as attr]
   ))


(defn test-config []
  (system/read-config {:filename "bbop.alpha.rad-test/config.edn"
                       :profile  :test}))


(def test-schema
  [(attr/new-attribute ::uid :uuid
                       {::attr/identity? true
                        ::rad.db/alias   :primary-test})

   (attr/new-attribute ::name :string
                       {::rad.db/alias :primary-test})

   (attr/new-attribute ::supervisor :ref
                       {::attr/target  ::uid
                        ::rad.db/alias :primary-test})
   ])


(specification "Rad DB with Datomic"
  (test/with-system [system [(test-config) [:comp/rad-db-conn]]]
    (let [rdbconn (-> (system/state system) :comp/rad-db-conn)]
      (assertions

       "connects to rdbconn"
       (set (keys (:databases rdbconn)))
       => #{:primary-test}


       "all-migration-txes"
       (rad.db/all-migration-txes rdbconn test-schema)
       =>
       {:primary-test
        [#:db{:ident       :bbop.alpha.rad.database-adapter.datomic-api-test/uid,
              :valueType   :db.type/uuid,
              :cardinality :db.cardinality/one,
              :index       true,
              :unique      :db.unique/identity}
         #:db{:ident       :bbop.alpha.rad.database-adapter.datomic-api-test/name,
              :valueType   :db.type/string,
              :cardinality :db.cardinality/one,
              :index       true}
         #:db{:ident       :bbop.alpha.rad.database-adapter.datomic-api-test/supervisor,
              :valueType   :db.type/ref,
              :cardinality :db.cardinality/one,
              :index       true}]}


       "migrate-all!"
       (-> (rad.db/migrate-all! rdbconn test-schema)
           :primary-test :db-after)
       =fn=> some?


       "connect-all"
       (some-> (rad.db/connect-all rdbconn {})
               :database-conns :primary-test :datomic-db-ref)
       =fn=> some?)


      (let [rdb  (rad.db/connect-all rdbconn {})
            rdb2 (rad.db/connect-all rdbconn {})]
        (assertions

         "transact!"
         (rad.db/transact! rdb
                           :primary-test
                           [{:db/id "jack"
                             ::uid  (uuid/fake-uuid 1)
                             ::name "Jack"}
                            {::uid  (uuid/fake-uuid 2)
                             ::name "Jill"
                             ::supervisor "jack"}])
         =fn=> :tx-data


         "pull-by-attr (same conn as transact)"
         (rad.db/pull-by-attr rdb
                              :primary-test ::uid
                              [(uuid/fake-uuid 1)]
                              [::name])
         => [{::name "Jack"}]


         "pull-by-attr (conn before transact)"
         (rad.db/pull-by-attr rdb2
                              :primary-test ::uid
                              [(uuid/fake-uuid 1)]
                              [::name])
         => [nil]


         "pull-by-attr (new conn after transact)"
         (rad.db/pull-by-attr (rad.db/connect-all rdbconn {})
                              :primary-test ::uid
                              [(uuid/fake-uuid 1)]
                              [::name])
         => [{::name "Jack"}]


         "query (same conn as transact)"
         (rad.db/query rdb
                       :primary-test
                       '[:find ?name
                         :where [_ ::name ?name]])
         => #{["Jack"] ["Jill"]}


         "query (conn before transact)"
         (rad.db/query rdb2
                       :primary-test
                       '[:find ?name
                         :where [_ ::name ?name]])
         => #{}


         "query (new conn after transact)"
         (rad.db/query (rad.db/connect-all rdbconn {})
                       :primary-test
                       '[:find ?name
                         :where [_ ::name ?name]])
         => #{["Jack"] ["Jill"]}


         "query (same conn as transact) with extra inputs"
         (rad.db/query rdb
                       :primary-test
                       '[:find ?name
                         :in $ ?uid
                         :where
                         [?e ::uid ?uid]
                         [?e ::name ?name]]
                       [(uuid/fake-uuid 1)])
         => #{["Jack"]}


         "query with to-one joins"
         (rad.db/pull-by-attr (rad.db/connect-all rdbconn {})
                              :primary-test ::uid
                              [(uuid/fake-uuid 2)]
                              [::name {::supervisor [::name]}])
         => [{::name "Jill"
              ::supervisor {::name "Jack"}}]


         )))))
