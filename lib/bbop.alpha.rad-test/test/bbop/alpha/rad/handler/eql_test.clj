(ns bbop.alpha.rad.handler.eql-test
  (:require
   [bbop.alpha.core.contrib :as contrib]
   [bbop.alpha.core.system :as system]
   [bbop.alpha.test.api :as test :refer [specification component behavior assertions]]
   [bbop.alpha.rad.database.api :as rad.db]
   [bbop.alpha.rad.env :as rad.env]
   [bbop.alpha.datomic.api :as datomic]
   [bbop.alpha.rad.resolver.api :as rad.resolver]
   [bbop.alpha.core.uuid :as uuid]
   [bbop.alpha.pathom.api :as pathom]
   [bbop.alpha.reitit-http.api :as reitit-http]
   [bbop.alpha.rad.mspec :as rad.mspec]
   [bbop.alpha.rad.handler.eql :as rad.handler.eql]
   [bbop.alpha.http-client.api :as http-client]

   [com.fulcrologic.rad.attributes :as attr]
   ))



(def test-schema
  [(attr/new-attribute ::uid :uuid
                       {::attr/identity? true
                        ::rad.db/alias   :primary-test})

   (attr/new-attribute ::first-name :string
                       {::rad.db/alias :primary-test})

   (attr/new-attribute ::last-name :string
                       {::rad.db/alias :primary-test})

   (attr/new-attribute ::secret :string
                       {::rad.db/alias :primary-test})

   (attr/new-attribute ::gender :enum
                       {::attr/enumerated-values #{:male :female :unspecified}
                        ::rad.db/alias           :primary-test})

   (attr/new-attribute ::supervisor :ref
                       {::attr/target  ::uid
                        ::rad.db/alias :primary-test})
   ])


(def test-resolvers
  [(rad.resolver/db-entity-attr-resolver
    :primary-test ::uid
    [::first-name ::last-name])

   (rad.resolver/db-entity-enum-resolver
    :primary-test ::uid #{::gender})

   (rad.resolver/db-entity-ref-resolver
    :primary-test ::uid {::supervisor #{::uid}})

   (rad.resolver/compute-attr-resolver
    ::full-name #{::first-name ::last-name}
    (fn [_ {:keys [::first-name ::last-name]}]
      (str first-name " " last-name)))

   rad.resolver/index-explorer])


(def test-routes
  [["/" {::reitit-http/allowed-accept       [#{:transit+json :edn}]
         ::reitit-http/allowed-content-type [#{:transit+json :edn}]}
    [["api" {:post {:handler rad.handler.eql/parse-eql
                    ::reitit-http/env-specs
                    [[:map
                      [::rad.env/rad-pathom any?]
                      [::rad.env/rad-db-conn any?]]]
                    ::reitit-http/req-specs
                    [[:map [:body-params rad.mspec/eql-query]]]
                    }}]]]])


(defn test-config []
  (contrib/supdate
   (system/read-config {:filename "bbop.alpha.rad-test/config.edn"
                        :profile  :test})
   {:config {:cfg/rad-attr-registries   (constantly test-schema)
             :cfg/rad-pathom-registries (constantly test-resolvers)
             :cfg/rad-server-routes     (constantly test-routes)
             }}))


(defn seed-db [system]
  (let [rdb (-> (system/state system)
                :comp/rad-db-conn
                rad.db/connect-all)]
    (rad.db/transact! rdb :primary-test
                      [{::uid        (uuid/fake-uuid 1)
                        ::first-name "Jack"
                        ::last-name  "Trap"}
                       {::uid        (uuid/fake-uuid 2)
                        ::first-name "Jill"
                        ::last-name  "Thrill"}])))


(specification "Resolver API"
  (test/with-system [system [(test-config) [:comp/rad-web-server
                                            :comp/http-client]]]
    (contrib/cond
      :let [handler (-> (system/state system) :comp/rad-web-server)
            client  (-> (system/state system) :comp/http-client)]

      :do (seed-db system)

      :do (assertions

           "Start system"
           handler =fn=> some?
           client =fn=> some?


           "api resolver response"
           ((juxt :status :body)
            (http-client/fetch
             client
             {:url         "http://localhost:9876/api"
              :method      :post
              :as          :transit+json
              :form-params [{[::uid (uuid/fake-uuid 1)]
                             [::full-name]}]
              }))
           => [200 {[::uid (uuid/fake-uuid 1)] {::full-name "Jack Trap"}}]


           "index explorer"
           ((juxt :status
                  (comp
                   #(get-in % [[:com.wsscode.pathom.viz.index-explorer/id
                                [:fulcro.inspect.core/app-uuid (uuid/fake-uuid 9)]]
                               :com.wsscode.pathom.viz.index-explorer/index
                               :com.wsscode.pathom.connect/index-resolvers
                               'com.wsscode.pathom.connect/indexes-resolver
                               :com.wsscode.pathom.connect/sym])
                   :body))
            (http-client/fetch
             client
             {:url         "http://localhost:9876/api"
              :method      :post
              :as          :transit+json
              :form-params [{[:com.wsscode.pathom.viz.index-explorer/id
                              [:fulcro.inspect.core/app-uuid (uuid/fake-uuid 9)]]
                             [:com.wsscode.pathom.viz.index-explorer/index]}]}))
           => [200 'com.wsscode.pathom.connect/indexes-resolver]

           ))))
