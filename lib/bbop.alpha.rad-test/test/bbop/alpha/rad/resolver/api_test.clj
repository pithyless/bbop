(ns bbop.alpha.rad.resolver.api-test
  (:require
   [bbop.alpha.core.contrib :as contrib]
   [bbop.alpha.core.system :as system]
   [bbop.alpha.test.api :as test :refer [specification component behavior assertions]]
   [bbop.alpha.rad.database.api :as rad.db]
   [bbop.alpha.rad.env :as rad.env]
   [bbop.alpha.datomic.api :as datomic]
   [bbop.alpha.rad.resolver.api :as rad.resolver]
   [bbop.alpha.core.uuid :as uuid]
   [bbop.alpha.pathom.api :as pathom]

   [com.fulcrologic.rad.attributes :as attr]
   ))


(def test-schema
  [
   (attr/new-attribute :video/uid :uuid
                       {::rad.db/alias   :primary-test
                        ::attr/identity? true})

   (attr/new-attribute :video/title :string
                       {::rad.db/alias :primary-test})

   (attr/new-attribute :video.category/uid :uuid
                       {::rad.db/alias   :primary-test
                        ::attr/identity? true})

   (attr/new-attribute :video.category/title :string
                       {::rad.db/alias :primary-test})

   (attr/new-attribute :video.parody/source :ref
                       {::rad.db/alias :primary-test
                        ::attr/target  :video/uid})

   (attr/new-attribute :video.parody/source :ref
                       {::rad.db/alias :primary-test
                        ::attr/target  :video/uid})
   ])




;; (def test-schema
;;   [(attr/new-attribute ::uid :uuid
;;                        {::attr/identity? true
;;                         ::rad.db/alias   :primary-test})

;;    (attr/new-attribute ::first-name :string
;;                        {::rad.db/alias :primary-test})

;;    (attr/new-attribute ::last-name :string
;;                        {::rad.db/alias :primary-test})

;;    (attr/new-attribute ::secret :string
;;                        {::rad.db/alias :primary-test})

;;    (attr/new-attribute ::gender :enum
;;                        {::attr/enumerated-values #{:male :female :unspecified}
;;                         ::rad.db/alias :primary-test})

;;    (attr/new-attribute ::supervisor :ref
;;                        {::attr/target  ::uid
;;                         ::rad.db/alias :primary-test})
;;    ])


;; (def test-resolvers
;;   [(rad.resolver/db-entity-attr-resolver
;;     :primary-test ::uid
;;     [::first-name ::last-name])

;;    (rad.resolver/db-entity-enum-resolver
;;     :primary-test ::uid #{::gender})

;;    (rad.resolver/db-entity-ref-resolver
;;     :primary-test ::uid {::supervisor #{::uid}})

;;    (rad.resolver/db-entity-reverse-ref-resolver
;;     :primary-test ::uid {::employees [:bbop.alpha.rad.resolver.api-test/_supervisor ::uid]})

;;    (rad.resolver/compute-attr-resolver
;;     ::full-name #{::first-name ::last-name}
;;     (fn [_ {:keys [::first-name ::last-name]}]
;;       (str first-name " " last-name)))])


;; (defn test-config []
;;   (contrib/supdate
;;    (system/read-config {:filename "bbop.alpha.rad-test/config.edn"
;;                         :profile  :test})
;;    {:config {:cfg/rad-attr-registries   (constantly test-schema)
;;              :cfg/rad-pathom-registries (constantly test-resolvers)}}))


;; (specification "Resolver API"
;;   (test/with-system [system [(test-config) [:comp/rad-db-conn :comp/rad-pathom]]]
;;     (contrib/cond
;;       :let [rdbconn (-> (system/state system) :comp/rad-db-conn)
;;             rdb     (rad.db/connect-all rdbconn)]

;;       :do (assertions

;;            "Datomic DB is already migrated with attributes"
;;            (some->> (some-> rdb :database-conns :primary-test :datomic-db-ref deref)
;;                     (datomic/q '[:find [?name ...]
;;                                  :where [_ :db/ident ?name]])
;;                     (filter #(= (namespace ::id) (namespace %)))
;;                     (set))
;;            => #{::uid ::first-name ::last-name ::secret ::gender ::supervisor}


;;            "seed data"
;;            (rad.db/transact! rdb
;;                              :primary-test
;;                              [{:db/id       "fake-1"
;;                                ::uid        (uuid/fake-uuid 1)
;;                                ::first-name "Jack"
;;                                ::last-name  "Bravo"
;;                                ::secret     "secret1"
;;                                ::gender     :bbop.alpha.rad.resolver.api-test.gender/male}
;;                               {::uid        (uuid/fake-uuid 2)
;;                                ::first-name "Jill"
;;                                ::last-name  "Ripper"
;;                                ::secret     "secret2"
;;                                ::supervisor "fake-1"
;;                                }])
;;            =fn=> :tx-data)

;;       :let [parser (-> (system/state system) :comp/rad-pathom)]

;;       :do (assertions

;;            "resolve attrs by id"
;;            (pathom/parse parser
;;                          {::rad.env/rdb rdb}
;;                          [{[::uid (uuid/fake-uuid 1)]
;;                            [::first-name]}])
;;            => {[::uid (uuid/fake-uuid 1)]
;;                {::first-name "Jack"}}

;;            "resolve multiple attrs by id"
;;            (pathom/parse parser
;;                          {::rad.env/rdb rdb}
;;                          [{[::uid (uuid/fake-uuid 1)]
;;                            [::first-name ::last-name]}])
;;            => {[::uid (uuid/fake-uuid 1)]
;;                {::first-name "Jack"
;;                 ::last-name  "Bravo"}}

;;            "resolve '*"
;;            (pathom/parse parser
;;                          {::rad.env/rdb rdb}
;;                          [{[::uid (uuid/fake-uuid 1)]
;;                            ['*]}])
;;            => {[::uid (uuid/fake-uuid 1)]
;;                {::uid (uuid/fake-uuid 1)}}

;;            "do not resolve not-exported attributes"
;;            (pathom/parse parser
;;                          {::rad.env/rdb rdb}
;;                          [{[::uid (uuid/fake-uuid 1)]
;;                            [::first-name ::secret]}])
;;            => {[::uid (uuid/fake-uuid 1)]
;;                {::first-name "Jack"}}


;;            "resolve computed attr"
;;            (pathom/parse parser
;;                          {::rad.env/rdb rdb}
;;                          [{[::uid (uuid/fake-uuid 1)]
;;                            [::full-name]}])
;;            => {[::uid (uuid/fake-uuid 1)]
;;                {::full-name "Jack Bravo"}}


;;            "resolve enum idents as keywords (for fulcro-rad)"
;;            (pathom/parse parser
;;                          {::rad.env/rdb rdb}
;;                          [{[::uid (uuid/fake-uuid 1)] [::gender]}
;;                           {[::uid (uuid/fake-uuid 2)] [::gender]}])
;;            => {[::uid (uuid/fake-uuid 1)]
;;                {::gender :bbop.alpha.rad.resolver.api-test.gender/male}
;;                [::uid (uuid/fake-uuid 2)] {}}


;;            ;; TODO - fulcro-rad enum assumes keywords
;;            ;; "resolve enum idents"
;;            ;; (pathom/parse parser
;;            ;;               {::rad.env/rdb rdb}
;;            ;;               [{[::uid (uuid/fake-uuid 1)] [{::gender [:db/ident]}]}
;;            ;;                {[::uid (uuid/fake-uuid 2)] [{::gender [:db/ident]}]}])
;;            ;; => {[::uid (uuid/fake-uuid 1)]
;;            ;;     {::gender {:db/ident :bbop.alpha.rad.resolver.api-test.gender/male}}

;;            ;;     [::uid (uuid/fake-uuid 2)] {}}


;;            ;; TODO - fulcro-rad enum assumes keywords
;;            ;; "resolve enum idents without explicit join"
;;            ;; (pathom/parse parser
;;            ;;               {::rad.env/rdb rdb}
;;            ;;               [{[::uid (uuid/fake-uuid 1)] [::gender]}
;;            ;;                {[::uid (uuid/fake-uuid 2)] [::gender]}])
;;            ;; => {[::uid (uuid/fake-uuid 1)]
;;            ;;     {::gender {:db/ident :bbop.alpha.rad.resolver.api-test.gender/male}}

;;            ;;     [::uid (uuid/fake-uuid 2)] {}}


;;            "resolve to-one joins w/o explicit join"
;;            (pathom/parse parser
;;                          {::rad.env/rdb rdb}
;;                          [{[::uid (uuid/fake-uuid 1)] [::supervisor]}
;;                           {[::uid (uuid/fake-uuid 2)] [::supervisor]}])
;;            => {[::uid (uuid/fake-uuid 1)] {}
;;                [::uid (uuid/fake-uuid 2)]
;;                {::supervisor {::uid (uuid/fake-uuid 1)}}}


;;            "resolve to-one joins"
;;            (pathom/parse parser
;;                          {::rad.env/rdb rdb}
;;                          [{[::uid (uuid/fake-uuid 1)] [{::supervisor [::full-name]}]}
;;                           {[::uid (uuid/fake-uuid 2)] [{::supervisor [::full-name]}]}])
;;            => {[::uid (uuid/fake-uuid 1)] {}
;;                [::uid (uuid/fake-uuid 2)]
;;                {::supervisor {::full-name "Jack Bravo"}}}


;;            "resolve one-to-many reverse joins w/o explicit join"
;;            (pathom/parse parser
;;                          {::rad.env/rdb rdb}
;;                          [{[::uid (uuid/fake-uuid 1)] [::employees]}
;;                           {[::uid (uuid/fake-uuid 2)] [::employees]}])
;;            => {[::uid (uuid/fake-uuid 1)] {::employees [{::uid (uuid/fake-uuid 2)}]}
;;                [::uid (uuid/fake-uuid 2)] {}}


;;            "resolve one-to-many reverse joins"
;;            (pathom/parse parser
;;                          {::rad.env/rdb rdb}
;;                          [{[::uid (uuid/fake-uuid 1)] [{::employees [::full-name]}]}
;;                           {[::uid (uuid/fake-uuid 2)] [{::employees [::full-name]}]}])
;;            => {[::uid (uuid/fake-uuid 1)] {::employees [{::full-name "Jill Ripper"}]}
;;                [::uid (uuid/fake-uuid 2)] {}}
;;            ))))
