(ns bbop.alpha.http-server.comp.aleph
  (:require
   ;; [aleph.netty :as netty]
   [aleph.http :as aleph-http]
   [bbop.alpha.core.guard :refer [>defn =>]]
   [bbop.alpha.core.system :as system]
   [bbop.alpha.core.log :as log]
   [bbop.alpha.core.malli :as malli])
  (:import [java.io Closeable]))


(>defn component-start
  [{:keys [:handler :port]}]
  [[:map
    [:handler [:fn fn?]]
    [:port pos-int?]]
   => [:map [:server any?]]]
  (let [aleph-opts {:port port}
        server     (aleph-http/start-server handler aleph-opts)]
    {:server server}))


(>defn component-stop
  [{:keys [:server]}]
  [[:map [:server any?]] => any?]
  (try
    (.close ^Closeable server)
    ;; FIXME: https://github.com/ztellman/aleph/issues/365
    ;; (netty/wait-for-close server)
    (catch Exception e
      (log/error ::error-stopping e))))


(def component
  (system/lifecycle component-start component-stop))
