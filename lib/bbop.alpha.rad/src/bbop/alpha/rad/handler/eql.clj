(ns bbop.alpha.rad.handler.eql
  (:require
   [bbop.alpha.reitit-http.api :as reitit-http]
   [bbop.alpha.rad.env :as rad.env]
   [bbop.alpha.pathom.api :as pathom]
   [bbop.alpha.rad.database.api :as rad.db]

   [com.fulcrologic.rad.attributes :as attr]
   ))


(defn- parse-attr-registry [registry]
  (into {}
        (map (juxt ::attr/qualified-key identity))
        registry))


(defn parse-eql [{:keys [:body-params] :as req}]
  (let [{:keys [::rad.env/rad-pathom
                ::rad.env/rad-db-conn]
         :as   _env} (reitit-http/route-env req)
        parser-opts  {::rad.env/rdb       (rad.db/connect-all rad-db-conn)
                      ::rad.env/qid->attr (parse-attr-registry (:attr-registry rad-pathom))}
        eql          body-params
        result       (pathom/parse rad-pathom parser-opts eql)]
    {:status 200 :body result}))
