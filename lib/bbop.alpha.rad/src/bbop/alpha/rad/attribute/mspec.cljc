(ns bbop.alpha.rad.attribute.mspec)


(def qid
  qualified-keyword?)


(def attr
  [:map
   [:com.fulcrologic.rad.attributes/qualified-key qid]])
