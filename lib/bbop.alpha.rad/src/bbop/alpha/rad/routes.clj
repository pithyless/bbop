(ns bbop.alpha.rad.routes
  (:require
   [bbop.alpha.rad.handler.eql :as rad.handler.eql]
   [bbop.alpha.reitit-http.api :as reitit-http]
   [bbop.alpha.rad.env :as rad.env]
   [bbop.alpha.rad.mspec :as rad.mspec]))


(defn eql-api [path]
  [path
   {:post
    {:handler                           rad.handler.eql/parse-eql
     ::reitit-http/allowed-accept       [#{:transit+json :edn}]
     ::reitit-http/allowed-content-type [#{:transit+json :edn}]
     ::reitit-http/env-specs            [[:map
                                          [::rad.env/rad-pathom any?]
                                          [::rad.env/rad-db-conn any?]]]
     ::reitit-http/req-specs            [[:map
                                          [:body-params
                                           rad.mspec/eql-query]]]}}])
