(ns bbop.alpha.rad.crud.action
  (:require
   [edn-query-language.core :as eql]
   [com.wsscode.pathom.connect :as pc]
   [com.rpl.specter :as specter]
   [bbop.alpha.fulcro.app :as app]
   [bbop.alpha.core.contrib :as contrib]
   [clojure.set :as set]
   [com.fulcrologic.rad.attributes :as attr]
   [bbop.alpha.rad.database.api :as rad.db]
   [bbop.alpha.core.uuid :as uuid]
   [bbop.alpha.fulcro.algo :as algo]
   [clojure.string :as str]))


(defn keys-in-delta [delta]
  (let [id-keys  (into #{}
                       (map first)
                       (keys delta))
        all-keys (into id-keys
                       (mapcat keys)
                       (vals delta))]
    all-keys))


(defn ref->ident
  "Sometimes references on the client are actual idents and sometimes they are
  nested maps, this function attempts to return an ident regardless."
  [x]
  (cond
    (eql/ident? x) x))


(def default-scalar-conformer
  {:boolean (fn [_ v] v)
   :decimal (fn [_ v] v)
   :double  (fn [_ v] v)
   :enum    (fn [_ v] v)
   :instant (fn [_ v] v)
   :keyword (fn [_ v] v)
   :long    (fn [_ v] v)
   :string  (fn [_ v] v)
   :symbol  (fn [_ v] v)
   :text    (fn [_ v] v)
   :uuid    (fn [_ v] (when v (uuid/parse v)))})


(defn conform-attr [qid->attr qid value]
  (contrib/cond
    :let [attr-spec (get qid->attr qid)
          kind      (::attr/type attr-spec)]

    (= :ref kind)
    (let [ident (ref->ident value)]
      (when ident
        (let [[cqid cval] ident]
          (cond
            (and (string? cval)
                 (not (str/blank? cval))) cval
            (nil? cval)                   nil
            :else                         [cqid (conform-attr qid->attr cqid cval)]))))

    :let [scalar-fn (get default-scalar-conformer kind)]

    (some? scalar-fn)
    (scalar-fn attr-spec value)

    :else
    (throw (ex-info "Unsupported DB conform-attr" {:qid qid :value value}))))


(defn kdiff->txn [qid->attr eid [k diff]]
  (contrib/cond
    :let [{:keys [:before :after]} diff]

    :let [conformer (partial conform-attr qid->attr k)
          dbalias   (get-in qid->attr [k ::rad.db/alias])
          many?     (= :many (get-in qid->attr [k ::attr/cardinality]))]

    :let [conform-all (fn [v] (if many?
                                (into #{}
                                      (comp (map conformer)
                                            (remove nil?))
                                      v)
                                (conformer v)))
          before (conform-all before)
          after  (conform-all after)]

    ;; Assume field is optional and omit
    (and (not many?) (nil? before) (nil? after))
    []

    (and many? (empty? before) (empty? after))
    []

    (not many?)
    (if (nil? after)
      [{:alias dbalias :tx [[:db/retract eid k before]]}]
      [{:alias dbalias :tx [[:db/add eid k after]]}])

    many?
    (let [retracts (set/difference before after)
          adds     (set/difference after before)]
      [{:alias dbalias
        :tx (into (mapv #(-> [:db/retract eid k %]) retracts)
              (mapv #(-> [:db/add eid k %]) adds))}])

    :else
    (throw (ex-info "Invalid kdiff->txn state" {:data [eid k diff many?]}))))


(defn rdb-delta-txes
  [qid->attr raw-delta]
  (tap> [:RDB-DELTA-RAW raw-delta])
  (let [tempids       (specter/select (specter/walker algo/tempid?) raw-delta)
        tempid->uuid  (into {} (map (fn [t] [t (uuid/random-squuid)]) tempids))
        rootid->tx    (into {}
                            (map (fn [[id-key id-val]]
                                   [(str id-val) [{id-key id-val, :db/id (str id-val)}]])
                                 (keys raw-delta)))
        delta         (specter/transform (specter/walker algo/tempid?) (comp str tempid->uuid) raw-delta)
        entity-differ (fn [[[id-key id-val] entity-diff]]
                        (mapcat (fn [[k diff]] (kdiff->txn qid->attr (str id-val) [k diff]))
                                entity-diff))
        tx-groups     (->> (mapcat entity-differ delta)
                           (group-by :alias)
                           (reduce-kv (fn [m k v] (assoc m k (reduce into [] (map :tx v)))) {}))
        used-ids      (into {}
                            (map (fn [[dbalias txes]]
                                   [dbalias (set (specter/select (specter/walker (set (keys rootid->tx))) txes))]))
                            tx-groups)
        all-txes      (into {}
                            (for [[dbalias txes] tx-groups]
                              [dbalias (into (vec
                                              (mapcat
                                               #(get rootid->tx %)
                                               (seq (get used-ids dbalias []))))
                                             txes)]))]
    (tap> [:RDB-DELTA-TX all-txes])
    {:tempid->uuid tempid->uuid
     :txes         all-txes}))
