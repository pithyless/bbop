(ns bbop.alpha.rad.resolver.api
  (:require
   [com.wsscode.pathom.connect :as pc]
   [bbop.alpha.rad.env :as rad.env]
   [bbop.alpha.rad.database.api :as rad.db]
   [clojure.walk :as walk]))


(defn munge-kw [kw]
  (munge (subs (str kw) 1)))


(defn db-entity-attr-resolver
  [db-id id-attr attrs]
  (let [sym         (symbol (str (munge-kw id-attr) "->db-entity-attributes"))
        opts        {::pc/input  #{id-attr}
                     ::pc/output attrs}
        resolver-fn (fn [{:keys [::rad.env/rdb] :as _env} input]
                      ;; TODO - validate and check inputs
                      (rad.db/pull-by-attr rdb db-id id-attr
                                           (get input id-attr)
                                           attrs))]
    (pc/resolver sym opts resolver-fn)))


(defn db-entity-enum-resolver
  [db-id id-attr attrs]
  (let [sym         (symbol (str (munge-kw id-attr) "->db-entity-enums"))
        opts        {::pc/input  #{id-attr}
                     ::pc/output (mapv #(hash-map % [:db/ident]) attrs)}
        resolver-fn (fn [{:keys [::rad.env/rdb] :as _env} input]
                      (let [results (rad.db/pull-by-attr rdb db-id id-attr
                                                         (get input id-attr)
                                                         (mapv #(hash-map % [:db/ident]) attrs))]
                        (into {}
                              (keep (fn [[k v]]
                                      (when (:db/ident v)
                                        [k (:db/ident v)])))
                              results)))]
    (pc/resolver sym opts resolver-fn)))


(defn db-entity-ref-resolver
  [db-id id-attr attr->ref-attrs]
  (let [sym         (symbol (str (munge-kw id-attr) "->db-entity-refs"))
        opts        {::pc/input  #{id-attr}
                     ::pc/output (mapv (fn [[k vs]] {k (vec vs)})
                                       attr->ref-attrs)}
        resolver-fn (fn [{:keys [::rad.env/rdb] :as _env} input]
                      (rad.db/pull-by-attr rdb db-id id-attr
                                           (get input id-attr)
                                           (mapv (fn [[k vs]] {k (vec vs)})
                                                 attr->ref-attrs)))]
    (pc/resolver sym opts resolver-fn)))


(defn db-entity-alias-ref-resolver
  [db-id id-attr attr->ref-attrs]
  (let [sym         (symbol (str (munge-kw id-attr) "->db-entity-refs"))
        opts        {::pc/input  #{id-attr}
                     ::pc/output (mapv (fn [[k vs]] {k (vec vs)})
                                       attr->ref-attrs)}
        resolver-fn (fn [{:keys [::rad.env/rdb] :as _env} input]
                      (rad.db/pull-by-attr rdb db-id id-attr
                                           (get input id-attr)
                                           (mapv (fn [[k vs]] {k (vec vs)})
                                                 attr->ref-attrs)))]
    (pc/resolver sym opts resolver-fn)))


(defn db-entity-reverse-ref-resolver
  [db-id id-attr attr->rev+target]
  (let [sym         (symbol (str (munge-kw id-attr) "->db-entity-rev-refs"))
        opts        {::pc/input  #{id-attr}
                     ::pc/output (mapv (fn [[k [_rev target]]] {k [target]})
                                       attr->rev+target)}
        resolver-fn (let [rev->key (into {} (map (fn [[k [rev _]]] [rev k])
                                                 attr->rev+target))]
                      (fn [{:keys [::rad.env/rdb] :as _env} input]
                        (let [db-data (rad.db/pull-by-attr rdb db-id id-attr
                                                           (get input id-attr)
                                                           (mapv (fn [[_ [rev target]]]
                                                                   {rev [target]})
                                                                 attr->rev+target))
                              data    (reduce-kv
                                       (fn [m rev v]
                                         (assoc m (get rev->key rev) v))
                                       {} db-data)]
                          data)))]
    (pc/resolver sym opts resolver-fn)))


(defn compute-attr-resolver
  [new-attr attr-deps compute-fn]
  (let [sym         (symbol (str "compute->" (munge-kw new-attr)))
        opts        {::pc/input  (set attr-deps)
                     ::pc/output [new-attr]}
        resolver-fn (fn [env input]
                      {new-attr (compute-fn env input)})]
    (pc/resolver sym opts resolver-fn)))


(defn- cleanup-index [data]
  (let [xform (fn [[k v]]
                (cond
                  (fn? v)
                  nil

                  (and (map? v)
                       (when-some [attr (:com.wsscode.pathom.connect/attribute v)]
                         (= "com.wsscode.pathom.connect" (and (keyword? attr) (namespace attr)))))
                  nil

                  :else
                  [k v]))
        xf (fn [x]
             (if (map? x)
               (into {} (keep xform x))
               x))]
    (walk/postwalk xf data)))


(pc/defresolver index-explorer [env _]
  {::pc/input  #{:com.wsscode.pathom.viz.index-explorer/id}
   ::pc/output [:com.wsscode.pathom.viz.index-explorer/index]}
  {:com.wsscode.pathom.viz.index-explorer/index
   (->> ::pc/indexes
        (get env)
        (cleanup-index))})


(def index-explorer-registry
  [index-explorer])
