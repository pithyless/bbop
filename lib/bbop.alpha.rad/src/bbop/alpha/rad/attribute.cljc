(ns bbop.alpha.rad.attribute
  (:require [clojure.spec.alpha :as s]))


(s/def ::qid qualified-keyword?)
(s/def ::kind keyword?)
(s/def ::enum-values any?)


(defn enum-idents
  [{:keys [::kind ::enum-values ::qid]}]
  (let [parser (fn [v]
                 (cond
                   (qualified-keyword? v) v
                   (keyword? v)           (keyword (str (namespace qid) "." (name qid))
                                                   (name v))
                   :else                  (:db/ident v)))]
    (when (and (= :enum kind) (seq enum-values))
      (map parser enum-values))))
