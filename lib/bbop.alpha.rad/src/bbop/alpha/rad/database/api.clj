(ns bbop.alpha.rad.database.api
  (:require
   [bbop.alpha.core.guard :refer [>defn =>]]
   [bbop.alpha.rad.database.protocol :as protocol]
   [bbop.alpha.rad.database.mspec :as mspec.rad.db]
   [bbop.alpha.rad.database-adapter.mspec :as mspec.rad.db-adapter]
   [bbop.alpha.rad.attribute.mspec :as mspec.rad.attr]))


(>defn all-migration-txes
  [component attr-registry]
  [mspec.rad.db/pool mspec.rad.db/attr-registry
   => [:map-of mspec.rad.db/schema-id any?]]
  (protocol/all-migration-txes component (vec (flatten attr-registry))))


(>defn migrate-all!
  [component attr-registry]
  [mspec.rad.db/pool mspec.rad.db/attr-registry
   => [:map-of mspec.rad.db/schema-id any?]]
  (protocol/migrate-all! component (vec (flatten attr-registry))))


(>defn connect-all
  ([component]
   [mspec.rad.db/pool => mspec.rad.db/conn]
   (connect-all component {}))
  ([component opts]
   [mspec.rad.db/pool map? => mspec.rad.db/conn]
   (protocol/connect-all component opts)))


(>defn pull-by-attr
  [component db-id id-attr id-or-ids eql-query]
  [mspec.rad.db/conn
   mspec.rad.db/schema-id
   mspec.rad.attr/qid
   [:or mspec.rad.db-adapter/scalar [:sequential mspec.rad.db-adapter/scalar]]
   mspec.rad.db-adapter/eql-query
   => any?]
  (protocol/pull-by-attr component db-id id-attr id-or-ids eql-query))


(>defn query
  ([component db-id query-spec]
   [mspec.rad.db/conn mspec.rad.db/schema-id any? => any?]
   (query component db-id query-spec []))
  ([component db-id query-spec query-inputs]
   [mspec.rad.db/conn mspec.rad.db/schema-id any? [:vector any?] => any?]
   (protocol/query component db-id query-spec query-inputs)))


(>defn transact! [component db-id tx-data]
  [mspec.rad.db/conn mspec.rad.db/schema-id [:vector {:min 1} any?]
   => mspec.rad.db-adapter/tx-result]
  (protocol/transact! component db-id tx-data))
