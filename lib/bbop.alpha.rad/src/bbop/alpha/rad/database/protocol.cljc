(ns ^{:clojure.tools.namespace.repl/unload false
      :clojure.tools.namespace.repl/load    false}
    bbop.alpha.rad.database.protocol)


(defprotocol IRadDBPool
  (all-migration-txes [this new-schema-attrs])
  (migrate-all! [this new-schema-attrs])
  (connect-all [this opts]))


(defprotocol IRadDBPoolConn
  (pull-by-attr [this db-id id-attr id-or-ids eql-query])
  (query [this db-id query-spec query-inputs])
  (transact! [this db-id tx-data]))
