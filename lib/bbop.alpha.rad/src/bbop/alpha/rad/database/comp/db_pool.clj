(ns bbop.alpha.rad.database.comp.db-pool
  (:require
   [bbop.alpha.core.guard :refer [>defn =>]]
   [bbop.alpha.rad.database.mspec :as mspec.rad.db]
   [bbop.alpha.rad.database-adapter.mspec :as mspec.rad.db-adapter]
   [bbop.alpha.rad.database.protocol :as protocol]
   [bbop.alpha.rad.database-adapter.api :as rad.db.adapter]
   [bbop.alpha.rad.database-adapter.protocol :as rad.db.adapter.protocol]
   [bbop.alpha.rad.database.api :as rad.db]
   [bbop.alpha.core.system :as system]
   [bbop.alpha.core.malli :as malli]
   [bbop.alpha.core.mspec :as mspec]))


(defn all-migration-txes* [databases new-schema-attrs]
  (let [attrs-per-db (group-by :bbop.alpha.rad.database.api/alias new-schema-attrs)]
    (into {}
          (for [[db-id attrs] attrs-per-db]
            (when-some [db (get databases db-id)]
              (when-some [schema (seq (rad.db.adapter/migrate-tx db attrs))]
                [db-id (vec schema)]))))))


(defrecord RadDBPoolConn [database-conns]
  protocol/IRadDBPoolConn
  (pull-by-attr [this db-id id-attr id-or-ids eql-query]
    (let [db (get database-conns db-id)]
      (assert db (str "Missing db <" db-id "> at <" ::pull-by-attr ">"))
      (rad.db.adapter/pull-by-attr db id-attr id-or-ids eql-query)))

  (query [this db-id query-spec query-inputs]
    (let [db (get database-conns db-id)]
      (assert db (str "Missing db <" db-id "> at <" ::query ">"))
      (rad.db.adapter/query db query-spec query-inputs)))

  (transact! [this db-id tx-data]
    (let [db (get database-conns db-id)]
      (assert db (str "Missing db <" db-id "> at <" ::transact! ">"))
      (rad.db.adapter/transact! db tx-data))))


(defrecord RadDBPool [databases]
  protocol/IRadDBPool
  (all-migration-txes [this new-schema-attrs]
    (all-migration-txes* databases new-schema-attrs))

  (migrate-all! [this new-schema-attrs]
    (into {}
          (for [[db-id tx-data] (all-migration-txes* databases new-schema-attrs)]
            [db-id (rad.db.adapter/migrate! (get databases db-id) tx-data)])))

  (connect-all [this opts]
    (let [dbs (reduce-kv
               (fn [m k v]
                 (assoc m k (rad.db.adapter/new-conn v (get opts k {}))))
               {}
               databases)]
      (map->RadDBPoolConn {:database-conns dbs}))))


(>defn component-start
  [{:keys [:databases :attr-registries]}]
  [[:map
    [:databases [:map-of mspec.rad.db/schema-id mspec.rad.db-adapter/adapter]]
    [:attr-registries mspec.rad.db/attr-registry]]
   => mspec.rad.db/pool]
  (let [pool (map->RadDBPool {:databases databases})]
    (when (seq attr-registries)
      (rad.db/migrate-all! pool attr-registries))
    pool))


(def component
  (system/lifecycle component-start))


(def databases-component
  (system/lifecycle
   (fn [databases-map]
     databases-map)))
