(ns bbop.alpha.rad.database.mspec
  (:require
   [bbop.alpha.core.mspec.protocol :as mspec.protocol]
   [bbop.alpha.rad.database.protocol :as protocol]))


(def schema-id
  simple-keyword?)


(def attr-registry
  ;; TODO: this can be nested; but leaves are always attrs
  [:vector any?])


(def pool
  (mspec.protocol/satisfies protocol/IRadDBPool))


(def conn
  (mspec.protocol/satisfies protocol/IRadDBPoolConn))
