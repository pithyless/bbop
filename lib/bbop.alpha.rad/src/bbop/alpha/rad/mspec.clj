(ns bbop.alpha.rad.mspec
  (:require
   [clojure.string :as str]
   [edn-query-language.core :as eql]
   [clojure.spec.alpha :as s]))


(def string
  [:and string? [:fn '(fn [x] (not (str/blank? x)))]])


(def eql-query
  [:and vector? [:fn (fn [x] (s/valid? ::eql/query x))]])


(def uuid uuid?)

;; TODO
(def rdb some?)
