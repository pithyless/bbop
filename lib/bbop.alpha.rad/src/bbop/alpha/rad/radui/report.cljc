(ns bbop.alpha.rad.radui.report
  #?(:cljs (:require-macros [bbop.alpha.rad.radui.report]))
  (:require
   [bbop.alpha.fulcro.app :as app]
   [bbop.alpha.core.log :as log]
   [bbop.alpha.core.malli :as malli]
   [bbop.alpha.rad.radui.mspec :as radui.mspec]
   [bbop.alpha.rad.radui.core :as radui]
   [bbop.alpha.rad.radui.controller :as radui.controller]
   [com.fulcrologic.fulcro.ui-state-machines :as uism :refer [defstatemachine]]
   [com.fulcrologic.fulcro.dom :as dom]

   ;; [bbop.alpha.rad.radui.api :as radui.api]
   ))


(defn render-layout [report-instance]
  (let [layout (some-> report-instance app/component-options ::layout ::render-layout)]
    (if-not (fn? layout)
      (log/error ::render-layout {:msg       "Missing or invalid ::render-layout."
                                  :layout    layout
                                  :component (app/component-name report-instance)})
      (layout report-instance))))


;; (defn render-item-layout [item-instance]
;;   (let [layout (some-> item-instance app/component-options ::layout ::render-item-layout)]
;;     (if-not (fn? layout)
;;       (log/error ::render-item-layout {:msg       "Missing or invalid ::render-item-layout."
;;                                        :layout    layout
;;                                        :component (app/component-name item-instance)})
;;       (layout item-instance))))


(defn render-parameter-input [this parameter-key]
  (let [input-type  (some-> this app/component-options ::parameters (get parameter-key))
        input-style :default ; TODO: Support parameter styles
        input       (some-> this app/any->runtime ::radui/controls ::parameter-type->style->input (get-in [input-type input-style]))]
    (if input
      (input this parameter-key)
      (log/error ::missing-parameter-renderer {:component (app/component-name this)
                                               :parameter parameter-key}))))



;; (defn exit-report [{::uism/keys [fulcro-app] :as env}]
;;   (let [Report       (uism/actor-class env :actor/report)
;;         id           (uism/retrieve env ::radui/controller-id)
;;         cancel-route (some-> Report app/component-options ::radui/cancel-route)]
;;     (when-not cancel-route
;;       (log/error ::missing-cancel-route {:report (app/component-name Report)}))
;;     ;; TODO: probably return to original route instead
;;     (radui.controller/route-to! fulcro-app id (or cancel-route []))
;;     (uism/exit env)))


(defn load-report! [app TargetReportClass parameters]
  (let [{:keys [::BodyItem ::source-attribute]} (app/component-options TargetReportClass)
        path                                    (conj (app/get-ident TargetReportClass {}) source-attribute)]
    (log/info ::load-report!
              {:source-attribute source-attribute
               :target           (app/component-name TargetReportClass)
               :item             (app/component-name BodyItem)})
    (app/load! app source-attribute BodyItem {:params parameters :target path})))


(def global-events {})


(defstatemachine report-machine
  {::uism/actors
   #{:actor/report}

   ::uism/aliases
   {}

   ::uism/states
   {:initial
    {::uism/handler (fn [env]
                      (let [{:keys [::uism/event-data]}              env
                            {:keys [::radui/controller-id ::action]} event-data
                            Report                                   (uism/actor-class env :actor/report)]

                        (tap> [::all-attributes (-> env ::uism/fulcro-app (app/external-config ::radui/all-attributes))])
                        (-> env
                            (uism/store ::action action)
                            (uism/store ::radui/controller-id controller-id)
                            (uism/activate :state/gathering-parameters))))}

    :state/gathering-parameters
    (merge global-events
           {::uism/events
            {
             ;; TODO - forms
             ;; :event/parameter-changed {::uism/handler (fn [{::uism/keys [event-data] :as env}]
             ;;                                            ;; NOTE: value at this layer is ALWAYS typed to the attribute.
             ;;                                            ;; The rendering layer is responsible for converting the value to/from
             ;;                                            ;; the representation needed by the UI component (e.g. string)
             ;;                                            (let [{:keys [parameter value]} event-data
             ;;                                                  form-ident                (uism/actor->ident env :actor/report)
             ;;                                                  path                      (when (and form-ident parameter)
             ;;                                                                              (conj form-ident parameter))]
             ;;                                              ;; (when-not path
             ;;                                              ;;   (log/error "Unable to record attribute change. Path cannot be calculated."))
             ;;                                              (cond-> env
             ;;                                                path (uism/apply-action assoc-in path value))))}
             :event/run
             {::uism/handler (fn [{:keys [::uism/fulcro-app ::uism/state-map ::uism/event-data] :as env}]
                               (let [Report         (uism/actor-class env :actor/report)
                                     report-ident   (uism/actor->ident env :actor/report)
                                     desired-params (some-> Report app/component-options ::parameters keys set)
                                     _              (tap> [::event-run-report
                                                           {:report         (app/component-name Report)
                                                            :report-ident   report-ident
                                                            :state          (get-in state-map report-ident)
                                                            :desired-params desired-params}])
                                     current-params (merge
                                                     (select-keys (get-in state-map report-ident) desired-params)
                                                     event-data)]
                                 (load-report! fulcro-app Report current-params)
                                 env))}}})}})


(defmethod radui.controller/-start-io! ::report-type
  [{:keys [::uism/fulcro-app] :as env} TargetClass {:keys [::radui/target-route] :as options}]
  (tap> [::start-io {:name (app/component-name TargetClass)}])
  (let [report-machine-id (app/ident TargetClass {})
        event-data        (assoc options ::id report-machine-id)]
    (uism/begin! fulcro-app report-machine report-machine-id
                 {:actor/report (uism/with-actor-class report-machine-id TargetClass)}
                 event-data)
    (radui.controller/activate-route env target-route)))


(def ui-keyword-spec
  [:and qualified-keyword?
   [:fn {:error/message "ui-keyword must be namespaced `:ui/*`"}
    (fn [x] (= "ui" (namespace x)))]])


(defmacro defsc-report-item
  [sym arglist & args]
  (let [this-sym    (first arglist)
        custom-body (rest args)
        options     (first args)
        new-options (cond-> options
                      (seq custom-body) (dissoc ::columns))
        body        (if (seq custom-body)
                      custom-body
                      [])]
    (malli/assert [:map
                   [::columns [:vector qualified-keyword?]]
                   ;; [::column-headings #_{:optional? true} [:vector string?]]
                   [:ident some?]
                   [:query some?]]
                  options
                  "Invalid defsc-report-item macro usage.")
    `(app/defsc ~sym ~arglist ~new-options ~@body)))


(defmacro defsc-report
  [sym arglist & args]
  (let [this-sym        (first arglist)
        {:keys [::BodyItem ::source-attribute ::route ::parameters]
         :as   options} (first args)
        subquery        `(app/get-query ~BodyItem)
        query           (into [{source-attribute subquery}]
                              (keys parameters))
        options         (assoc options
                               ::radui/io? true
                               :route-segment [route]
                               ::radui/type ::report-type
                               :query query
                               :ident (list 'fn [] [:component/id (keyword sym)]))
        body            (if (seq (rest args))
                          (rest args)
                          [`(render-layout ~this-sym)])]
    (malli/assert [:map
                   [::BodyItem symbol?]
                   [::source-attribute keyword?]
                   [::route string?]
                   [::parameters {:optional? true} [:map-of ui-keyword-spec any?]]
                   [::layout some?]]
                  options
                  "Invalid defsc-report macro usage.")
    `(app/defsc ~sym ~arglist ~options ~@body)))
