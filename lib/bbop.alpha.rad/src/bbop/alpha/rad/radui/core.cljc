(ns bbop.alpha.rad.radui.core
  (:require
   [clojure.spec.alpha :as s]))



(s/def ::all-attributes (s/map-of qualified-keyword? (s/keys)))

(s/def ::target-route any?)
(s/def ::cancel-route any?)
(s/def ::controls any?)

(s/def ::id any?)
(s/def ::tempid any?)

(s/def ::controller-id any?)

(s/def ::io? boolean?)
(s/def ::type qualified-keyword?)



(s/def ::attr-spec (s/keys))
