(ns bbop.alpha.rad.radui.form2.machine
  (:require
   [bbop.alpha.core.guard :refer [>defn =>]]
   [bbop.alpha.fulcro.app :as app]
   [bbop.alpha.fulcro.algo :as algo]
   [bbop.alpha.rad.radui.core :as radui]
   [bbop.alpha.core.malli :as malli]
   [bbop.alpha.rad.radui.mspec :as radui.mspec]
   [bbop.alpha.rad.radui.form2.core :as radui.form2]
   [bbop.alpha.rad.radui.form2.mspec :as radui.form2.mspec]
   [com.fulcrologic.fulcro.ui-state-machines :as uism :refer [defstatemachine]]
   [com.fulcrologic.fulcro.algorithms.form-state :as fulcro.form-state]

   [bbop.alpha.core.log :as log]))


(def declare-keywords [::action ::new-route])


(defn start-create [_env]
  (assert nil ::not-implemented))


(defn start-edit [env]
  (let [FormClass  (uism/actor-class env :actor/form)
        form-ident (uism/actor->ident env :actor/form)]
    (tap> [::start-edit form-ident])
    (-> env
        (uism/load form-ident FormClass {::uism/ok-event    :event/loaded
                                         ::uism/error-event :event/failed})
        (uism/activate :state/loading))))


(>defn handle-event-uism-started
  [env]
  [[:map
    [::uism/event-data [:map
                        [::radui.form2/action radui.form2.mspec/action]]]]
   => radui.form2.mspec/env]
  (let [{:keys [::uism/event-data]}    env
        {:keys [::radui.form2/action]} event-data]
    (case action
      :edit   (start-edit env)
      :create (start-create env))))


(defn handle-event-exit
  [{:keys [::uism/event-data ::uism/fulcro-app] :as env}]
  (let [route (::new-route event-data)]
    (when route
      (app/change-route fulcro-app route))
    (uism/exit env)))


(defn handle-event-will-leave [env]
  ;; FIXME: implement
  ;; TODO: Handle the controller asking if it is OK to abort this edit
  env)


(defn handle-event-form-abandoned [env]
  ;; FIXME: implement
  ;; TODO: hook this up in controller
  (uism/exit env))


(defn rad-attrs [env]
  (uism/retrieve env ::radui/all-attributes))


(defn handle-event-route-denied [env]
  #?(:cljs (js/alert "Editing in progress"))
  env)


(defn handle-event-loaded
  "Loading complete. Mark the form complete."
  [env]
  (let [FormClass  (uism/actor-class env :actor/form)
        form-ident (uism/actor->ident env :actor/form)]
    (-> env
        (uism/apply-action fulcro.form-state/add-form-config* FormClass form-ident)
        (uism/apply-action fulcro.form-state/mark-complete* form-ident)
        (app/uism-target-ready form-ident)
        (uism/activate :state/editing))))


(defn handle-event-failed [env]
   ;; FIXME: error handling
   #?(:cljs (js/alert "Load failed"))
   env)


(defn handle-event-attribute-changed [env]
  ;; NOTE: value at this layer is ALWAYS typed to the attribute.
  ;; The rendering layer is responsible for converting the value to/from
  ;; the representation needed by the UI component (e.g. string)
  (let [{:keys [::uism/event-data]}       env
        {:keys [:qid :value :form-ident]} event-data
        path                              (conj form-ident qid)
        mark-complete?                    true]
    (cond-> env
      mark-complete? (uism/apply-action fulcro.form-state/mark-complete* form-ident qid)
      path           (uism/apply-action assoc-in path value))))


(defn handle-event-attribute-blurred [env]
  env)


(defn handle-event-reset [env]
  (let [form-ident (uism/actor->ident env :actor/form)]
    (uism/apply-action env fulcro.form-state/pristine->entity* form-ident)))


(>defn calc-diff
  [{:keys [::uism/state-map] :as env}]
  [radui.form2.mspec/env => [:map [::radui.form2/delta any?]]]
  (let [form-ident (uism/actor->ident env :actor/form)
        Form       (uism/actor-class env :actor/form)
        props      (algo/ui->props state-map Form form-ident)
        delta      (fulcro.form-state/dirty-fields props true)]
    (tap> [::radui.form2/delta delta])
    {::radui.form2/delta delta}))


(defn handle-event-save
  [{:keys [::uism/event-data] :as env}]
  (let [Form         (uism/actor-class env :actor/form)
        attr-id      (-> Form app/component-options ::radui.form2/attr-id)
        data-to-save (calc-diff env)
        params       (merge event-data data-to-save)]
    (-> env
        (uism/trigger-remote-mutation
         :actor/form 'bbop.alpha.rad.radui.action/save-form
         (merge params
                {::uism/error-event :event/save-failed
                 ::uism/ok-event    :event/saved
                 ::radui.form2/attr-id attr-id}
                (app/returning Form)))
        (uism/activate :state/saving))))


(defn handle-event-save-failed
  [env]
  ;; TODO: handle errors
  #?(:cljs (js/alert "Failed to save."))
  (uism/activate env :state/editing))


(defn handle-event-saved
  [env]
  (let [form-ident (uism/actor->ident env :actor/form)]
    (-> env
        (uism/apply-action fulcro.form-state/entity->pristine* form-ident)
        (uism/activate :state/editing))))


(def all-events
{::uism/started           {::uism/handler handle-event-uism-started}
 :event/will-leave        {::uism/handler handle-event-will-leave}
 :event/form-abandoned    {::uism/handler handle-event-form-abandoned}
 :event/exit              {::uism/handler handle-event-exit}
 :event/route-denied      {::uism/handler handle-event-route-denied}
 :event/loaded            {::uism/handler handle-event-loaded}
 :event/failed            {::uism/handler handle-event-failed}
 :event/reset             {::uism/handler handle-event-reset}
 :event/attribute-changed {::uism/handler handle-event-attribute-changed}
 :event/blur              {::uism/handler handle-event-attribute-blurred}
 :event/save              {::uism/handler handle-event-save}
 :event/save-failed       {::uism/handler handle-event-save-failed}
 :event/saved             {::uism/handler handle-event-saved}
 })


(def default-events
  [:event/exit :event/route-denied])


(defstatemachine form-machine
  {::uism/actors
   #{:actor/form}

   ::uism/aliases
   {:new?                 [:actor/form :ui/new?]
    :confirmation-message [:actor/form :ui/confirmation-message]}

   ::uism/states
   {:initial
    {::uism/events (select-keys all-events [::uism/started])}

    :state/loading
    {::uism/events (select-keys all-events (into default-events [:event/loaded
                                                                 :event/failed]))}

    :state/editing
    {::uism/events (select-keys all-events (into default-events [:event/attribute-changed
                                                                 :event/blur
                                                                 :event/reset
                                                                 :event/save]))}

    :state/saving
    {::uism/events (select-keys all-events (into default-events [:event/save-failed
                                                                 :event/saved]))}
    }})
