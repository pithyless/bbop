(ns bbop.alpha.rad.radui.form2.core
  (:require
   [clojure.spec.alpha :as s]))


(s/def ::action #{:create :edit})

(s/def ::render-layout fn?)

(s/def ::layout (s/keys :req [::render-layout]))

(s/def ::master-form any?)

(s/def ::form-instance any?)

(s/def ::delta any?)
