(ns bbop.alpha.rad.radui.form2.api
  (:require
   [com.fulcrologic.fulcro.ui-state-machines :as uism]
   [bbop.alpha.rad.radui.form2.core :as radui.form2]
   [bbop.alpha.fulcro.app :as app]))


(defn change-attr!
  [{:keys [::radui.form2/form-instance
           ::radui.form2/master-form]}
                      qid value]
  (let [form-ident (app/get-ident form-instance)
        asm-id     (app/get-ident master-form)]
    (uism/trigger! master-form asm-id :event/attribute-changed
                   {:qid        qid
                    :form-ident form-ident
                    :value      value})))


(defn blur-attr!
  [{:keys [::radui.form2/form-instance
           ::radui.form2/master-form]}
   qid value]
  (let [form-ident (app/get-ident form-instance)
        asm-id     (app/get-ident master-form)]
    (uism/trigger! master-form asm-id :event/blur
                   {:qid        qid
                    :form-ident form-ident
                    :value      value})))


(defn reset-form!
  [{:keys [::radui.form2/master-form]}]
  (let [asm-id (app/get-ident master-form)]
    (uism/trigger! master-form asm-id :event/reset {})))


(defn save-form!
  [{:keys [::radui.form2/master-form]}]
  (let [asm-id (app/get-ident master-form)]
    (uism/trigger! master-form asm-id :event/save {})))
