(ns bbop.alpha.rad.radui.form2.macro
  #?(:cljs (:require-macros [bbop.alpha.rad.radui.form2.macro]))
  (:require
   [bbop.alpha.rad.radui.core :as radui]
   [bbop.alpha.rad.radui.api :as radui.api]
   [bbop.alpha.core.contrib :as contrib]
   [bbop.alpha.fulcro.app :as app]
   [bbop.alpha.core.uuid :as uuid]
   [bbop.alpha.fulcro.algo :as algo]
   [bbop.alpha.core.log :as log]
   [bbop.alpha.rad.radui.form2.core :as radui.form2]
   [bbop.alpha.rad.radui.form2.machine :refer [form-machine]]
   [bbop.alpha.core.namespace :refer [cljs-env?]]
   [com.fulcrologic.fulcro.algorithms.form-state :as fulcro.form]
   [com.fulcrologic.fulcro.components :as fulcro.comp]
   [com.fulcrologic.fulcro.ui-state-machines :as uism :refer [defstatemachine]]

   #?(:clj [com.fulcrologic.fulcro.dom-server :as dom]
      :cljs [com.fulcrologic.fulcro.dom :as dom])

   [clojure.spec.alpha :as s]
   ))


(defn form-will-enter
  [app route-params form-class]
  ;;; TODO - obj-id must be UUID format for now
  (let [{:keys [:action :id]}           route-params
        action                          (case action
                                          "edit"   :edit
                                          "create" :create)
        uid                             (if (= :create action)
                                          (algo/tempid (uuid/parse id))
                                          (uuid/parse id))
        {:keys [::radui.form2/attr-id]} (app/component-options form-class)
        form-ident                      [attr-id uid]]
    (app/route-deferred form-ident
                        (fn []
                          (uism/begin! app form-machine
                                       form-ident
                                       {:actor/form (uism/with-actor-class form-ident form-class)}
                                       {::radui.form2/action action})))))


(defn form-will-leave
  [this form-props]
  (let [id         (app/get-ident this)
        abandoned? (= :state/abandoned (uism/get-active-state this id))
        dirty?     (and (not abandoned?)
                        (fulcro.form/dirty? form-props))]
    (when dirty? (uism/trigger! this id :event/route-denied))
    (not dirty?)))


;; NOTE: This MUST be used within a lambda in the component, not as a static bit of query at compile time.
(defn form-options->form-query
  "Converts form options to a proper EQL query."
  [form-options]
  (let [{:keys [::radui.form2/attributes
                ::radui.form2/attr-id]
         }                 form-options
        ;; {refs true scalars false} (group-by #(= :ref (::attr/type %)) attr)
        scalars            attributes
        query-with-scalars (into
                            [attr-id :ui/new? :ui/confirmation-message [::uism/asm-id '_] fulcro.form/form-config-join]
                            scalars)
        ;; subforms           (::subforms form-options)
        ;; full-query         (into query-with-scalars
        ;;                          (map (fn [{::attr/keys [qualified-key target]}]
        ;;                                 (required! (str "Form attribute " qualified-key
        ;;                                                 " is a reference type. The ::form/subforms map")
        ;;                                            subforms qualified-key #(contains? % ::ui))
        ;;                                 (let [subform (get-in subforms [qualified-key ::ui])]
        ;;                                   {qualified-key (app/get-query subform)})))
        ;;                          refs)
        full-query         query-with-scalars
        ]
    full-query))


(defn convert-options [get-class options]
  (let [{:keys [::radui.form2/attributes
                ::radui.form2/route-prefix
                ::radui.form2/attr-id]} options
        ;; form-field?                           (fn [{::attr/keys [identity?]}] (not identity?))
        ]
    (merge options
           {:query         (fn [this] (form-options->form-query (app/component-options this)))
            :ident         (fn [_ props] [attr-id (get props attr-id)])
            :form-fields   (->> attributes
                                ;; TODO! - need to filter out refs
                                ;; (filter form-field?)
                                ;; (map ::attr/qualified-key)
                                (into #{}))
            :route-segment [route-prefix :action :id]
            :will-leave    form-will-leave
            :will-enter    (fn [app route-params]
                             (form-will-enter app route-params (get-class)))})))


#?(:clj
   (defn make-render-form [sym arglist body]
     (let [[thissym propsym computedsym extra-args] arglist]
       (#'com.fulcrologic.fulcro.components/build-render
        sym thissym propsym computedsym extra-args body))))


(defn master-form
  "Return the master form for the given component instance."
  [component]
  (or (some-> component app/get-computed ::radui.form2/master-form) component))


(defn render-layout [form-instance]
  (let [layout (some-> form-instance app/component-options ::radui.form2/layout ::radui.form2/render-layout)]
    (tap> [::foo :bbop.alpha.rad.radui.form2/render-layout ::radui.form2/render-layout])
    (tap> [::render-layout (some-> form-instance app/component-options ::radui.form2/layout)])

    (if-not (fn? layout)
      (log/error ::render-layout
                 {:msg       "Missing or invalid ::radui.form2/render-layout."
                  :layout    layout
                  :component (app/component-name form-instance)})
      (layout {::radui.form2/master-form   (master-form form-instance)
               ::radui.form2/form-instance form-instance}))))


;; (defn render-layout [form-instance]
;;   ;; (let [props ])
;;   (dom/div
;;     "render-form-layout"
;;     ;; (pr-str (keys props))

;;     #_(map #(dom/div (pr-str %)) props))
;;   ;; (let [{::app/keys [runtime-atom]} (app/any->app form-instance)
;;   ;;       cprops                      (app/get-computed props)
;;   ;;       rendering-env               cprops
;;   ;;       layout-style                (or (some-> form-instance app/component-options ::layout-style) :default)
;;   ;;       layout                      (some-> runtime-atom deref :com.fulcrologic.rad/controls ::style->layout layout-style)]
;;   ;;   (if layout
;;   ;;     (layout (merge rendering-env
;;   ;;                    {::master-form    (master-form form-instance)
;;   ;;                     ::form-instance  form-instance
;;   ;;                     ::props          props
;;   ;;                     ::computed-props cprops}))
;;   ;;     (do
;;   ;;       (log/error "No layout function found for form layout style" layout-style)
;;   ;;       nil)))
;;   )

(defn form-body [arglist body]
  (if (empty? body)
    `[(render-layout ~(first arglist))]
    body))


(s/def ::defsc-form-args (s/cat
                          :sym symbol?
                          :doc (s/? string?)
                          :arglist (s/and vector? #(<= 2 (count %) 5))
                          :options map?
                          :body (s/* any?)))


#?(:clj
   (defn defsc-form*
     [env args]
     (let [{:keys [:sym :doc :arglist :options :body]
            :as   _conformed} (s/conform ::defsc-form-args args)
           ;; TODO - throw error if not conformed
           nspc              (if (cljs-env? env) (-> env :ns :name str) (name (ns-name *ns*)))
           body              (form-body arglist body)
           qualified-sym     (keyword (str nspc) (name sym))
           render-form       (make-render-form sym arglist body)
           ]
       (if (cljs-env? env)
         `(do
            (declare ~sym)
            (let [get-class# (fn [] ~sym)
                  options#   (assoc (convert-options get-class# ~options) :render ~render-form)]
              (defonce ~(vary-meta sym assoc :doc doc :jsdoc ["@constructor"])
                (fn [props#]
                  (cljs.core/this-as this#
                    (if-let [init-state# (get options# :initLocalState)]
                      (set! (.-state this#) (cljs.core/js-obj "fulcro$state" (init-state# this# (goog.object/get props# "fulcro$value"))))
                      (set! (.-state this#) (cljs.core/js-obj "fulcro$state" {})))
                    nil)))
              (com.fulcrologic.fulcro.components/configure-component! ~sym ~qualified-sym options#)))
         `(do
            (declare ~sym)
            (let [get-class# (fn [] ~sym)
                  options#   (assoc (convert-options get-class# ~options) :render ~render-form)]
              (def ~(vary-meta sym assoc :doc doc :once true)
                (com.fulcrologic.fulcro.components/configure-component! ~(str sym) ~qualified-sym options#)))))
       )))


(defmacro defsc-form [& args]
  (try
    nil
    (defsc-form* &env args)
    (catch Exception e
      (if (contains? (ex-data e) :tag)
        (throw e)
        (throw (ex-info "Invalid defsc-form." e))))))
