(ns bbop.alpha.rad.radui.action
  (:require
   [com.wsscode.pathom.connect :as pc]
   [com.rpl.specter :as specter]
   [bbop.alpha.core.log :as log]
   [bbop.alpha.fulcro.app :as app]
   [bbop.alpha.rad.crud.action :as crud.action]
   [bbop.alpha.rad.env :as rad.env]
   [bbop.alpha.rad.database.api :as rad.db]

   [com.fulcrologic.rad.form :as form]
   ))


#?(:clj
   (def save-form
     (pc/mutation `form/save-form
                  {::pc/params #{::form/master-pk ::form/delta}}
                  (fn save-form [env params]
                    (let [{:keys [::rad.env/qid->attr
                                  ::rad.env/rdb]} env
                          {:keys [::form/master-pk
                                  ::form/delta]}  params
                          idents                  (keys delta)
                          uid                     (specter/select-first
                                                   [specter/ALL #(= master-pk (first %)) specter/LAST]
                                                   idents)
                          rdb-txes                (crud.action/rdb-delta-txes qid->attr delta)]
                      (doseq [[dbalias txes] (:txes rdb-txes)]
                        (rad.db/transact! rdb dbalias txes))
                      {master-pk uid
                       :tempids  (:tempids rdb-txes)}))))
   :cljs
   (app/defmutation save-form [_]
     (action [_] :noop)))


#?(:clj
   (def pathom-registry
     [save-form]))
