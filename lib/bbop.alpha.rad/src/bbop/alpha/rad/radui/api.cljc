(ns bbop.alpha.rad.radui.api
  (:require
   [bbop.alpha.fulcro.app :as app]
   [bbop.alpha.rad.radui.core :as radui]
   [com.fulcrologic.fulcro.ui-state-machines :as uism]))


(defn route-to!
  "Tell the controller to route the application to the given path."
  [app machine-id path]
  (uism/trigger! app machine-id :event/route {::radui/target-route path}))


(defn run-report!
  "Run a report with the current parameters"
  [this]
  (uism/trigger! this (app/get-ident this) :event/run))


(defn install-ui-controls!
  "Install the given control set as the RAD UI controls used for rendering forms. This should be called before mounting
  your app. The `controls` is just a map from data type to a sub-map that contains a :default key, with optional
  alternate renderings for that data type that can be selected with `::form/field-style {attr-key style-key}`."
  [app controls]
  (let [{:keys [:com.fulcrologic.fulcro.application/runtime-atom]} app]
    (swap! runtime-atom assoc ::radui/controls controls)))


(defn edit!
  "Route to the given form for editing the entity with the given ID."
  [this machine-id form-class entity-id]
  ;; FIXME: need to figure out base path
  (route-to! this machine-id
             (app/path-to form-class {:action "edit"
                                      :id     entity-id})))

(defn attr-spec
  ([app-ish qid]
   (let [spec (some-> app-ish
                      (app/any->app)
                      (app/external-config ::radui/all-attributes)
                      (get qid))]
     (if spec
       spec
       (throw (ex-info "Cannot find attr-spec" {:app-ish app-ish :qid qid})))))
  ([app-ish qid kw]
   (let [attr (attr-spec app-ish qid)
         value (get attr kw ::sentinel)]
     (if (= value ::sentinel)
       (throw (ex-info "Cannot find attr-spec key" {:app-ish app-ish :qid qid :key kw}))
       value))))
