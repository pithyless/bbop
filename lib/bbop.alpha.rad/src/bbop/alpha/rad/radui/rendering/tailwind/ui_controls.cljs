(ns bbop.alpha.rad.radui.rendering.tailwind.ui-controls
  (:require
   [bbop.alpha.rad.radui.rendering.tailwind.report :as tailwind-report]
   ))


(def all-controls
  {;; Form-related UI
   ;; :com.fulcrologic.rad.form/style->layout        {:default sui-form/ui-render-layout}
   ;; :com.fulcrologic.rad.form/type->style->control {:layout  {:default sui-form/ui-render-layout}
   ;;                                                 :text    {:default text-field/render-field}
   ;;                                                 :enum    {:default enumerated-field/render-field}
   ;;                                                 :string  {:default text-field/render-field}
   ;;                                                 :boolean {:default boolean-field/render-field}
   ;;                                                 :instant {:default instant/render-field}}

   ;; Report-related controls
   :bbop.alpha.rad.radui.report/style->layout {:default tailwind-report/ui-render-layout}
   ;; :bbop.alpha.rad.radui.report/parameter-type->style->input {:boolean {:default boolean-input/render-input}}
   })
