(ns bbop.alpha.rad.radui.rendering.tailwind.report
  (:require
   [clojure.string :as str]
   [com.fulcrologic.fulcro.dom :as dom]
   [bbop.alpha.fulcro.app :as app]
   [bbop.alpha.rad.radui.api :as radui.api]
   [bbop.alpha.rad.radui.report :as radui.report]
   #_[com.fulcrologic.rad.form :as form]))

(defn ui-render-layout [this]
  (let [{::radui.report/keys [source-attribute BodyItem parameters]} (app/component-options this)
        {::radui.report/keys [columns column-headings edit-form]}    (app/component-options BodyItem)
        ;; id-key                                                 (some-> edit-form app/component-options ::form/id ::attr/qualified-key)
        props                                                        (app/props this)
        rows                                                         (get props source-attribute [])]
    ;; (log/info "Rendering report layout")
    (dom/div
      (dom/div :.ui.top.attached.segment
        (dom/h3 :.ui.header
          (or (some-> this app/component-options ::radui.report/title) "Report")
          (dom/button :.ui.tiny.right.floated.primary.button {:onClick (fn [] (radui.api/run-report! this))} "Run!"))
        (dom/div :.ui.form
          (map-indexed
           (fn [idx k]
             (dom/div :.ui.inline.field {:key idx}
               (dom/label (some-> k name str/capitalize))
               (radui.report/render-parameter-input this k)))
           (keys parameters))))
      (dom/div :.ui.attached.segment
        (when (seq rows)
          (if (seq columns)
            (dom/table :.table-fixed.table-striped
              (dom/thead
                (dom/tr
                  (if (seq column-headings)
                    (map-indexed
                     (fn [idx h]
                       (dom/th :.text-xs.tracking-wide.font-bold.text-grey-dark {:key idx}
                         h))
                     column-headings)
                    (map-indexed
                     (fn [idx k]
                       (dom/th :.text-xs.tracking-wide.font-bold.text-grey-dark {:key idx}
                         (or
                          #_(some-> k attr/key->attribute ::radui.report/column-header)
                          (some-> k name str/capitalize))))
                     columns))))
              (dom/tbody
                (map-indexed (fn [idx row]
                               (dom/tr {:key (str "row-" idx)}
                                 (map-indexed
                                  (fn [idx k]
                                    (dom/td {:key (str "col-" idx)}
                                      ;; TODO: Coercion
                                      (let [label (str (get row k))]
                                        (if (and edit-form (= 0 idx))
                                          (dom/a {:onClick (fn [] #_(form/edit! this edit-form (get row id-key)))} label)
                                          label))))
                                  columns))) rows)))
            (let [factory (app/factory BodyItem)]
              (dom/div :.ui.list
                (mapv factory rows)))))))))
