(ns bbop.alpha.rad.radui.rendering.semantic-ui.controls
  (:require
   [com.fulcrologic.rad.rendering.semantic-ui.semantic-ui-controls :as semantic-ui-controls]
   [com.fulcrologic.rad.rendering.semantic-ui.int-field :as int-field]
   [bbop.alpha.rad.radui.rendering.semantic-ui.keyword-field :as keyword-field]
   [bbop.alpha.rad.radui.rendering.semantic-ui.text-area :as text-area]))

;; https://github.com/fulcrologic/fulcro-rad/blob/develop/src/main/com/fulcrologic/rad/rendering/semantic_ui/semantic_ui_controls.cljc


(def all-controls
  (-> semantic-ui-controls/all-controls
      (assoc-in [:com.fulcrologic.rad.form/type->style->control :long :default]
                int-field/render-field)
      (assoc-in [:com.fulcrologic.rad.form/type->style->control :keyword :default]
                keyword-field/render-field)
      (assoc-in [:com.fulcrologic.rad.form/type->style->control :text :default]
                text-area/render-field)))
