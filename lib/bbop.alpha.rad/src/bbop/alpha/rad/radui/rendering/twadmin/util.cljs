(ns bbop.alpha.rad.radui.rendering.twadmin.util
  (:require
   [clojure.string :as str]
   [camel-snake-kebab.core :as kebab]))


(defn titleize [s]
  (-> s
      (kebab/->PascalCase)
      (str/replace #"(\w+[^A-Z])([A-Z]+)" "$1 $2")))
