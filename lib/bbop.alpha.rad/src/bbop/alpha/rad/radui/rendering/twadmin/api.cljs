(ns bbop.alpha.rad.radui.rendering.twadmin.api
  (:require
   [bbop.alpha.rad.radui.rendering.twadmin.report]
   [bbop.alpha.rad.radui.rendering.twadmin.form]
   [bbop.alpha.rad.radui.rendering.twadmin.view-field :as view-field]
   [bbop.alpha.rad.radui.rendering.twadmin.edit-field :as edit-field]
   ))



(def report
  {:bbop.alpha.rad.radui.report/render-layout #'bbop.alpha.rad.radui.rendering.twadmin.report/ui-render-layout
   :bbop.alpha.rad.radui.report/kind->layout  {:string  #'view-field/render-text-field
                                               :keyword #'view-field/render-keyword-field
                                               :enum    #'view-field/render-enum-field}})


(def form
  {:bbop.alpha.rad.radui.form2.core/render-layout #'bbop.alpha.rad.radui.rendering.twadmin.form/ui-render-layout
   :bbop.alpha.rad.radui.form2.core/kind->layout  {:string  #'edit-field/render-text-field
                                                   :keyword #'edit-field/render-keyword-field
                                                   :enum    #'edit-field/render-enum-field}
   })


;; (def all-controls
;;   {;; Form-related UI
;;    ;; :com.fulcrologic.rad.form/style->layout        {:default sui-form/ui-render-layout}
;;    ;; :com.fulcrologic.rad.form/type->style->control {:layout  {:default sui-form/ui-render-layout}
;;    ;;                                                 :text    {:default text-field/render-field}
;;    ;;                                                 :enum    {:default enumerated-field/render-field}
;;    ;;                                                 :string  {:default text-field/render-field}
;;    ;;                                                 :boolean {:default boolean-field/render-field}
;;    ;;                                                 :instant {:default instant/render-field}}

;;    ;; Report-related controls
;;    :bbop.alpha.rad.radui.report/style->layout {:default tailwind-report/ui-render-layout}
;;    ;; :bbop.alpha.rad.radui.report/parameter-type->style->input {:boolean {:default boolean-input/render-input}}
;;    })
