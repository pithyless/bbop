(ns bbop.alpha.rad.radui.rendering.twadmin.edit-field
  (:require
   [bbop.alpha.core.string :as string]
   [bbop.alpha.rad.radui.form2.core :as radui.form2]
   [bbop.alpha.rad.radui.form2.api :as radui.form2.api]
   [bbop.alpha.rad.attribute :as rad.attr]
   [bbop.alpha.rad.radui.api :as radui.api]
   [bbop.alpha.fulcro.app :as app]
   [bbop.alpha.fulcro.algo :as algo ]
   [bbop.alpha.rad.radui.rendering.twadmin.util :as twadmin.util]
   [com.fulcrologic.fulcro.dom :as dom]
   [com.fulcrologic.fulcro.dom.events :as dom.event]
   [cljs-bean.core :as bean]
   ["react-select" :default Select]
   ))


(def ui-react-select
  (algo/react-factory Select))


(defn render-text-field
  [{:keys [::radui.form2/form-instance] :as env} qid]
  (let [props (app/props form-instance)
        value (or (get props qid) "")]
    (dom/div
      {:key (str qid)}
      (dom/label :.block
        (dom/span :.text-gray-700
          (twadmin.util/titleize (name qid)))
        (dom/input :.form-input.mt-1.block.w-full
          {:type  :text
           :value value
           :onBlur (fn [evt]
                     (radui.form2.api/blur-attr!
                      env qid (dom.event/target-value evt)))
           :onChange (fn [evt]
                       (radui.form2.api/change-attr!
                        env qid (dom.event/target-value evt)))
           })))))


(defn render-keyword-field
  [{:keys [::radui.form2/form-instance] :as env} qid]
  (let [props (app/props form-instance)
        value (or (get props qid) "")]
    (dom/div
      {:key (str qid)}
      (dom/label :.block
        (dom/span :.text-gray-700
          (twadmin.util/titleize (name qid)))
        (dom/input :.form-input.mt-1.block.w-full
          {:type     :text
           :value    (str value)
           :onBlur   (fn [evt]
                       (radui.form2.api/blur-attr!
                        env qid (string/parse-keyword (dom.event/target-value evt))))
           :onChange (fn [evt]
                       (radui.form2.api/change-attr!
                        env qid (string/parse-keyword (dom.event/target-value evt))))
           })))))


(defn render-enum-field
  [{:keys [::radui.form2/form-instance] :as env} qid]
  (let [props       (app/props form-instance)
        enum-idents (rad.attr/enum-idents (radui.api/attr-spec form-instance qid))
        value-kw    (get-in props [qid :db/ident])
        options     (mapv #(hash-map :value (str %) :label (name %))
                          enum-idents)]
    (tap> [::current-value value-kw options])
    (dom/div
      {:key (str qid)}
      (dom/label :.block
        (dom/span :.text-gray-700
          (twadmin.util/titleize (name qid)))
        (ui-react-select
         {:onChange (fn [choice]
                      (tap> [::change choice])
                      (when-some [value (some-> (:value (bean/bean choice)) (string/parse-keyword))]
                        (tap> [::change! qid choice value])
                        (radui.form2.api/change-attr! env qid {:db/ident value})))
          :value    {:value (str value-kw) :label (name value-kw)}
          :options  options})))))
