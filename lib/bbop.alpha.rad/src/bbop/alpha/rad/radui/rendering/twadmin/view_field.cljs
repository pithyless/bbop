(ns bbop.alpha.rad.radui.rendering.twadmin.view-field
  (:require
   [bbop.alpha.core.string :as string]
   [bbop.alpha.rad.radui.core :as radui]
   [bbop.alpha.rad.radui.rendering.twadmin.util :as twadmin.util]
   [com.fulcrologic.fulcro.dom :as dom]))


(defn render-text-field
  [{:keys [::radui/attr-spec]} value]
  (dom/div value))


(defn render-keyword-field
  [{:keys [::radui/attr-spec]} value]
  (dom/div value))


(defn render-enum-field
  [{:keys [::radui/attr-spec]} value]
  (dom/div (some-> (:db/ident value)
                   (name)
                   (twadmin.util/titleize))))
