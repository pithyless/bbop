(ns bbop.alpha.rad.radui.rendering.twadmin.report
  (:require
   [com.fulcrologic.fulcro.dom :as dom]
   [bbop.alpha.fulcro.app :as app]
   [bbop.alpha.rad.radui.api :as radui.api]
   [bbop.alpha.rad.radui.core :as radui]
   [bbop.alpha.rad.radui.report :as radui.report]
   [bbop.alpha.rad.radui.rendering.twadmin.util :as twadmin.util]
   [bbop.alpha.rad.radui.form2.core :as radui.form2]
   [bbop.alpha.rad.radui.api :as radui.api]
   [bbop.alpha.rad.attribute :as rad.attr]
   [bbop.alpha.core.log :as log]
   ))


;; (defn column-names [columns column-headings]
;;   ;; TODO: (some-> k attr/key->attribute ::radui.report/column-header)
;;   (if (seq column-headings)
;;     column-headings
;;     (map #(some-> % name titleize) columns)))


(defn column-name [column-options column]
  (or (get-in column-options [column :title])
      (some-> column name twadmin.util/titleize)))


(defn report-title [this report-opts]
  (or (some-> report-opts ::radui.report/title)
      (str (-> this app/component-name keyword name twadmin.util/titleize))))


(defn render-attribute
  [report-instance qid value]
  (let [attr-spec (radui.api/attr-spec report-instance qid)
        kind      (radui.api/attr-spec report-instance qid ::rad.attr/kind)
        render-fn (some-> report-instance app/component-options ::radui.report/layout ::radui.report/kind->layout kind)]
    (tap> [::render kind render-fn])
    (if render-fn
      (dom/div {}
        (render-fn {::radui/attr-spec attr-spec} value))
      (log/error "Missing report render-fn for attribute" {:qid qid :kind kind}))))


(defn ui-render-layout [this]
  (let [{:keys [::radui.report/BodyItem
                ::radui.report/source-attribute
                ::radui.report/parameters]
         :as   report-opts} (app/component-options this)
        {:keys [::radui.report/columns
                ::radui.report/column-headings
                ::radui.report/column-options
                ::radui.report/edit-form]
         :as   item-opts}   (app/component-options BodyItem)
        {:keys [::radui.form2/attr-id]
         :as   form-opts}   (some-> edit-form app/component-options)

        props (app/props this)
        rows  (get props source-attribute [])]
    ;; (log/info "Rendering report layout")
    (dom/div {:classes ["container mx-auto my-4 flex flex-grow flex-col bg-white sm:rounded sm:border shadow overflow-hidden"]}
      (dom/div {:classes ["border-b flex justify-between px-6 -mb-px"]}
        (dom/h3 {:classes ["text-teal-600 py-4 font-normal text-lg"]}
          (report-title this report-opts))
        (dom/div {:classes ["flex"]}
          (dom/button
            {:classes ["text-white bg-teal-600 hover:bg-teal-800 py-1 px-2 rounded my-4"]
             :onClick (fn [] (radui.api/run-report! this))}
            "Run!")))
      (dom/div
        {:classes ["mx-6 my-4"]}
        (dom/div
          (when (seq rows)
            (let [factory (app/factory BodyItem)]
              (if (seq columns)
                (dom/table :.table-fixed.table-striped
                  (dom/thead
                    (dom/tr
                      (map-indexed
                       (fn [idx column]
                         (dom/th :.text-xs.tracking-wide.font-bold.text-grey-dark
                           {:key     idx
                            :classes (get-in column-options [column :th-classes])}
                           (column-name column-options column)))
                       columns)))
                  (dom/tbody
                    (map-indexed
                     (fn [idx row]
                       (dom/tr {:key (str "row-" idx)}
                         (map-indexed
                          (fn [idx qid]
                            (dom/td {:key (str "col-" idx)}
                              ;; TODO: Coercion
                              (let [label (str (get row qid))]
                                (if (and edit-form (= 0 idx))
                                  ;; FIXME: remove hard-coded :main-controller
                                  (dom/a {:classes ["cursor-pointer text-blue-700"]
                                          :onClick (fn [] (radui.api/edit! this :main-controller edit-form (get row attr-id)))}
                                    label)
                                  (render-attribute this qid (get row qid))))))
                          columns)))
                     rows)))
                (dom/div {:classes ["container mx-auto"]}
                  (dom/div {:classes ["flex flex-col sm:flex-row flex-wrap"]}
                    (map factory rows)))))))))
    #_(dom/div
        (dom/div :.ui.top.attached.segment
          (dom/div :.ui.form
            (map-indexed
             (fn [idx k]
               (dom/div :.ui.inline.field {:key idx}
                 (dom/label (some-> k name str/capitalize))
                 (radui.report/render-parameter-input this k)))
             (keys parameters))))
        (dom/div :.ui.attached.segment
          (when (seq rows)
            (if (seq columns)
              ))))))
