(ns bbop.alpha.rad.radui.rendering.twadmin.form
  (:require
   [com.fulcrologic.fulcro.dom :as dom]
   [bbop.alpha.fulcro.app :as app]
   [bbop.alpha.rad.radui.report :as radui.report]
   [bbop.alpha.rad.radui.rendering.twadmin.util :as twadmin.util]
   [bbop.alpha.rad.radui.form2.core :as radui.form2]
   [bbop.alpha.rad.radui.form2.api :as radui.form2.api]
   [bbop.alpha.rad.radui.api :as radui.api]
   [bbop.alpha.rad.attribute :as rad.attr]
   [bbop.alpha.core.log :as log]
   ))


(defn column-name [column-options column]
  (or (get-in column-options [column :title])
      (some-> column name twadmin.util/titleize)))


(defn report-title [this report-opts]
  (or (some-> report-opts ::radui.report/title)
      (str (-> this app/component-name keyword name twadmin.util/titleize))))


(defn render-attribute
  [{:keys [::radui.form2/form-instance] :as env} qid]
  (let [kind      (radui.api/attr-spec form-instance qid ::rad.attr/kind)
        render-fn (some-> form-instance app/component-options ::radui.form2/layout ::radui.form2/kind->layout kind)]
    (if render-fn
      (dom/div
        {:classes ["mx-6 my-4"]
         :key     (str qid)}
        (render-fn env qid))
      (log/error "Missing form render-fn for attribute" {:qid qid :kind kind}))))


(defn ui-render-layout
  [env]
  (let [{:keys [::radui.form2/master-form
                ::radui.form2/form-instance]} env
        {:keys [::radui.form2/attributes]
         :as   options}                       (app/component-options form-instance)]
    (dom/section {:classes ["container mx-auto my-4 flex flex-grow flex-col bg-white sm:rounded sm:border shadow"]}
      (dom/header {:classes ["border-b flex justify-between px-6 -mb-px"]}
        (dom/h3 {:classes ["text-teal-600 py-4 font-normal text-lg"]}
          "Edit")
        (dom/div {:classes ["flex"]}
          (dom/button
            {;; :disabled (not dirty?) ;; TODO
             :classes ["text-white bg-teal-600 hover:bg-teal-800 py-1 px-2 rounded my-4 mr-2"]
             :onClick (fn [] (radui.form2.api/reset-form! env))}
            "Undo")
          (dom/button
            {:classes ["text-white bg-teal-600 hover:bg-teal-800 py-1 px-2 rounded my-4"]
             :onClick (fn [] (radui.form2.api/save-form! env))}
            "Save")))
      (dom/article
        (map
         (fn [attr] (render-attribute env attr))
         attributes)))))
