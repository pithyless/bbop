(ns bbop.alpha.rad.radui.rendering.semantic-ui.text-area
  (:require
   #?(:cljs [com.fulcrologic.fulcro.dom :as dom]
      :clj  [com.fulcrologic.fulcro.dom-server :as dom])
   [com.fulcrologic.fulcro.dom.events :as dom.event]
   [com.fulcrologic.rad.rendering.semantic-ui.field :refer [render-field-factory]]))


(defn- text-area [{:keys [value onChange onBlur]}]
  (dom/textarea
    {:value    (or value "")
     :onBlur   (fn [evt]
                 (when onBlur
                   (onBlur (dom.event/target-value evt))))
     :onChange (fn [evt]
                 (when onChange
                   (onChange (dom.event/target-value evt))))}))

(def render-field (render-field-factory text-area))
