(ns bbop.alpha.rad.radui.rendering.semantic-ui.keyword-field
  (:require
   #?(:cljs [com.fulcrologic.fulcro.dom :as dom]
      :clj  [com.fulcrologic.fulcro.dom-server :as dom])
   [com.fulcrologic.rad.rendering.semantic-ui.field :refer [render-field-factory]]
   [com.fulcrologic.fulcro.dom.inputs :as dom.inputs]
   [bbop.alpha.fulcro.app :as app]
   [bbop.alpha.core.string :as string]))


(def ui-keyword-input
  (app/factory
   (dom.inputs/StringBufferedInput
    ::KeywordInput
    {:model->string (fn [k] (str k))
     :string->model (fn [s] (string/parse-keyword s))})))


(def render-field (render-field-factory ui-keyword-input))
