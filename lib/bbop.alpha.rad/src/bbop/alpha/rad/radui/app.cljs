(ns bbop.alpha.rad.radui.app
  (:require
   [com.fulcrologic.fulcro.networking.http-remote :as fulcro.http-remote]
   [com.fulcrologic.fulcro.data-fetch :as fulcro.data-fetch]
   [com.fulcrologic.fulcro.algorithms.form-state :as fulcro.form-state]
   [com.fulcrologic.rad.application :as rad-app]
   [edn-query-language.core :as eql]
   [clojure.walk :as walk]
   [clojure.string :as str]
   [clojure.set :as set]))


(defn request-middleware [{:keys [csrf-token]}]
  (-> (fulcro.http-remote/wrap-fulcro-request
       (fn [req]
         (merge req
                {:headers {"Accept"       "application/transit+json"
                           "Content-Type" "application/transit+json"}})))
      (cond->
          csrf-token (fulcro.http-remote/wrap-csrf-token csrf-token))))


(defn pathom-error?
  [body]
  ;; TODO: crazy, temporary hack. :)
  (try
    (walk/prewalk (fn [x]
                    (when (= x :com.wsscode.pathom.core/reader-error)
                      (throw (js/Error.)))
                    x)
                  body)
    false
    (catch :default _
      true)))



(defn remote-error?
  [result]
  (let [{:keys [:status-code :body]} result]
    (or (not= 200 status-code)
        (pathom-error? body))))


(def bbop-network-blacklist #{})


(defn elision-predicate []
  (let [blacklist      (set/union rad-app/default-network-blacklist
                                  bbop-network-blacklist)
        default-pred   (rad-app/elision-predicate blacklist)
        starts-with-ui (fn [k]
                         (let [kw (if (vector? k) (first k) k)
                               ns (and (keyword? kw) (namespace kw))]
                           (and (string? ns) (str/starts-with? ns "ui."))))]
    (fn [k]
      (or (default-pred k)
          (starts-with-ui k)))))


(defn global-eql-transform []
  (rad-app/global-eql-transform (elision-predicate)))


(defn rad-app [{:keys [api-url fulcro-app-opts]}]
  (rad-app/fulcro-rad-app
   (merge
    {:remotes              {:remote (fulcro.http-remote/fulcro-http-remote
                                     {:url                api-url
                                      :request-middleware (request-middleware
                                                           ;; TODO: csrf-token
                                                           {:csrf-token nil})})}
     :remote-error?        remote-error?
     :global-eql-transform (global-eql-transform)}
    fulcro-app-opts)))
