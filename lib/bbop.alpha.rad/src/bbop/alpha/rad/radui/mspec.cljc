(ns bbop.alpha.rad.radui.mspec
  (:require
   [bbop.alpha.fulcro.app :as app]))


(def component-class
  [:fn app/component-class?])


(def component-instance
  [:fn app/component-instance?])


(def ui-keyword
  [:and qualified-keyword?
   [:fn {:error/message "ui-keyword must be namespaced `:ui/*`"}
    (fn [x] (= "ui" (namespace x)))]])


(def controller-id
  some?)
