(ns bbop.alpha.rad.radui.dev
  (:require
   [com.fulcrologic.fulcro.algorithms.timbre-support :as timbre-support]
   [taoensso.timbre :as timbre]))


(defn setup-logging []
  (js/console.log "Turning logging to :debug")
  (timbre/set-level! :debug)
  (timbre/merge-config!
   {:output-fn timbre-support/prefix-output-fn
    :appenders {:console (timbre-support/console-appender)}}))
