(ns bbop.alpha.rad.radui.controller
  (:require
   [bbop.alpha.fulcro.app :as app]
   [bbop.alpha.rad.radui.core :as radui]
   [bbop.alpha.rad.radui.authorization :as auth]
   [bbop.alpha.rad.attribute :as rad.attr]
   [bbop.alpha.core.log :as log]
   [com.fulcrologic.fulcro.ui-state-machines :as uism :refer [defstatemachine]]

   #_[clojure.set set]
   #_[clojure.spec.alpha :as s]
   #_[com.fulcrologic.guardrails.core :refer [>defn =>]]
   #_[com.fulcrologic.rad.attributes :as attr]
   #_[com.fulcrologic.fulcro.application :as app]

   ))


;;;;;;;;;;;;;;;;; Public API


(defmulti -start-io!
  (fn [_ ComponentClass _]
    (some-> ComponentClass (app/component-options ::radui/type))))


(defmulti -desired-attributes
  (fn [TargetClass]
    (some-> TargetClass (app/component-options ::radui/type))))


(defn io-complete!
  "Custom components should call this to indicate that they are done with I/O, allowing the
   controller to complete the route."
  [app {:keys [::radui/controller-id ::radui/target-route]}]
  (tap> [::controller-io-complete {:id controller-id :target-route target-route}])
  (uism/trigger! app controller-id :event/route-loaded {::radui/target-route target-route}))



;;;;;;;;;;;;;;;;; Private API


(defmethod -start-io! :default [_env ComponentClass _options]
  (log/warn ::start-io!
            {:msg (str "No Controller I/O defined for components of type" (app/component-options ComponentClass ::radui/type)
                       ". This can be ignored via ::radui/io? if that component type does no I/O.")}))


(defmethod -desired-attributes :default [c]
  (log/warn ::desired-attributes
            {:msg (str "No implementation found for `-desired-attributes` for the target component of the route " (app/component-name c))})
  [])


(defn desired-attributes
  "Get the data attributes that are used by the component. The controller uses this to
   interact with the authorization model."
  [c]
  ;; TODO: malli/assert
  ;;[app/component-class? => (s/every ::attr/attribute :kind vector?)]
  (-desired-attributes c))


(defn start-io!
  [env ComponentClass {:keys [::radui/target-route] :as options}]
  (-> env
      (uism/store ::radui/target-route target-route)
      (-start-io! ComponentClass options)))


(defn does-io? [component-class]
  (boolean (some-> component-class (app/component-options ::radui/io?))))


(defn activate-route [{:keys [::uism/fulcro-app] :as env} target-route]
  (app/change-route fulcro-app target-route)
  (-> env
      (uism/store ::radui/target-route nil)
      (uism/activate :state/idle)))


(defn- initialize-route-data [{:keys [::uism/event-data] :as env}]
  (tap> [::initialize-route-data env])
  (let [target-route (or (::radui/target-route event-data)
                         (uism/retrieve env ::radui/target-route))
        machine-id   (::uism/asm-id env)
        options      (assoc event-data
                            ::radui/target-route target-route
                            ::radui/controller-id machine-id)]
    (if (empty? target-route)
      (let [{:keys [::home-page]} (uism/retrieve env :config)]
        (log/info ::no-target-route {:default :home-page})
        (cond-> (uism/activate env :state/idle)
          (seq home-page) (activate-route home-page)))
      ;; The main thing we end up needing to know is the target class, and from
      ;; that we can pull the information we need to do the remaining steps.
      ;; I.e. Load, create a new, etc.
      (let [router            (uism/actor-class env :actor/router)
            {:keys [:target]} (app/route-target router target-route)]
        (if (does-io? target)
          (-> env
              (start-io! target options)
              ;; TODO: In case I/O fails to come back
              #_(uism/set-timeout! :event/io-faled 2000))
          (activate-route env target-route))))))


;; (defn authorities-required-for-route
;;   "Returns a set of providers that are required to properly render the given route"
;;   [{::uism/keys [event-data] :as env}]
;;   ;; TODO: Calculate providers from attributes that are on the query of the given route
;;   (tap> [::authorities-required-for-route event-data])
;;   (let [{::radui/keys [target-route]} event-data]
;;     (if (empty? target-route)
;;       #{}
;;       (let [router           (uism/actor-class env :actor/router)
;;             {:keys [target]} (app/route-target router target-route)
;;             _                (tap> [::route-target target])
;;             attributes       (some-> target desired-attributes)
;;             _                (tap> [::route-attributes attributes])
;;             authorities      (into #{}
;;                                    (keep ::auth/authority)
;;                                    attributes)
;;             _                (tap> [::route-authorities authorities])]
;;         authorities))))


(defn prepare-for-route [{:keys [#_::uism/fulcro-app ::uism/event-data] :as env}]
  (tap> [::prepare-for-route env])
  (let [;; TODO FIXME - authorities
        ;; necessary-authorities (authorities-required-for-route env)
        ;; current-authorities   (auth/verified-authorities fulcro-app)
        ;; missing-authorities   (set/difference necessary-authorities current-authorities)
        missing-authorities #{}]
    ;; TODO: cancel any in-progress route loading (could be a route while a route was loading)
    (if (empty? missing-authorities)
      (initialize-route-data env)
      (let [target-route (::radui/target-route event-data)
            machine-id   (::uism/asm-id env)]
        (-> env
            (uism/store ::radui/target-route target-route)
            ;; (auth/authenticate (first missing-authorities) machine-id)
            (uism/activate :state/gathering-permissions))))))


(defstatemachine central-controller
  {::uism/aliases
   {}

   ::uism/states
   {:initial
    {::uism/handler (fn [{::uism/keys [event-data] :as env}]
                      (-> env
                          (uism/store :config event-data)
                          (prepare-for-route)))}

    :state/gathering-permissions
    {::uism/events
     {:event/route         {::uism/handler prepare-for-route}
      :event/authenticated {::uism/handler prepare-for-route}}}

    ;; TODO: Route timeout
    :state/routing
    {::uism/events
     {:event/route        {::uism/handler prepare-for-route}
      :event/route-loaded {::uism/handler (fn [{::uism/keys [event-data] :as env}]
                                            (let [loaded-route (get event-data ::radui/target-route)
                                                  _            (tap> [::uism-loaded-route loaded-route])
                                                  target-route (uism/retrieve env ::radui/target-route)
                                                  _            (tap> [::uism-target-route target-route])
                                                  ]
                                              (if (= loaded-route target-route)
                                                (activate-route env target-route)
                                                (do
                                                  (log/warn ::ignoring-mismatched-route
                                                            {:target target-route
                                                             :loaded loaded-route})
                                                  env))))}}}

    :state/idle
    {::uism/events
     {:event/route {::uism/handler prepare-for-route}}}}})


(defn start!
  "Start the central controller and auth system."
  [app {:keys [::radui/target-route ::radui/controller-id ::router ::home-page]}]
  (uism/begin! app central-controller controller-id
               {:actor/router router}
               {::radui/target-route  target-route
                ::radui/controller-id controller-id
                ::home-page           (or home-page ["index"])}))
