(ns ^{:clojure.tools.namespace.repl/unload false
      :clojure.tools.namespace.repl/load   false}
    bbop.alpha.rad.database-adapter.protocol)


(defprotocol IRadDBAdapterConn
  (pull-by-attr [this id-attr id-or-ids eql-query])
  (query [this query-spec query-inputs])
  (transact! [this tx-data]))


(defprotocol IRadDBAdapter
  (migrate-tx [this new-schema-attrs])
  (migrate! [this new-schema-attrs])
  (new-conn [this opts]))
