(ns bbop.alpha.rad.database-adapter.datomic.schema
  (:require
   [bbop.alpha.rad.database.api :as rad.db]
   [com.fulcrologic.rad.attributes :as attr]
   [bbop.alpha.core.malli :as malli]))


;; TODO - enum :ref type

(def scalar->type
  {:boolean :db.type/boolean
   :decimal :db.type/bigdec
   :double  :db.type/double
   :enum    :db.type/ref
   :instant :db.type/instant
   :keyword :db.type/keyword
   :long    :db.type/long
   :string  :db.type/string
   :symbol  :db.type/symbol
   :text    :db.type/string
   :ref     :db.type/ref
   :uuid    :db.type/uuid})


(def type->db-type
  (merge scalar->type
         {:tuple :db.type/tuple}))


(defn attr-ident
  [{:keys [::attr/qualified-key ::rad.db/deprecated-ident]}]
  (if deprecated-ident
    {:db/ident deprecated-ident}
    {:db/ident qualified-key}))


(defn attr-value-type
  [{:keys [::attr/type]}]
  (when-let [type (get type->db-type type)]
    {:db/valueType type}))


(defn attr-cardinality
  [{:keys [::attr/cardinality]
    :or   {cardinality :one}}]
  (case cardinality
    :one  {:db/cardinality :db.cardinality/one}
    :many {:db/cardinality :db.cardinality/many}))


(defn attr-index
  [attrs]
  (let [index? (get attrs :db/index true)]
    {:db/index index?}))


(defn attr-identity
  [{:keys [::attr/identity?]}]
  (when identity?
    {:db/unique :db.unique/identity}))


(defn attr-enumerated-values
  [{:keys [::attr/type ::attr/enumerated-values ::attr/qualified-key]}]
  (let [parser (fn [v]
                 (cond
                   (qualified-keyword? v) {:db/ident v}
                   (keyword? v)           {:db/ident (keyword (str (namespace qualified-key) "." (name qualified-key))
                                                              (name v))}
                   :else                  v))]
    (when (and (= :enum type) (seq enumerated-values))
      (map parser enumerated-values))))


(defn attr-alias
  [{:keys [::rad.db/deprecated-ident ::attr/qualified-key]}]
  (when deprecated-ident
    [[:db/add deprecated-ident :db/ident qualified-key]]))


;; TODO
;; (defn attr-tuple-type
;;   [{:keys [::attr/tuple-kind]}]
;;   (when tuple-kind
;;     (let [[name opts] tuple-kind]
;;       (case name
;;         :composite {:db/tupleAttrs opts}
;;         :struct    {:db/tupleTypes opts}
;;         :list      {:db/tupleType opts}))))


;; TODO
;; (defn attr-component
;;   [{:keys [::attr/kind ::attr/component?]}]
;;   (when (and (= :ref kind) component?)
;;     {:db/isComponent true}))


(def attr-db-spec
  [:map
   [::rad.db/alias {:optional true} keyword?]
   [::rad.db/deprecated-ident {:optional true} qualified-keyword?]
   [::attr/qualified-key qualified-keyword?]
   [::attr/type (into [:enum] (keys type->db-type))]
   [:db/index? {:optional true} boolean?]
   [::attr/identity? {:optional true} boolean?]
   [::attr/enumerated-values {:optional true} [:set [:or
                                                     keyword?
                                                     qualified-keyword?
                                                     [:map [:db/ident qualified-keyword?]]]]]
   ;; TODO:
   ;; [::attr/component? {:optional true} boolean?]
   ;; [::attr/tuple-kind
   ;;  {:optional true}
   ;;  [:or
   ;;   [:tuple
   ;;    [:enum :composite]
   ;;    [:vector {:min 1} qualified-keyword?]]
   ;;   [:tuple
   ;;    [:enum :struct]
   ;;    [:vector {:min 1}
   ;;     (into [:enum] (keys scalar->type))]]
   ;;   [:tuple
   ;;    [:enum :list]
   ;;    (into [:enum] (keys scalar->type))]]]
   ])



(defn attr-tx-data [attr]
  (malli/assert attr-db-spec attr)
  (reduce into
   [(merge
     (attr-ident attr)
     (attr-value-type attr)
     (attr-cardinality attr)
     (attr-index attr)
     (attr-identity attr)
     #_(attr-tuple-type attr)
     #_(attr-component attr))]
   [(attr-enumerated-values attr)
    (attr-alias attr)]))
