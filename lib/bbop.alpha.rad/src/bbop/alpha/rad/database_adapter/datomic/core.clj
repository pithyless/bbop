(ns bbop.alpha.rad.database-adapter.datomic.core
  (:require
   [bbop.alpha.core.guard :refer [>defn =>]]
   [bbop.alpha.datomic-conn.mspec :as mspec.datomic-conn]
   [bbop.alpha.rad.database-adapter.mspec :as mspec.rad.db-adapter]
   [bbop.alpha.rad.database-adapter.protocol :as protocol]
   [bbop.alpha.rad.database-adapter.datomic.schema :as dbadapter.datomic.schema]
   [bbop.alpha.datomic-conn.api :as datomic-conn]
   [bbop.alpha.datomic.api :as datomic]
   [bbop.alpha.core.system :as system]))


(defrecord DatomicRadAdapterConn [datomic-conn datomic-db-ref]
  protocol/IRadDBAdapterConn
  (pull-by-attr [this id-attr id-or-ids eql-query]
    (let [db          @datomic-db-ref
          eid-or-eids (if (sequential? id-or-ids)
                        (mapv #(vector id-attr %) id-or-ids)
                        [id-attr id-or-ids])]
      (datomic/pull-* db eql-query eid-or-eids)))

  (query [this query-spec query-inputs]
    (let [db @datomic-db-ref]
      (apply datomic/q query-spec db query-inputs)))

  (transact! [this tx-data]
    (let [result (datomic-conn/transact datomic-conn tx-data)]
      (reset! datomic-db-ref (:db-after result))
      result)))


(defrecord DatomicRadAdapter [datomic-conn]
  protocol/IRadDBAdapter
  (new-conn [this opts]
    (map->DatomicRadAdapterConn
     {:datomic-conn   datomic-conn
      :datomic-db-ref (atom (datomic-conn/db datomic-conn))}))

  (migrate-tx [this new-schema-attrs]
    (vec (mapcat dbadapter.datomic.schema/attr-tx-data new-schema-attrs)))

  (migrate! [this tx-data]
    (when (seq tx-data)
      (datomic-conn/transact datomic-conn tx-data))))


(>defn component-start
  [{:keys [:datomic-conn]}]
  [[:map [:datomic-conn mspec.datomic-conn/conn]]
   => mspec.rad.db-adapter/adapter]
  (map->DatomicRadAdapter {:datomic-conn datomic-conn}))


(def component
  (system/lifecycle component-start))
