(ns bbop.alpha.rad.database-adapter.mspec
  (:require
   [bbop.alpha.core.mspec.protocol :as mspec.protocol]
   [bbop.alpha.rad.database-adapter.protocol :as protocol]))


(def scalar
  ;; TODO: any valid EQL scalar
  any?)


(def eql-query
  ;; TODO: replace with ::eql/query
  [:vector any?])


(def tx-migration
  [:vector [:map [:db/ident any?]]])


(def tx-result
  [:map
   [:db-before any?]
   [:db-after any?]
   [:tx-data any?]
   [:tempids map?]])


(def adapter
  (mspec.protocol/satisfies protocol/IRadDBAdapter))


(def conn
  (mspec.protocol/satisfies protocol/IRadDBAdapterConn))
