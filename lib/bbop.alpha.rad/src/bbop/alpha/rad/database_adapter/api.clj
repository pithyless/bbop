(ns bbop.alpha.rad.database-adapter.api
  (:require
   [bbop.alpha.core.guard :refer [>defn =>]]
   [bbop.alpha.rad.database-adapter.protocol :as protocol]
   [bbop.alpha.rad.database-adapter.mspec :as mspec.rad.db-adapter]
   [bbop.alpha.rad.attribute.mspec :as mspec.rad.attr]
   [bbop.alpha.core.malli :as malli]
   [bbop.alpha.core.mspec :as mspec]))


(>defn migrate-tx
  [component new-schema-attrs]
  [mspec.rad.db-adapter/adapter [:vector mspec.rad.attr/attr]
   => mspec.rad.db-adapter/tx-migration]
  (protocol/migrate-tx component new-schema-attrs))


(>defn migrate!
  [component tx-data]
  [mspec.rad.db-adapter/adapter mspec.rad.db-adapter/tx-migration
   => mspec.rad.db-adapter/tx-result]
  (let [result (protocol/migrate! component tx-data)]
    (if (:db-after result)
      result
      (throw (ex-info "Migration failed" {:tx-data tx-data :details result})))))


(>defn new-conn
  [component opts]
  [mspec.rad.db-adapter/adapter map? => mspec.rad.db-adapter/conn]
  (protocol/new-conn component opts))


(>defn pull-by-attr
  [component id-attr id-or-ids eql-query]
  [mspec.rad.db-adapter/conn
   mspec.rad.attr/qid
   [:or mspec.rad.db-adapter/scalar [:sequential mspec.rad.db-adapter/scalar]]
   mspec.rad.db-adapter/eql-query
   => any?]
  (protocol/pull-by-attr component id-attr id-or-ids eql-query))


(>defn query
  ([component query-spec]
   [mspec.rad.db-adapter/conn any? => any?]
   (query component query-spec []))
  ([component query-spec query-inputs]
   [mspec.rad.db-adapter/conn any? [:vector any?] => any?]
   (protocol/query component query-spec query-inputs)))


(>defn transact! [component tx-data]
  [mspec.rad.db-adapter/conn [:vector {:min 1} any?]
   => mspec.rad.db-adapter/tx-result]
  (protocol/transact! component tx-data))
