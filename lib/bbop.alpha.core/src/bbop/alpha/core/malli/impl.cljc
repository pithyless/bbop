(ns bbop.alpha.core.malli.impl
  (:require
   [bbop.alpha.core.malli :as malli]))


(defn data-schema? [data]
  (try
    (and (vector? data)
         (-> data
             (malli/schema)
             (malli/schema?)))
    (catch #?(:clj  java.lang.IllegalArgumentException
              :cljs :default) _
      false)))
