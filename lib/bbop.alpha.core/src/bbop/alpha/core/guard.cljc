(ns bbop.alpha.core.guard
  #?(:cljs (:require-macros [bbop.alpha.core.guard]))
  (:require
   [clojure.walk :as walk]
   [clojure.spec.alpha :as s]
   [bbop.alpha.core.namespace :refer [cljs-env?]]
   [bbop.alpha.core.guard.spec :as guard.spec]
   [bbop.alpha.core.malli :as malli]
   [bbop.alpha.core.log :as log]))


(def => :=>)


(defn gensym-missing-arg [[arg-type arg]]
  (as-> arg arg
    (case arg-type
      :sym [arg-type arg]
      :seq [arg-type (update arg :as #(or % [{:as :as :sym (gensym "arg_")}]))]
      :map [arg-type (update arg :as #(or % (gensym "arg_")))])))


(defn arg->name [[arg-type arg]]
  (as-> arg arg
    (case arg-type
      :sym arg
      :seq (get-in arg [:as 0 :sym])
      :map (get-in arg [:as]))))


(defn malli-validator [log-info mspec malli-opts]
  (try
    (malli/validator mspec malli-opts)
    (catch #?(:cljs :default :clj Throwable) e
      (throw (ex-info "Invalid malli validator"
                      (merge (ex-data e)
                             {:mspec mspec}
                             log-info))))))

(defn malli-explainer [log-info mspec malli-opts]
  (try
    (malli/explainer mspec malli-opts)
    (catch #?(:cljs :default :clj Throwable) e
      (throw (ex-info "Invalid malli validator"
                      (merge (ex-data e)
                             log-info))))))


(defn param-validator [log-info gw-spec]
  (let [arg-mspecs (:arg-mspecs gw-spec)
        mspec      (if (seq arg-mspecs)
                     (into [:tuple] arg-mspecs)
                     [:enum []])
        malli-opts {}
        validator  (malli-validator log-info mspec malli-opts)
        explainer  (malli-explainer log-info mspec malli-opts)]
    (fn [args]
      (let [valid-exception (atom nil)]
        (try
          (when-not (validator args)
            (reset! valid-exception
                    (ex-info "Arguments do not match spec"
                             (merge log-info
                                    {:details (-> (explainer args)
                                                  (malli/humanize))}
                                    {:explain (explainer args)}))))
          (catch #?(:cljs :default :clj Throwable) e
            (log/error ::param-validator-bug
                       (merge log-info
                              {:msg "Internal error in validator."
                               :ex  e}))
            (throw e)))
        (when @valid-exception
          (throw @valid-exception))
        nil))))


(defn return-validator [log-info gw-spec]
  (let [mspec      (:ret-mspec gw-spec)
        malli-opts {}
        validator  (malli-validator log-info mspec malli-opts)
        explainer  (malli-explainer log-info mspec malli-opts)]
    (fn [value]
      (let [valid-exception (atom nil)]
        (try
          (when-not (validator value)
            (reset! valid-exception
                    (ex-info "Return value does not match spec"
                             (merge log-info
                                    {:details (-> (explainer value)
                                                  (malli/humanize))}
                                    {:explain (explainer value)}))))
          (catch #?(:cljs :default :clj Throwable) e
            (log/error ::return-validator-bug
                       (merge log-info
                              {:msg "Internal error in validator."
                               :ex  e}))
            (throw e)))
        (when @valid-exception
          (throw @valid-exception))
        nil))))


(defn process-defn-body [cfg args+body]
  ;; TODO: does not support varargs
  (let [{:keys [:args :prepost :body :gw-spec]
         }                        args+body
        {:keys [:vargs :varargs]} args
        gw-spec                   (dissoc gw-spec :ret-sym)
        log-info                  (select-keys cfg [:location :fn-name])
        _                         (when (seq varargs)
                                    (throw (ex-info ">defn syntax does not support varargs" log-info)))
        new-vargs                 (mapv gensym-missing-arg vargs)
        named-vargs               (mapv arg->name new-vargs)
        orig-arg-list             (s/unform ::guard.spec/arg-list {:vargs vargs})
        new-arg-list              (s/unform ::guard.spec/arg-list {:vargs new-vargs})
        p                         (gensym "p")
        param-validator           `(param-validator ~log-info ~gw-spec)
        check-args                `(~p ~named-vargs)
        f                         (gensym "f")
        ret                       (gensym "ret")
        real-fn                   `(fn ~orig-arg-list ~@body)
        call                      `(~f ~@named-vargs)
        r                         (gensym "r")
        return-validator          `(return-validator ~log-info ~gw-spec)
        check-ret                 `(~r ~ret)]
    `(~@(remove nil? [new-arg-list prepost])
      (let [~p   ~param-validator
            ~r   ~return-validator
            ~f   ~real-fn
            ~'_  ~check-args
            ~ret ~call]
        ~check-ret
        ~ret))))


(defn parse-location [cfg fn-name]
  (let [{:keys [file line]} (if (cljs-env? (:env cfg))
                              (meta fn-name)
                              {:file #?(:clj *file* :cljs "N/A")
                               :line (some-> cfg :form meta :line)})]
    (str file ":" line)))


(defn rename-cljs-forms
  ([form]
   (rename-cljs-forms form false))
  ([form strip-core-ns]
   (let [ns-replacements   (cond-> {"clojure.core"            "cljs.core"
                                    "clojure.test"            "cljs.test"
                                    "clojure.spec.alpha"      "cljs.spec.alpha"
                                    "clojure.spec.test.alpha" "cljs.spec.test.alpha"
                                    "orchestra.spec.test"     "orchestra-cljs.spec.test"
                                    "clojure.spec.gen.alpha"  "cljs.spec.gen.alpha"}
                             strip-core-ns (merge {"clojure.core" nil
                                                   "cljs.core"    nil}))
         replace-namespace #(if-not (qualified-symbol? %)
                              %
                              (let [nspace (namespace %)]
                                (if (contains? ns-replacements nspace)
                                  (symbol (get ns-replacements nspace) (name %))
                                  %)))]
     (walk/postwalk replace-namespace form))))


(defn generate-defn [cfg defn-forms]
  (let [conformed-defn    (s/conform ::guard.spec/gw-defn-args defn-forms)
        _                 (when (= ::s/invalid conformed-defn)
                            (throw (ex-info ">defn syntax does not conform"
                                            {:explain (s/explain-data ::guard.spec/gw-defn-args conformed-defn)
                                             :form    (:form cfg)})))
        fn-name           (:name conformed-defn)
        fn-bodies         (:bs conformed-defn)
        docstring         (:docstring conformed-defn)
        meta-map          (:meta conformed-defn)
        defn-sym          'defn
        arity             (key fn-bodies)
        new-cfg           (-> cfg
                              (assoc :fn-name (str fn-name))
                              (assoc :location (parse-location cfg fn-name)))
        process-fn-bodies (fn []
                            (cond-> (case arity
                                      :arity-1 (process-defn-body new-cfg (val fn-bodies))
                                      :arity-n (mapv (partial process-defn-body new-cfg) (get-in fn-bodies [1 :bodies])))
                              (cljs-env? (:env cfg)) rename-cljs-forms))
        main-defn         `(~@(remove nil? [defn-sym fn-name docstring meta-map])
                            ~@(process-fn-bodies))]
    main-defn))


(defmacro >defn
  {:arglists '([name doc-string? attr-map? [params*] gspec prepost-map? body?])}
  [& forms]
  (generate-defn {:env &env :form &form} forms))
