(ns bbop.alpha.core.contrib
  (:refer-clojure :exclude [cond])
  #?(:cljs (:require-macros [bbop.alpha.core.contrib]))
  (:require
   [bbop.alpha.core.namespace :refer [import-vars]]
   [vvvvalvalval.supdate.api]
   [fipp.edn]
   [better-cond.core]))


#?(:clj (import-vars [vvvvalvalval.supdate.api
                      supdate]))


#?(:clj (import-vars [better-cond.core
                      cond]))


(def compile-supdate vvvvalvalval.supdate.api/compile)


(def fipp fipp.edn/pprint)
