(ns bbop.alpha.core.string)


(def kw-regex
  #?(:clj #"^:([\w\.\-_]+)(?:\/([\w\.\-_]+))?$"
     :cljs (re-pattern "^:([\\w\\.\\-_]+)(?:\\/([\\w\\.\\-_]+))?$")))


(defn parse-keyword [s]
  (when s
    (let [[match a b] (re-find kw-regex s)]
      (when match
        (if b
          (keyword a b)
          (keyword a))))))
