(ns bbop.alpha.core.mspec
  ;; #?(:cljs (:refer-clojure :exclude [uuid]))
  (:require [clojure.spec.alpha :as s]
            [bbop.alpha.core.malli :as malli]
            [bbop.alpha.core.time :as time]
            [clojure.string :as str]))


#?(:clj
   (defn- malli-schema-vector? [data]
     (try
       (and (vector? data)
            (-> data
                (malli/schema)
                (malli/schema?)))
       (catch java.lang.IllegalArgumentException _
         false))))


#?(:clj
   (s/def ::malli-schema-vector
     malli-schema-vector?))


#?(:clj
   (def malli-schema-vector
     [:fn (fn [x] (malli-schema-vector? x))]))


;; (def string
;;   [:and string? [:fn '(fn [x] (not (str/blank? x)))]])


(defn satisfies-protocol [p]
  [:fn {:error/fn
        (fn error-fn [{:keys [value]} _]
          (str "Value <" (pr-str value) "> does not implement <" (:on p) ">"))}
   (fn check-fn [x] (satisfies? p x))])


;; (def uuid
;;   uuid?)


;; (def inst
;;   [:fn {:error/fn
;;         (fn error-fn [{:keys [value]} _]
;;           (str "Should be instant: <" (pr-str value) ">"))}
;;    time/inst?])
