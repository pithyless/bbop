(ns bbop.alpha.core.cspec.malli
  (:require
   [clojure.spec.alpha :as s]
   [bbop.alpha.core.malli.impl :as malli.impl]))


(s/def ::data-schema
  malli.impl/data-schema?)
