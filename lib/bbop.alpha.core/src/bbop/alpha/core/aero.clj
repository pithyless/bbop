(ns bbop.alpha.core.aero
  (:require
   [aero.core :as aero]
   [bbop.alpha.core.contrib :as contrib]
   [bbop.alpha.core.mspec.string :as mspec.string]
   [bbop.alpha.core.guard :refer [>defn =>]]
   [clojure.java.io :as jio]
   [bbop.alpha.core.malli :as malli]))


(>defn resolve-symbol
  [sym]
  [symbol? => [:fn #(var? %)]]
  (contrib/cond
    :let [ns (namespace sym)]

    (nil? ns)
    (throw (ex-info (str "Symbol not found: " sym) {}))

    :let [resolved-ns (or (resolve sym)
                          (do (-> sym namespace symbol require)
                              (resolve sym)))]

    ;; TODO - log better error if could not compile ns?

    (nil? resolved-ns)
    (throw (ex-info (str "Cannot resolve symbol: " sym) {}))

    resolved-ns))


(defmethod aero/reader 'var
  [_ _ value]
  (var-get (resolve-symbol value)))


(>defn read-config
  [filename profile]
  [mspec.string/present keyword?
   => [:map]]
  (contrib/cond
    :let [file (jio/resource filename)]

    (nil? file)
    (assert nil (str "Resource not found: " filename))

    (aero/read-config file {:profile profile})))
