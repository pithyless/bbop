(ns bbop.alpha.core.time.impl
  (:refer-clojure :exclude [inst?])
  (:require
   [tick.alpha.api :as tick]))


(defn inst? [x]
  #?(:clj  (clojure.core/inst? x)
     :cljs (and (not (string? x))
                (cljs.core/inst? (tick/inst x)))))
