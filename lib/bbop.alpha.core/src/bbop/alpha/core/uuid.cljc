(ns bbop.alpha.core.uuid
  (:refer-clojure :exclude [random-uuid])
  (:require
   [cljc.java-time.instant :as cljc-time.instant]
   [bbop.alpha.core.malli :as malli]
   [bbop.alpha.core.guard :refer [>defn =>]]
   [bbop.alpha.core.time :as time]
   [bbop.alpha.core.mspec.time :as mspec.time]
   [bbop.alpha.core.mspec.string :as mspec.string]
   [bbop.alpha.core.mspec.uuid :as mspec.uuid]
   #?(:cljs [goog.string :as gstring]))
  #?(:clj (:import
           (java.util UUID))))


#?(:cljs
   (defn- rand-bits [pow]
     (rand-int (bit-shift-left 1 pow))))


#?(:cljs
   (defn- to-hex-string [n l]
     (let [s (.toString n 16)
           c (count s)]
       (cond
         (> c l) (subs s 0 l)
         (< c l) (str (apply str (repeat (- l c) "0")) s)
         :else   s))))


(>defn random-uuid
  "Generate a new UUID."
  []
  [=> mspec.uuid/uuid]
  #?(:clj  (UUID/randomUUID)
     :cljs (cljs.core/random-uuid)))


(>defn random-squuid
  "Generate a UUID that grows with time. Such UUIDs will always go to the end
  of the index and that will minimize insertions in the middle. Consist of 64
  bits of current UNIX timestamp (in seconds) and 64 random bits (2^64 different
  unique values per second)."
  {:attribution "tonsky/datascript"}
  ([]
   [=> mspec.uuid/uuid]
   (random-squuid (time/now)))
  ([inst]
   [mspec.time/instant => mspec.uuid/uuid]
   (let [seconds (time/seconds-after-epoch inst)]
     #?(:clj
        (let [uuid     (random-uuid)
              high     (.getMostSignificantBits uuid)
              low      (.getLeastSignificantBits uuid)
              new-high (bit-or (bit-and high 0x00000000FFFFFFFF)
                               (bit-shift-left seconds 32)) ]
          (UUID. new-high low))
        :cljs
        (uuid
          (str
            (to-hex-string seconds 8)
            "-" (-> (rand-bits 16) (to-hex-string 4))
            "-" (-> (rand-bits 16) (bit-and 0x0FFF) (bit-or 0x4000) (to-hex-string 4))
            "-" (-> (rand-bits 16) (bit-and 0x3FFF) (bit-or 0x8000) (to-hex-string 4))
            "-" (-> (rand-bits 16) (to-hex-string 4))
            (-> (rand-bits 16) (to-hex-string 4))
            (-> (rand-bits 16) (to-hex-string 4))))))))


(>defn squuid-instant
  [uuid]
  [mspec.uuid/uuid => mspec.time/instant]
  #?(:clj (-> (.getMostSignificantBits ^UUID uuid)
              (bit-shift-right 32)
              (cljc-time.instant/of-epoch-second))
     :cljs (-> (subs (str uuid) 0 8)
               (js/parseInt 16)
               (cljc-time.instant/of-epoch-second))))


(def uuid-regex
  (re-pattern #"^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"))


(>defn parse
  "Tries to parse and return UUID, or returns nil."
  [str-or-uuid]
  [[:or mspec.string/present mspec.uuid/uuid] => [:maybe mspec.uuid/uuid]]
  #?(:clj (if (uuid? str-or-uuid)
            str-or-uuid
            (try
              (UUID/fromString str-or-uuid)
              (catch IllegalArgumentException _
                nil)))
     :cljs (if (uuid? str-or-uuid)
             (when (re-find uuid-regex (str str-or-uuid))
               str-or-uuid)
             (when (re-find uuid-regex str-or-uuid)
               (cljs.core/uuid str-or-uuid)))))


(>defn fake-uuid
  "Generate a fake, deterministic UUID. Useful only for testing."
  [int]
  [pos-int? => mspec.uuid/uuid]
  #?(:clj (parse (format "ffffffff-ffff-ffff-ffff-%012d" int))
     :cljs (parse (gstring/format "ffffffff-ffff-ffff-ffff-%012d" int))))
