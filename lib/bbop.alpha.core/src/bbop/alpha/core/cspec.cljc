(ns bbop.alpha.core.cspec
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as str]))


(s/def ::string
  (s/and string? (complement str/blank?)))


(defn vec-of [spec]
  (s/coll-of spec :kind vector? :gen-max 10))


(defn set-of [spec]
  (s/coll-of spec :kind set? :gen-max 10))
