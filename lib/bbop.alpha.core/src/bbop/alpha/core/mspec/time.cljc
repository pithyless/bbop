(ns bbop.alpha.core.mspec.time
  (:require
   [clojure.spec.alpha :as s]
   [clojure.test.check.generators]
   [bbop.alpha.core.time.impl :as time.impl]))


(def instant
  [:fn {:error/fn
        (fn error-fn [{:keys [value]} _]
          (str "Should be instant: <" (pr-str value) ">"))
        :gen/gen (s/gen inst?)}
   time.impl/inst?])
