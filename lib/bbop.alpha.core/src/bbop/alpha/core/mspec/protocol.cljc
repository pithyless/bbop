(ns bbop.alpha.core.mspec.protocol)


(defn satisfies [p]
  #?(:clj
     [:fn {:error/fn
           (fn error-fn [{:keys [value]} _]
             (str "Value <" (pr-str value) "> does not implement <" (:on p) ">"))}
      (fn check-fn [x] (satisfies? p x))]
     ;; TODO: satisfies? is a macro in CLJS; ignoring for now
     :cljs
     [:fn (constantly true)]))


(defn instance-of [p]
  #?(:clj
     [:fn {:error/fn
           (fn error-fn [{:keys [value]} _]
             (str "Value <" (pr-str value) "> is not instance of <" p ">"))}
      (fn check-fn [x] (instance? p x))]
     ;; TODO: ignoring CLJS for now
     :cljs
     [:fn (constantly true)]))
