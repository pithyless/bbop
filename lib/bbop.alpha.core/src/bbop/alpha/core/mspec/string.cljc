(ns bbop.alpha.core.mspec.string
  (:require
   [bbop.alpha.core.malli :as malli]))


;; TODO: deprecate
(def present
  [:and string? [:fn '(fn [x] (not (str/blank? x)))]])


(def string
  (malli/schema
   [:and string? [:fn '(fn [x] (not (str/blank? x)))]]))
