(ns bbop.alpha.core.mspec.malli
  (:require
   [bbop.alpha.core.malli.impl :as malli.impl]))


(def data-schema
  [:fn (fn [x] (malli.impl/data-schema? x))])
