(ns bbop.alpha.core.system.impl
  (:require
   [bbop.alpha.core.contrib :as contrib]
   [bbop.alpha.core.guard :refer [>defn =>]]
   [clojure.set :as set]
   [flc-x.kw-args :as fx.kw-args]
   [flc-x.partial :as fx.partial]
   [flc-x.try :as fx.try]
   [flc.core :as fc.core]
   [flc.map-like :as fc.map]
   [flc.process :as fc.process]
   [flc.program :as fc.program])
  (:import (clojure.lang ExceptionInfo)))


(defn lifecycle
  "A program with start/stop behavior."
  ([start]
   (lifecycle start (fn [_])))
  ([start stop]
   (fc.program/lifecycle start stop)))


(>defn comp-kw
  "Component that takes a map of both configuration and dependencies.
   e.g. `(comp-kw jetty {:port 8080} {:handler :handler})`"
  [program cfg-map deps-map]
  [any? [:map-of keyword? any?] [:map-of keyword? any?]
   => [:map [:flc.component/program any?]]]
  (fx.kw-args/component program cfg-map deps-map))


(defn fc-state-map
  [started]
  (fc.map/->map (fc.core/states started)))


(defn state
  "Takes the output from `start!` and returns a map of states."
  [sys]
  (fc-state-map (get-in sys [::system :started])))


(defn parse-config [{:keys [:config :components]}]
  (let [consts (fc.core/constants config)
        comps  (into {}
                     (keep (fn [[k {:keys [:component :config :deps]
                                    :or   {config {}
                                           deps   {}}}]]
                             (when component
                               [k (comp-kw component config deps)])))
                     components)]
    (fx.try/try-all
     (reduce into [] [consts comps]))))


(defn system [sys-config]
  (let [comps (parse-config sys-config)]
    {::system {:started    []
               :components comps}}))


(defn stop! [sys]
  (contrib/cond
    :let [prev-sys (::system sys)]

    :let [[success failed] (try [(fx.partial/stop+! prev-sys) nil]
                                (catch ExceptionInfo ex
                                  [nil {:msg  (ex-message ex)
                                        :data (ex-data ex)}]))]

    (some? failed)
    {::system  prev-sys
     ::failure {:kind    :failed-to-stop
                :details failed}}

    :else
    {::system  (:system success)
     ::success {:kind         :stopped
                :just-stopped (map first (:stopped success))}}))


(defn start!
  ([sys names]
   (contrib/cond

     :let [prev-sys (::system sys)
           available-components (->> (:components prev-sys)
                                     (map first)
                                     (set))]

     (seq (set/difference (set names) available-components))
     {::system  prev-sys
      ::failure {:kind      :missing-component
                 :available available-components
                 :requested (set names)
                 :missing   (set/difference (set names) available-components)}}

     :let [[success failed] (try [(fx.partial/start+! prev-sys names) nil]
                                 (catch ExceptionInfo ex
                                   [nil {:msg  (ex-message ex)
                                         :data (ex-data ex)}]))]

     (some? failed)
     {::system  prev-sys
      ::failure {:kind      :ambiguous-deps
                 :msg       (:msg failed)
                 :sorted    (:sorted (:data failed))
                 :ambiguous (:ambiguous (:data failed))
                 :missing   (set (keep (fn [[k v]] (when (nil? v) k))
                                       (:dependencies (:data failed))))}}

     :let [wrapped-started  (:started (:system success))
           new-started      (:started success)
           states           (fc-state-map wrapped-started)
           failures         (fx.try/failing-dependencies states)]

     (nil? failures)
     (let [curr-started (fc.map/fmap #(fc.process/update-state
                                       % (fn [x] (if (and (map? x)
                                                          (contains? x :success))
                                                   (:success x)
                                                   x)))
                                     wrapped-started)
           curr-sys     (assoc (:system success) :started curr-started)]
       {::system  curr-sys
        ::success {:kind               :started
                   :currently-running  (->> curr-sys :started (map first))
                   :previously-running (->> prev-sys :started (map first))
                   :just-started       (map first new-started)}})

     :let [primaries   (keep #(when (nil? (second %)) (first %)) failures)
           dependents  (into {} (filter #(seq (second %)) failures))
           errors      (reduce-kv
                        (fn [m k v]
                          (assoc m k (ex-cause (:failure v))))
                        {}
                        (select-keys states primaries))]

     :else
     {::system  (:system success)
      ::failure {:kind              :failed-to-start
                 :failed-components primaries
                 :failed-dependents dependents
                 :primary-errors    errors}})))


(defn restart! [old-sys new-sys new-names]
  (contrib/cond
    :let [prev-sys (::system old-sys)]

    :let [stopping (stop! old-sys)]

    (::failure stopping)
    {::system  prev-sys
     ::failure {:kind    :failed-to-restart-stop
                :details (::failure stopping)}}

    :let [starting (start! new-sys new-names)]

    (::failure starting)
    {::system  prev-sys
     ::failure {:kind    :failed-to-restart-start
                :details (::failure starting)}}

    :else
    {::system  (::system starting)
     ::success {:kind         :restarted
                :just-stopped (get-in stopping [::success :just-stopped])
                :just-started (get-in starting [::success :just-started])}}))
