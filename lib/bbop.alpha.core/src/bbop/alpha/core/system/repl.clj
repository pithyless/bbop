(ns bbop.alpha.core.system.repl
  (:require
   [bbop.alpha.core.contrib :as contrib]
   [bbop.alpha.core.malli :as malli]
   [bbop.alpha.core.system.impl :as system.impl]
   [clojure.tools.namespace.repl :as c.t.n.repl]))


(c.t.n.repl/disable-reload!)


(defonce system-ref
  (atom nil))


(defonce system-init-fn
  (atom nil))


(defn new-sys-config []
  (contrib/cond
    :let [init-fn @system-init-fn]

    (nil? init-fn)
    (throw (ex-info "Missing init-fn. Did you forget to `set-init-fn`?" {}))

    :do (malli/assert [:fn fn?] init-fn)

    :let [new-sys-config (init-fn)]

    :do (malli/assert [:map
                       [:config [:map]]
                       [:components [:map]]] new-sys-config)

    :else
    new-sys-config))


;;;; Public API


(defn status []
  (dissoc @system-ref ::system.impl/system))


(defn state []
  (when-let [curr-sys @system-ref]
    (system.impl/state curr-sys)))


(defn set-init-fn [sys-config-fn]
  (reset! system-init-fn sys-config-fn))


(defn start [names]
  (contrib/cond
    :let [new-cfg (new-sys-config)
          new-sys (system.impl/system new-cfg)]

    :let [prev-sys @system-ref]

    :let [refresh-result (if prev-sys
                           (c.t.n.repl/refresh)
                           :ok)]

    (not= :ok refresh-result)
    {::system.impl/system  prev-sys
     ::system.impl/failure {:kind    :namespace-refresh-failed
                            :details refresh-result}}

    :let [final-sys (if prev-sys
                      (system.impl/restart! prev-sys new-sys names)
                      (system.impl/start! new-sys names))]

    :do (reset! system-ref final-sys)

    (status)))


(defn stop []
  (let [prev-sys @system-ref
        new-sys  (when prev-sys
                   (system.impl/stop! prev-sys))]
    (reset! system-ref new-sys)
    (status)))
