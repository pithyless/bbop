(ns bbop.alpha.core.log
  #?(:cljs (:require-macros [bbop.alpha.core.log]))
  (:require
   [bbop.alpha.core.namespace :as namespace]
   #?(:clj [clojure.tools.logging :as clj-log])))


(defmacro info [kw expr]
  (let [cljs? (namespace/cljs-env? &env)]
    (if cljs?
      `(do (js/console.info {:level :info :name ~kw :val ~expr})
           nil)
      `(do (clj-log/info {:level :info :name ~kw :val ~expr})
           nil))))


(defmacro warn [kw expr]
  (let [cljs? (namespace/cljs-env? &env)]
    (if cljs?
      `(do (js/console.warn {:level :warn :name ~kw :val ~expr})
           nil)
      `(do (clj-log/warn {:level :warn :name ~kw :val ~expr})
           nil))))


(defmacro error [kw expr]
  (let [cljs? (namespace/cljs-env? &env)]
    (if cljs?
      `(do (js/console.error {:level :error :name ~kw :val ~expr})
           nil)
      `(do (clj-log/error {:level :error :name ~kw :val ~expr})
           nil))))
