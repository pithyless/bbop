(ns bbop.alpha.core.time
  (:refer-clojure :exclude [inst?])
  #?(:cljs (:require-macros [bbop.alpha.core.time]))
  (:require
   [tick.timezone]      ;; Load timezones
   [tick.locale-en-us]  ;; Load formatters
   [bbop.alpha.core.namespace :refer [import-vars]]
   [bbop.alpha.core.time.impl :as time.impl]
   [tick.alpha.api :as tick]
   [cljc.java-time.clock :as cljc-time.clock]))


#?(:clj (import-vars [tick.alpha.api
                      with-clock]))


(import-vars [tick.alpha.api
              instant])


(defn inst? [x]
  (time.impl/inst? x))


(defn now []
  (tick/now))


(defn seconds-after-epoch [inst]
  (int (tick/seconds (tick/between (tick/epoch) inst))))


(defn fixed-clock
  ([inst]
   (fixed-clock inst tick/UTC))
  ([inst zone]
   (cljc-time.clock/fixed inst zone)))
