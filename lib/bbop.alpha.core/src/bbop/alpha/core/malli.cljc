(ns bbop.alpha.core.malli
  (:refer-clojure :exclude [assert merge])
  (:require
   [bbop.alpha.core.namespace :refer [import-vars]]
   [malli.core]
   [malli.util]
   [malli.error]
   [malli.generator]
   [malli.provider]
   ;; [malli.edn]
   ;; [malli.json-schema]
   ;; [malli.swagger]
   ))


(declare explain humanize validate)

;; (import-vars [malli.edn
;;               write-string
;;               read-string])

(import-vars [malli.core
              explain
              schema?
              schema
              explainer
              validator
              validate])


(import-vars [malli.util
              merge])


(import-vars [malli.generator
              generate
              generator
              sample])


(import-vars [malli.provider
              provide])


(import-vars [malli.error
              humanize])


(defn explain-full [spec data]
  (when-some [error (explain spec data)]
    (assoc error :msg (humanize error))))


(defn explain-msg [spec data]
  (some-> (explain spec data)
          (humanize)))


(defn assert
  ([schema data] (assert schema data nil))
  ([schema data msg]
   (if (try
         (validate schema data)
         (catch #?(:cljs :default :clj Throwable) e
           (throw (ex-info (str "Failed to validate spec - " msg)
                           {:schema    schema
                            :data      data
                            :exception e}))))
     data
     (throw (ex-info (str "Failed to match spec - " msg)
                     {:details (explain schema data)})))))
