(ns bbop.alpha.core.system
  (:require
   [bbop.alpha.core.contrib :as contrib]
   [bbop.alpha.core.malli :as malli]
   [bbop.alpha.core.mspec.string :as mspec.string]
   [bbop.alpha.core.system.impl :as system.impl]
   [bbop.alpha.core.aero :as aero]
   [bbop.alpha.core.guard :refer [>defn =>]]
   [clojure.java.io :as jio]))


(defn read-config
  [{:keys [:filename :profile]}]
  [[:map
    [:filename mspec.string/present]
    [:profile  [:enum :prod :dev :test]]
    => [:map [:components [:map]]]]]
  (aero/read-config filename profile))


(def lifecycle system.impl/lifecycle)


(def state system.impl/state)
