(ns bbop.alpha.core.namespace
  #?(:cljs (:require-macros [bbop.alpha.core.namespace]))
  #?(:clj (:require [potemkin :as potemkin])))


(defmacro cljs-import-vars [& syms]
  (let [unravel (fn unravel [x]
                  (if (sequential? x)
                    (->> x
                         rest
                         (mapcat unravel)
                         (map
                          #(symbol
                            (str (first x)
                                 (when-let [n (namespace %)]
                                   (str "." n)))
                            (name %))))
                    [x]))
        syms (mapcat unravel syms)]
    `(do
       ~@(map
          (fn [sym]
            `(def ~(symbol (name sym)) ~sym))
          syms))))


(defmacro clj-import-vars [& syms]
  `(potemkin/import-vars ~@syms))


(defn cljs-env?
  "Take the &env from a macro, and tell whether we are expanding into CLJS.
   Source: https://github.com/Prismatic/schema/blob/master/src/clj/schema/macros.clj"
  [env] (boolean (:ns env)))


;; TODO: https://github.com/cgrand/macrovich

(defmacro if-cljs
  "Return then if we are generating cljs code and else for Clojure code.
   Source: https://github.com/Prismatic/schema/blob/master/src/clj/schema/macros.clj"
  {:style/indent 0}
  [then else]
  (if (cljs-env? &env) then else))


(defmacro import-vars
  [& syms]
  `(if-cljs
     (cljs-import-vars ~@syms)
     (clj-import-vars ~@syms)))
