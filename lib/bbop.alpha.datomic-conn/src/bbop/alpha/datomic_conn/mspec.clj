(ns bbop.alpha.datomic-conn.mspec
  (:require
   [bbop.alpha.core.mspec.protocol :as mspec.protocol]
   [bbop.alpha.datomic-conn.protocol :as protocol]))


(def tx-data
  [:vector {:min 1} any?])


(def tx-result
  [:map
   [:db-before any?]
   [:db-after any?]
   [:tx-data any?]
   [:tempids map?]])


(def conn
  (mspec.protocol/satisfies protocol/DatomicConn))
