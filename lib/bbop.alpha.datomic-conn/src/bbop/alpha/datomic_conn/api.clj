(ns bbop.alpha.datomic-conn.api
  (:require
   [bbop.alpha.datomic-conn.preload]
   [bbop.alpha.core.guard :refer [>defn =>]]
   [bbop.alpha.datomic-conn.mspec :as mspec.datomic-conn]
   [bbop.alpha.datomic.mspec :as mspec.datomic]
   [bbop.alpha.datomic-conn.protocol :as protocol]))


(>defn db
  [comp]
  [mspec.datomic-conn/conn => mspec.datomic/db]
  (protocol/db comp))


(>defn transact
  [comp tx-data]
  [mspec.datomic-conn/conn mspec.datomic-conn/tx-data
   => mspec.datomic-conn/tx-result]
  @(protocol/transact comp tx-data))


(>defn fork
  [comp]
  [mspec.datomic-conn/conn => mspec.datomic-conn/conn]
  (protocol/fork comp))
