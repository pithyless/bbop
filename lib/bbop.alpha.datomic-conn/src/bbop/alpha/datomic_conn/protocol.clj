(ns ^{:clojure.tools.namespace.repl/unload false
      :clojure.tools.namespace.repl/load   false}
    bbop.alpha.datomic-conn.protocol)


(defprotocol DatomicConn
  (db [this])
  (transact [this tx-data])
  (fork [this]))
