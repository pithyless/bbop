(ns bbop.alpha.pathom.comp.parser
  (:require [com.wsscode.pathom.core :as p]
            [com.wsscode.pathom.connect :as pc]
            [bbop.alpha.core.system :as system]
            [bbop.alpha.core.malli :as malli]))


;; (defn process-error []
;;   "If there were any exceptions in the parser that cause complete failure we
;;   respond with a well-known message that the client can handle."
;;   )
;; (defn process-error
;;   [env err]
;;   (let [msg  (.getMessage err)
;;         data (or (ex-data err) {})]
;;     (log/error "Parser Error:" msg data)
;;     {::errors {:message msg
;;                :data    data}}))


;; (p/transduce-maps
;;  (map (fn [[k v]]
;;         (println k)
;;         (if (= ::p/reader-error k)
;;           nil
;;           [k v])))
;;  {:foo {::p/reader-error :baz}})


;; (defn elide-reader-errors
;;   [input]
;;   (with-meta
;;     (p/transduce-maps (map (fn [[k v]]
;;                              (if (= ::p/reader-error v)
;;                                [k nil]
;;                                [k v])))
;;                       input)
;;     (meta input)))


(defn new-parser [registries]
  (p/parallel-parser
   {::p/env     {::p/reader               [p/map-reader
                                           pc/parallel-reader
                                           pc/open-ident-reader
                                           p/env-placeholder-reader]
                 ::p/placeholder-prefixes #{">"}}
    ::p/mutate  pc/mutate-async
    ::p/plugins [(pc/connect-plugin {::pc/register registries})
                 ;; (p/env-plugin {::p/process-error process-error})
                 ;; (p/post-process-parser-plugin elide-reader-errors)
                 p/error-handler-plugin
                 p/elide-special-outputs-plugin
                 p/trace-plugin]}))


(defn parse-registries [registries]
  ;; Registries could be arbitrarily nested in vectors,
  ;; but in this case we want to flatten to nearest map.
  (let [registries (vec (flatten registries))]
    (malli/assert [:vector {:min 1}
                   [:map [::pc/sym symbol?]]]
                  registries)))


(defn component-start [{:keys [:registries :attr-registry]}]
  (let [reg (parse-registries registries)]
    {:parser        (new-parser reg)
     :attr-registry (vec (flatten attr-registry))
     :registry      reg}))


(def component
  (system/lifecycle #'component-start))
