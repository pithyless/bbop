(ns bbop.alpha.pathom.api
  (:require
   [bbop.alpha.pathom.preload]
   [clojure.core.async :as async]))


(defn parse-async [{:keys [:parser]} opts query]
  ;; TODO - validate parser, opts, query -> return 400 bad request from api if invalid
  (parser opts query))


(defn parse [component opts query]
  ;; TODO - validate parser, opts, query -> return 400 bad request from api if invalid
  (async/<!! (parse-async component opts query)))
