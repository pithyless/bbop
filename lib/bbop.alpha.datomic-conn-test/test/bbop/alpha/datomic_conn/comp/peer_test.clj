(ns bbop.alpha.datomic-conn.comp.peer-test
  (:require
   [bbop.alpha.core.system :as system]
   [bbop.alpha.datomic-conn.api :as datomic-conn]
   [bbop.alpha.datomic-conn.comp.peer :as datomic-conn.comp.peer]
   [bbop.alpha.datomic-conn.protocol :as protocol]
   [bbop.alpha.datomic.api :as datomic]
   [bbop.alpha.test.api :as test :refer [specification component behavior assertions]]))


(def test-config
  {:config
   {:cfg/datomic-conn-uri "datomic:mem://in-mem-test-db"}

   :components
   {:comp/datomic-conn
    {:component #'datomic-conn.comp.peer/component
     :config    {:delete-existing? true
                 :release-conn?    false}
     :deps      {:uri :cfg/datomic-conn-uri}}}})


(specification "Datomic Connection"
  (test/with-system [system [test-config [:comp/datomic-conn]]]
    (component "Datomic Peer"
      (let [comp (-> (system/state system) :comp/datomic-conn)]
        (assertions

         "instance of datomic-conn"
         comp =fn=> #(satisfies? protocol/DatomicConn %)

         "connected to datomic peer"
         (str (type comp)) => "class bbop.alpha.datomic_conn.comp.peer.DatomicPeer"

         "migration"
         (datomic-conn/transact
          comp
          [{:db/ident       :user/name
            :db/valueType   :db.type/string
            :db/index       true
            :db/cardinality :db.cardinality/one}])
         =fn=> #(some? (:db-after %))

         "persist Bob (original)"
         (datomic-conn/transact comp [{:user/name "Bob"}])
         =fn=> #(some? (:db-after %))

         "fetch people (original)"
         (some->>
          (datomic/datoms (datomic-conn/db comp) :avet :user/name)
          (mapv :v) (sort))
         => ["Bob"])

        (component "Datomic Fork"
          (let [fork (datomic-conn/fork comp)]
            (assertions

             "forked datomic instance"
             fork =fn=> #(satisfies? protocol/DatomicConn %)
             (type fork) => datomock.impl.MockConnection

             "persist Jill (fork)"
             (datomic-conn/transact fork [{:user/name "Jill"}])
             =fn=> #(some? (:db-after %))

             "persist Jack (original)"
             (datomic-conn/transact comp[{:user/name "Jack"}])
             =fn=> #(some? (:db-after %))

             "fetch people (fork)"
             (some->>
              (datomic/datoms (datomic-conn/db fork) :avet :user/name)
              (mapv :v) (sort))
             => ["Bob" "Jill"]

             "fetch people (original)"
             (some->>
              (datomic/datoms (datomic-conn/db comp) :avet :user/name)
              (mapv :v) (sort))
             => ["Bob" "Jack"]
             )))))))
