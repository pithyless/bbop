(ns bbop.alpha.test.impl
  (:require
   [clojure.spec.alpha :as s]
   [expound.alpha :as expound]
   [bbop.alpha.core.log :as log]
   #?(:clj [unilog.config :as unilog]))
  #?(:clj (:import [java.lang Thread$UncaughtExceptionHandler])))


(defn setup-logging []
  #?(:clj
     (let [config {:level     :debug
                   :overrides {"datomic.process-monitor" :warn
                               "datomic.db"              :warn
                               "datomic.domain"          :warn
                               "io.netty"                :warn
                               "io.undertow"             :warn
                               "org.apache.http"         :warn
                               "org.xnio.nio"            :warn}}]
       (unilog/start-logging! config))))


(defn install-uncaught-exception-handler []
  #?(:clj
     (Thread/setDefaultUncaughtExceptionHandler
      (reify Thread$UncaughtExceptionHandler
        (uncaughtException [_ _ ex]
          (log/error ::uncaught-exception
                     {:msg (or (.getMessage ex)
                               (-> ex .getClass .getName))
                      ;; :ex ex
                      }))))))


(defn setup-errors []
  #?(:clj
     (do
       (s/check-asserts true)

       ;; https://github.com/bhb/expound/blob/master/doc/faq.md#using-set
       ;; (alter-var-root #'s/*explain-out* (constantly expound/printer))
       ;; (set! s/*explain-out* expound/printer)

       ;;(aviso.repl/install-pretty-exceptions)
       ;;(aviso.logging/install-pretty-logging)
       (install-uncaught-exception-handler))))
