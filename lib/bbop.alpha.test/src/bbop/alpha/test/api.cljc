(ns bbop.alpha.test.api
  #?(:cljs (:require-macros [bbop.alpha.test.api]))
  (:require
   [bbop.alpha.core.preload]

   #?(:clj [bbop.alpha.core.system.impl :as system.impl])
   #?(:cljs [cljs.test    :as cljs-test]
      :clj  [clojure.test :as clj-test])
   #?(:cljs [matcher-combinators.cljs-test]
      :clj  [matcher-combinators.clj-test])
   [matcher-combinators.test]

   [nubank.workspaces.core :as workspaces]
   [bbop.alpha.core.malli :as malli]
   [bbop.alpha.core.namespace :refer [import-vars cljs-env?]]
   [bbop.alpha.test.impl :as test.impl]
   [fulcro-spec.core]))


#?(:clj (import-vars [fulcro-spec.core
                      assertions
                      behavior
                      component
                      provided
                      specification
                      ]))


(defmacro deftest [& args]
  (let [cljs? (cljs-env? &env)]
    (if cljs?
      `(workspaces/deftest ~@args)
      `(clojure.test/deftest ~@args))))


(defmacro testing [& args]
  (let [cljs? (cljs-env? &env)]
    (if cljs?
      `(cljs.test/testing ~@args)
      `(clojure.test/testing ~@args))))


(defmacro is [& syms]
  (let [cljs? (cljs-env? &env)]
    (if cljs?
      `(cljs.test/is ~@syms)
      `(clojure.test/is ~@syms))))


(defmacro is-true [expected]
  `(is (true? ~expected)))


(defmacro is-false [expected]
  `(is (false? ~expected)))


(defmacro is-valid [spec value]
  `(let [spec#   ~spec
         value#  ~value
         result# (malli/validate spec# value#)]
     (is (= {:valid? true
             :explain nil}
            {:valid? result#
             :explain (malli/explain-full spec# value#)}))))


(defmacro isnt-valid [spec value]
  `(let [spec#   ~spec
         value#  ~value
         result# (malli/validate spec# value#)]
     (is (= {:valid? false
             :spec   spec#
             :value  value#}
            {:valid? result#
             :spec   spec#
             :value  value#}))))


;; (defmacro is-match [query actual]
;;   `(is ( ~query ~actual)))


(defmacro is= [expected actual]
  `(is (= ~expected ~actual)))


;; TODO: deprecate
(defmacro is? [expected actual]
  `(is (~'match? ~expected ~actual)))


#?(:clj
   (defmacro with-system
     [[system-sym [config components]] & body]
     `(let [system#     (system.impl/system ~config)
            instance#   (system.impl/start! system# ~components)
            ~system-sym instance#]
        (when-not (:bbop.alpha.core.system.impl/success instance#)
          (system.impl/stop! instance#)
          (throw (ex-info "Cannot start system." {:data instance#})))
        (try ~@body
             (finally (system.impl/stop! instance#))))))


(defn pre-test-runs []
  (test.impl/setup-logging)
  (test.impl/setup-errors))
