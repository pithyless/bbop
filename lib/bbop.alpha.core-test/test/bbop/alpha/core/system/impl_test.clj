(ns bbop.alpha.core.system.impl-test
  (:require
   [bbop.alpha.test.api :as test :refer [specification component behavior assertions]]
   [bbop.alpha.core.contrib :as contrib]
   [bbop.alpha.core.system.impl :as impl]))


(def jetty-component
  (impl/lifecycle
   (fn [{:keys [:port :handler :primary?]}]
     {:jetty-status   :running
      :jetty-primary? primary?
      :jetty-port     port
      :jetty-handler  handler})
   (fn [{:keys [:jetty-port]}]
     {:jetty-status :shutdown
      :closed-port  jetty-port})))


(def handler-component
  (impl/lifecycle
   (fn [{:keys [:dice-roll]}]
     {:handler-status :running
      :random         dice-roll})))


(def broken-component
  (impl/lifecycle
   (fn [_]
     (throw (ex-info "Failing Component" {:details :my-component-error})))))


(def broken-stop-component
  (impl/lifecycle
   (fn [_] {})
   (fn [_] (throw (ex-info "CLOSED!" {:details :my-broken-stop})))))


(def default-cfg
  {:config
   {:cfg/main-web-server-port      9999
    :cfg/secondary-web-server-port 8888
    :cfg/meaning-of-life           42}

   :components
   {:comp/main-web-server
    {:component #'jetty-component
     :config    {:primary? true}
     :deps      {:handler :comp/main-ring-handler
                 :port    :cfg/main-web-server-port}}
    :comp/secondary-web-server
    {:component #'jetty-component
     :config    {:primary? false}
     :deps      {:handler :comp/main-ring-handler
                 :port    :cfg/secondary-web-server-port}}
    :comp/main-ring-handler
    {:component #'handler-component
     :deps      {:dice-roll :cfg/meaning-of-life}}}})


(defn component-names [sys]
  (->> (get-in sys [::impl/system :components])
       (map first)
       set))


(defn find-component [sys name]
  (->> (get-in sys [::impl/system :components])
       (filter #(= name (first %)))
       last
       second))

(defn component-program [sys name]
  (-> (find-component sys name)
      :flc.component/program))


(defn component-dependencies [sys name]
  (-> (find-component sys name)
      :flc.component/dependencies
      set))


(specification "system"
  (behavior "parse config"
    (contrib/cond
      :do (assertions

           "constants"
           (component-names
            (impl/system (select-keys default-cfg [:config])))
           => #{:cfg/main-web-server-port
                :cfg/secondary-web-server-port
                :cfg/meaning-of-life}


           "const-component program"
           (-> default-cfg
               (impl/system)
               (component-program :cfg/meaning-of-life))
           =fn=> some?


           "const-component dependencies"
           (-> default-cfg
               (impl/system)
               (component-dependencies :cfg/meaning-of-life))
           => #{}


           "components - kw-component"
           (component-names
            (impl/system default-cfg))
           => #{:cfg/main-web-server-port
                :cfg/secondary-web-server-port
                :cfg/meaning-of-life
                :comp/main-web-server
                :comp/secondary-web-server
                :comp/main-ring-handler}


           "kw-component program"
           (-> default-cfg
               (impl/system)
               (component-program :comp/main-web-server))
           =fn=> some?


           "kw-component dependencies"
           (-> default-cfg
               (impl/system)
               (component-dependencies :comp/main-web-server))
           => #{:comp/main-ring-handler :cfg/main-web-server-port}


           "empty config"
           (impl/system {})
           => {::impl/system {:components []
                              :started    []}}
           ))))


(specification "start!"
  (behavior "start components"
    (contrib/cond
      :let [system (impl/system default-cfg)
            system2 (-> default-cfg
                        (assoc-in [:components :comp/missing-dep]
                                  {:component #'jetty-component
                                   :deps      {:port :cfg/missing-port}})
                        (assoc-in [:components :comp/broken-component]
                                  {:component #'broken-component
                                   :deps      {}})
                        (assoc-in [:components :comp/broken-dep]
                                  {:component #'jetty-component
                                   :deps      {:port :comp/broken-component}})
                        (impl/system))]
      :do (assertions

           "partial start"
           (-> system
               (impl/start! [:cfg/meaning-of-life])
               (::impl/success))
           => {:kind               :started
               :currently-running  [:cfg/meaning-of-life]
               :previously-running []
               :just-started       [:cfg/meaning-of-life]}


           "partial start states"
           (-> system
               (impl/start! [:cfg/meaning-of-life])
               impl/state)
           => {:cfg/meaning-of-life 42}


           "staged start"
           (-> system
               (impl/start! [:cfg/meaning-of-life])
               (impl/start! [:cfg/main-web-server-port])
               ::impl/success)
           => {:kind               :started
               :currently-running  [:cfg/main-web-server-port
                                    :cfg/meaning-of-life]
               :previously-running [:cfg/meaning-of-life]
               :just-started       [:cfg/main-web-server-port]}


           "staged start states"
           (-> system
               (impl/start! [:cfg/meaning-of-life])
               (impl/start! [:cfg/main-web-server-port])
               impl/state)
           => {:cfg/meaning-of-life      42
               :cfg/main-web-server-port 9999}


           "started dependency states"
           (-> system
               (impl/start! [:comp/main-web-server])
               impl/state)
           => {:cfg/main-web-server-port 9999
               :cfg/meaning-of-life      42
               :comp/main-ring-handler
               {:handler-status :running
                :random         42}
               :comp/main-web-server
               {:jetty-status   :running
                :jetty-primary? true
                :jetty-port     9999
                :jetty-handler  {:handler-status :running
                                 :random         42}}}


           "noop staged start"
           (-> system
               (impl/start! [:cfg/meaning-of-life])
               (impl/start! [:cfg/main-web-server-port])
               (impl/start! [:cfg/meaning-of-life
                             :cfg/main-web-server-port])
               ::impl/success)
           => {:kind               :started
               :currently-running  [:cfg/main-web-server-port
                                    :cfg/meaning-of-life]
               :previously-running [:cfg/main-web-server-port
                                    :cfg/meaning-of-life]
               :just-started       []}


           "noop staged start states"
           (-> system
               (impl/start! [:cfg/meaning-of-life])
               (impl/start! [:cfg/main-web-server-port])
               (impl/start! [:cfg/meaning-of-life
                             :cfg/main-web-server-port])
               impl/state)
           => {:cfg/meaning-of-life      42
               :cfg/main-web-server-port 9999}


           "ignore missing deps that won't be started"
           (::impl/success (impl/start! system2 [:cfg/meaning-of-life]))
           => {:kind               :started
               :currently-running  [:cfg/meaning-of-life]
               :previously-running []
               :just-started       [:cfg/meaning-of-life]}


           "fail on missing deps"
           (::impl/failure (impl/start! system2 [:comp/missing-dep]))
           => {:kind      :ambiguous-deps
               :msg       "Ambiguous ordering"
               :sorted    []
               :ambiguous #:comp{:missing-dep #{:cfg/missing-port}}
               :missing   #{}}


           "fail on missing component"
           (::impl/failure (impl/start! system [:comp/not-a-real-component]))
           => {:kind      :missing-component
               :available #{:comp/secondary-web-server :cfg/main-web-server-port
                            :comp/main-ring-handler :cfg/meaning-of-life
                            :cfg/secondary-web-server-port :comp/main-web-server}
               :requested #{:comp/not-a-real-component}
               :missing   #{:comp/not-a-real-component}}


           "fail on broken component"
           (dissoc (::impl/failure
                    (impl/start! system2 [:comp/broken-component]))
                   :primary-errors)
           => {:kind              :failed-to-start
               :failed-components [:comp/broken-component]
               :failed-dependents {}}


           "original exceptions for broken component"
           (ex-data
            (get-in
             (impl/start! system2 [:comp/broken-component])
             [::impl/failure :primary-errors :comp/broken-component]))
           => {:details :my-component-error}


           "fail on broken dependency"
           (dissoc (::impl/failure
                    (impl/start! system2 [:comp/broken-dep]))
                   :primary-errors)
           => {:kind              :failed-to-start
               :failed-components [:comp/broken-component]
               :failed-dependents {:comp/broken-dep [:comp/broken-component]}}


           "original exceptions for broken dependency"
           (ex-data
            (get-in
             (impl/start! system2 [:comp/broken-dep])
             [::impl/failure :primary-errors :comp/broken-component]))
           => {:details :my-component-error}
           ))))


(specification "stop!"
  (behavior "stop components"
    (contrib/cond
      :let [system (impl/system default-cfg)
            system2 (-> default-cfg
                        (assoc-in [:components :comp/missing-dep]
                                  {:component #'jetty-component
                                   :deps      {:port :cfg/missing-port}})
                        (assoc-in [:components :comp/broken-component]
                                  {:component #'broken-component
                                   :deps      {}})
                        (assoc-in [:components :comp/broken-stop-component]
                                  {:component #'broken-stop-component
                                   :deps      {}})
                        (assoc-in [:components :comp/broken-stop-dep]
                                  {:component #'jetty-component
                                   :deps      {:port :comp/broken-stop-component}})
                        (impl/system))]
      (assertions

       "after successful start"
       (-> system
           (impl/start! [:cfg/meaning-of-life])
           (impl/start! [:cfg/main-web-server-port])
           (impl/stop!)
           ::impl/success)
       => {:kind         :stopped
           :just-stopped [:cfg/main-web-server-port :cfg/meaning-of-life]}


       "after failed start"
       (-> system
           (impl/start! [:cfg/meaning-of-life])
           (impl/start! [:comp/broken-component])
           (impl/stop!)
           ::impl/success)
       => {:kind         :stopped
           :just-stopped [:cfg/meaning-of-life]}


       "empty start"
       (-> system
           (impl/start! [])
           (impl/stop!)
           ::impl/success)
       => {:kind         :stopped
           :just-stopped []}


       "fail on broken component-stop"
       (-> system2
           (impl/start! [:comp/broken-stop-component])
           (impl/stop!)
           ::impl/failure)
       => {:kind    :failed-to-stop
           :details {:msg  "CLOSED!"
                     :data {:details :my-broken-stop}}}


       "fail on broken component-stop dependency"
       (-> system2
           (impl/start! [:comp/broken-stop-dep])
           (impl/stop!)
           ::impl/failure)
       => {:kind    :failed-to-stop
           :details {:msg  "CLOSED!"
                     :data {:details :my-broken-stop}}}
       ))))



(specification "restart!"
  (behavior "restart components"
    (contrib/cond
      :let [system (impl/system default-cfg)
            system2 (-> default-cfg
                        (assoc-in [:components :comp/missing-dep]
                                  {:component #'jetty-component
                                   :deps      {:port :cfg/missing-port}})
                        (assoc-in [:components :comp/broken-component]
                                  {:component #'broken-component
                                   :deps      {}})
                        (assoc-in [:components :comp/broken-stop-component]
                                  {:component #'broken-stop-component
                                   :deps      {}})
                        (assoc-in [:components :comp/broken-stop-dep]
                                  {:component #'jetty-component
                                   :deps      {:port :comp/broken-stop-component}})
                        (impl/system))]
      (assertions

       "returns new system"
       (-> system
           (impl/start! [:comp/main-web-server])
           (impl/start! [:comp/secondary-web-server])
           (impl/restart! system [:comp/main-web-server
                                  :comp/secondary-web-server])
           ::impl/system keys set)
       => #{:started :components}

       "same components"
       (-> system
           (impl/start! [:comp/main-web-server])
           (impl/start! [:comp/secondary-web-server])
           (impl/restart! system [:comp/main-web-server
                                  :comp/secondary-web-server])
           ::impl/success)
       => {:kind :restarted
           :just-stopped
           [:comp/secondary-web-server
            :cfg/secondary-web-server-port
            :comp/main-web-server
            :comp/main-ring-handler
            :cfg/meaning-of-life
            :cfg/main-web-server-port]
           :just-started
           [:comp/secondary-web-server
            :comp/main-web-server
            :comp/main-ring-handler
            :cfg/meaning-of-life
            :cfg/secondary-web-server-port
            :cfg/main-web-server-port]}


       "more components"
       (-> system
           (impl/start! [:comp/main-web-server])
           (impl/restart! system [:comp/main-web-server
                                  :comp/secondary-web-server])
           ::impl/success)
       => {:kind :restarted
           :just-stopped
           [:comp/main-web-server
            :comp/main-ring-handler
            :cfg/meaning-of-life
            :cfg/main-web-server-port]
           :just-started
           [:comp/secondary-web-server
            :comp/main-web-server
            :comp/main-ring-handler
            :cfg/meaning-of-life
            :cfg/secondary-web-server-port
            :cfg/main-web-server-port]}


       "fewer components"
       (-> system
           (impl/start! [:comp/main-web-server])
           (impl/start! [:comp/secondary-web-server])
           (impl/restart! system [:comp/main-web-server])
           ::impl/success)
       => {:kind :restarted
           :just-stopped
           [:comp/secondary-web-server
            :cfg/secondary-web-server-port
            :comp/main-web-server
            :comp/main-ring-handler
            :cfg/meaning-of-life
            :cfg/main-web-server-port]
           :just-started
           [:comp/main-web-server
            :comp/main-ring-handler
            :cfg/meaning-of-life
            :cfg/main-web-server-port]}


       "failed restart-stop"
       (-> system2
           (impl/start! [:cfg/meaning-of-life
                         :comp/broken-stop-component])
           (impl/restart! system2 [:cfg/meaning-of-life])
           ::impl/failure)
       => {:kind    :failed-to-restart-stop
           :details {:kind    :failed-to-stop
                     :details {:msg  "CLOSED!"
                               :data {:details :my-broken-stop}}}}


       "failed restart-start"
       ((juxt
         :kind
         #(dissoc (:details %) :primary-errors)
         #(ex-message (get-in % [:details :primary-errors :comp/broken-component]))
         #(ex-data  (get-in % [:details :primary-errors :comp/broken-component])))
        (-> system
            (impl/start! [:cfg/meaning-of-life])
            (impl/restart! system2 [:comp/broken-component])
            ::impl/failure))
       => [:failed-to-restart-start
           {:kind              :failed-to-start
            :failed-components [:comp/broken-component]
            :failed-dependents {}}
           "Failing Component"
           {:details :my-component-error}]
       ))))
