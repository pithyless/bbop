(ns bbop.alpha.core.mspec-test
  (:require
   [bbop.alpha.test.api :as test :refer [deftest testing is-valid isnt-valid]]
   [bbop.alpha.core.uuid :as uuid]
   [bbop.alpha.core.time :as time]
   [bbop.alpha.core.mspec.string :as mspec.string]
   [bbop.alpha.core.mspec.time :as mspec.time]
   [bbop.alpha.core.mspec.uuid :as mspec.uuid]
   [bbop.alpha.core.mspec.malli :as mspec.malli]
   [bbop.alpha.core.malli :as malli]
   #?(:clj [bbop.alpha.core.mspec.protocol :as mspec.protocol])))


(defn can-generate [spec]
  (let [size 10
        values (malli/sample spec {:seed 42 :size size})]
    (test/is= size (count values))
    (doseq [v values]
      (test/is-valid spec v))))


(deftest t-string
  (testing "string"
    (is-valid mspec.string/string "test")
    (isnt-valid mspec.string/string "")
    (isnt-valid mspec.string/string "  "))
  (testing "string gen"
    (can-generate mspec.string/string)))


(deftest t-time
  (testing "instant"
    (is-valid mspec.time/instant (time/now))
    (isnt-valid mspec.time/instant (str (time/now))))
  (testing "instant gen"
    (can-generate mspec.time/instant)))


(deftest t-uuid
  (testing "uuid"
    (is-valid mspec.uuid/uuid (uuid/random-uuid))
    (isnt-valid mspec.uuid/uuid (str (uuid/random-uuid))))
  (testing "uuid gen"
    (can-generate mspec.uuid/uuid)))


#?(:clj
   ;; TODO: add CLJS support
   (deftest t-protocol
     (testing "satisfies"
       (is-valid (mspec.protocol/satisfies Inst) (time/now))
       (isnt-valid (mspec.protocol/satisfies Inst) (str (time/now))))))


(deftest t-malli
  (testing "data-schema"
    (is-valid mspec.malli/data-schema [:map [:x int?]])
    (testing "sci core functions are implicit"
      (is-valid mspec.malli/data-schema [int?]))
    (testing "non-sci functions must be wrapped"
      (isnt-valid mspec.malli/data-schema [time/inst?])
      (is-valid mspec.malli/data-schema [:fn time/inst?]))
    (testing "must be passed as a vector"
      (isnt-valid mspec.malli/data-schema int?)
      (is-valid mspec.malli/data-schema [int?]))))
