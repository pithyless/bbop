(ns bbop.alpha.core.string-test
  (:require
   [bbop.alpha.test.api :as test :refer [specification behavior assertions]]
   [bbop.alpha.core.string :as string]))


(specification "parse-keyword"
  (let [f #(string/parse-keyword %)]
    (assertions
     "simple keyword"
     (f ":foo") => :foo
     (f ":foo-bar_baz-123") => :foo-bar_baz-123

     "qualified keyword"
     (f ":foo/bar") => :foo/bar
     (f ":foo-bar_baz-123/quux-fred_waldo-456") => :foo-bar_baz-123/quux-fred_waldo-456

     "invalid"
     (f "foo") => nil

     "empty string"
     (f "") => nil

     "nil"
     (f nil) => nil
     )))
