(ns bbop.alpha.core.uuid-test
  (:require
   [bbop.alpha.test.api :as test :refer [specification behavior assertions]]
   [bbop.alpha.core.uuid :as uuid]
   [bbop.alpha.core.time :as time]))


(specification "squuid"
  (time/with-clock (time/fixed-clock (time/instant "2010-02-03T04:05:06.789Z"))
    (let [u1 (uuid/random-squuid)
          u2 (uuid/random-squuid)]
      (assertions
       "time is frozen"
       (time/now) => #time/instant "2010-02-03T04:05:06.789Z"

       "valid uuid"
       (uuid? u1) => true
       (uuid? u2) => true

       "precision up to nearest second"
       (uuid/squuid-instant u1) => #time/instant "2010-02-03T04:05:06Z"
       (uuid/squuid-instant u2) => #time/instant "2010-02-03T04:05:06Z"

       "uniqueness"
       (not= u1 u2) => true

       "to string"
       (str u1) =fn=> #(re-find #"^4b68f5f2-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$" %)

       "parse string"
       (uuid/parse (str u1)) => u1

       "parse uuid"
       (uuid/parse u1) => u1

       "parse invalid uuid"
       (uuid/parse "not-valid-uuid") => nil))))


(specification "uuid"
  (let [u1 (uuid/random-uuid)
        u2 (uuid/random-uuid)]
    (assertions
     "valid uuid"
     (uuid? u1) => true
     (uuid? u2) => true

     "uniqueness"
     (not= u1 u2) => true

     "to string"
     (str u1) =fn=> #(re-find #"^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$" %)

     "from string"
     (uuid/parse (str u1)) => u1

     "parse uuid"
     (uuid/parse u1) => u1

     "parse invalid uuid"
     (uuid/parse "not-valid-uuid") => nil)))


(specification "fake-uuid"
  (let [u1 (uuid/fake-uuid 1)
        u2 (uuid/fake-uuid 2)
        u3 (uuid/fake-uuid 1)]
    (assertions
     "valid uuid"
     (uuid? u1) => true
     (uuid? u2) => true

     "uniqueness"
     (not= u1 u2) => true

     "repeatable"
     (= u1 u3) => true

     "to string"
     (str u1) => "ffffffff-ffff-ffff-ffff-000000000001"
     (str u2) => "ffffffff-ffff-ffff-ffff-000000000002"
     (str u3) => "ffffffff-ffff-ffff-ffff-000000000001")))
