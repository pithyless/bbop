(ns bbop.alpha.core.guard-test
  (:require
   [bbop.alpha.test.api :as test :refer [specification component behavior assertions]]
   [bbop.alpha.core.guard :as sut :refer [>defn =>]]
   [bbop.alpha.core.guard.spec :as guard.spec]
   [bbop.alpha.core.mspec :as bbop.mspec]
   [clojure.spec.alpha :as s]))


(def test-fn1-args
  '(test-fn1
    "docstring"
    ([]
     [=> int?]
     (test-fn1 1))
    ([a]
     [int? => int?]
     (if (> a 0)
       (* a (test-fn1 (dec a)))
       1))
    ([a b]
     [int? int? => int?]
     (if (> a b)
       (recur a (inc b))
       (+ a b)))))


(>defn test-fn1
  "docstring"
  ([]
   [=> int?]
   (test-fn1 1))
  ([a]
   [int? => pos-int?]
   (if (> a 0)
     (* a (test-fn1 (dec a)))
     1))
  ([a b]
   [int? int? => pos-int?]
   (if (> a b)
     (recur a (inc b))
     (+ a b))))


(def test-fn2-args
  '(test-fn2
    [a {:keys [:b] :as _bb} {:keys [:c]} [d1 d2 :as _dd] [e1 e2]]
    [int? [:map [:b int?]] [:map [:c int?]] [:tuple int? int?] [:vector int?] :=> int?]
    (+ a b c d1 d2 e1 e2)))


(>defn test-fn2
  [a {:keys [:b] :as _bb} {:keys [:c]} [d1 d2 :as _dd] [e1 e2]]
  [int? [:map [:b int?]] [:map [:c int?]] [:tuple int? int?] [:vector int?] :=> pos-int?]
  (+ a b c d1 d2 e1 e2))


(>defn test-fn3
  []
  [=> nat-int?]
  (rand-int 10))


#?(:clj
   (specification "defn-args"
     (assertions
       (s/conform ::guard.spec/gw-defn-args test-fn1-args)
       => '{:name      test-fn1,
            :docstring "docstring",
            :bs
            [:arity-n
             {:bodies
              [{:args    {},
                :gw-spec {:ret-sym =>, :ret-mspec int?},
                :body    [(test-fn1 1)]}
               {:args    {:vargs [[:sym a]]},
                :gw-spec {:arg-mspecs [int?], :ret-sym =>, :ret-mspec int?},
                :body    [(if (> a 0) (* a (test-fn1 (dec a))) 1)]}
               {:args    {:vargs [[:sym a] [:sym b]]},
                :gw-spec {:arg-mspecs [int? int?], :ret-sym =>, :ret-mspec int?},
                :body    [(if (> a b) (recur a (inc b)) (+ a b))]}]}]}

       (s/conform ::guard.spec/gw-defn-args test-fn2-args)
       => '{:name test-fn2,
            :bs
            [:arity-1
             {:args
              {:vargs
               [[:sym a]
                [:map {:keys [:b], :as _bb}]
                [:map {:keys [:c]}]
                [:seq {:elems [[:sym d1] [:sym d2]], :as [{:as :as, :sym _dd}]}]
                [:seq {:elems [[:sym e1] [:sym e2]]}]]},
              :gw-spec
              {:arg-mspecs
               [int?
                [:map [:b int?]]
                [:map [:c int?]]
                [:tuple int? int?]
                [:vector int?]],
               :ret-sym   :=>,
               :ret-mspec int?},
              :body [(+ a b c d1 d2 e1 e2)]}]})))



(specification "defn examples"
  (assertions
    ">defn with seqs and maps"
    (test-fn2 1 {:b 2} {:c 3} [4 5] [6 7])
    => 28

    ">defn with invalid arg sym"
    (test-fn2 :1 {:b 2} {:c 3} [4 5] [6 7])
    =throws=> #"Arguments do not match spec"

    ">defn with invalid destructured arg"
    (test-fn2 1 {:b 2} {:c 3} [4 "5"] [6 7])
    =throws=> #"Arguments do not match spec"

    ">defn with invalid return"
    (test-fn2 -42 {:b 2} {:c 3} [4 5] [6 7])
    =throws=> #"Return value does not match spec"

    ">defn with multi-dispatch"
    (test-fn1) => 1
    (test-fn1 3) => 6
    (test-fn1 3 2) => 6

    ">defn with multi-dispatch - invalid arg"
    (test-fn1 :42)
    =throws=> #"Arguments do not match spec"

    ">defn with multi-dispatch - invalid return"
    (test-fn1 -42 1)
    =throws=> #"Return value does not match spec"

    ">defn with no args"
    (test-fn3)
    =fn=> (set (range 10))
    ))
