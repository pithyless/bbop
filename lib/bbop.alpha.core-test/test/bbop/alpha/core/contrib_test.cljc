(ns bbop.alpha.core.contrib-test
  (:require
   [bbop.alpha.test.api :as test :refer [specification component behavior assertions]]
   [bbop.alpha.core.contrib :as contrib]
   [clojure.string :as str]))


(specification "supdate"
  (behavior "transforms data via declarative spec"
    (let [bands {:bands [{:band/id      3141
                          :band/name    "Led Zeppelin"
                          :band/members [{:person/name "roger plant"}
                                         {:person/name "jimmy page"}
                                         {:person/name "john bonham"}
                                         {:person/name "john paul jones"}]}
                         {:band/id      8242
                          :band/name    "The White Stripes"
                          :band/members [{:person/name "jack white"}
                                         {:person/name "meg white"}]}]}]
      (assertions
       "maps"
       (contrib/supdate bands {:bands [{:band/name    str/upper-case
                                        :band/members false
                                        :unknown-key  inc}]})
       => {:bands [{:band/id   3141
                    :band/name "LED ZEPPELIN"}
                   {:band/id   8242
                    :band/name "THE WHITE STRIPES"}]}))))
