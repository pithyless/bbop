(ns bbop.alpha.datomic-conn.comp.peer.mock
  (:require
   [bbop.alpha.datomic-conn.protocol :as protocol]
   [datomic.api :as api-datomic]
   [datomock.core :as datomock])
  (:import [datomock.impl MockConnection]))


(extend-protocol protocol/DatomicConn
  MockConnection
  (db [this]
    (api-datomic/db this))

  (transact [this tx-data]
    (api-datomic/transact this tx-data))

  (fork [this]
    (datomock/fork-conn this)))
