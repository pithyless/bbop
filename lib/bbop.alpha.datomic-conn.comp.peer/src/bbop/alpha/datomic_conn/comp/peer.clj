(ns bbop.alpha.datomic-conn.comp.peer
  (:require
   [bbop.alpha.datomic-conn.comp.peer.mock] ;; Needed to extend protocol
   [bbop.alpha.core.guard :refer [>defn =>]]
   [bbop.alpha.core.mspec.string :as mspec.string]
   [bbop.alpha.datomic-conn.protocol :as protocol]
   [bbop.alpha.core.system :as system]
   [datomic.api :as api-datomic]
   [datomock.core :as datomock]))


(defrecord DatomicPeer [conn uri]
  protocol/DatomicConn
  (db [_]
    (api-datomic/db conn))

  (transact [_ tx-data]
    (api-datomic/transact conn tx-data))

  (fork [_]
    (datomock/fork-conn conn)))


(defmethod print-method DatomicPeer [this ^java.io.Writer w]
  (.write w "#<DatomicPeer ")
  (print-method {:uri (:uri this)} w)
  (.write w ">"))


(defn delete-existing-db
  [{:keys [:uri :delete-existing?]}]
  (when delete-existing?
    (api-datomic/delete-database uri))
  {:deleted-existing? delete-existing?})


(defn create-new-db
  [{:keys [:uri]}]
  {:created-new? (api-datomic/create-database uri)})


(defn connect-db
  [{:keys [:uri]}]
  {:uri  uri
   :conn (api-datomic/connect uri)})


(defn releaseable-conn
  [{:keys [:release-conn?]}]
  {:release-conn? release-conn?})


(>defn component-start
  [opts]
  [[:map
    [:uri mspec.string/string]
    [:delete-existing? boolean?]
    [:release-conn? boolean?]]
   => any?]
  (map->DatomicPeer
   (reduce merge {} [(delete-existing-db opts)
                     (create-new-db opts)
                     (releaseable-conn opts)
                     (connect-db opts)])))


(defn component-stop
  [{:keys [:conn :release-conn?]}]
  (when release-conn?
    (api-datomic/release conn)))


;; Public API


(def component
  (system/lifecycle component-start component-stop))
