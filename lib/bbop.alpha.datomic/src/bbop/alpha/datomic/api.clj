(ns bbop.alpha.datomic.api
  (:require
   [bbop.alpha.core.namespace :refer [import-vars]]
   [edn-query-language.core :as eql]
   [datomic.api :as api-datomic]
   [clojure.walk :as walk]))


(import-vars [datomic.api

              as-of
              datoms
              entity
              entity-db
              pull
              pull-many
              q
              squuid
              touch])


(defn entity-by-attr [db attr attr-val]
  (some->> (api-datomic/datoms db :avet attr attr-val)
           first :e (api-datomic/entity db)))


(defn basis-txid [db]
  (-> db
      api-datomic/basis-t
      api-datomic/t->tx))


(defn- replace-ref-types
  "db   - database to query
   refs - a set of keywords that ref datomic entities,
          which you want to access directly
          (rather than retrieving the entity id)
   m    - map returned from datomic pull containing the
          entity IDs you want to deref"
  [db refs arg]
  (walk/postwalk
   (fn [arg]
     (cond
       (and (map? arg) (some #(contains? refs %) (keys arg)))
       (reduce
        (fn [acc ref-k]
          (cond
            (and (get acc ref-k) (not (vector? (get acc ref-k))))
            (update acc ref-k
                    (comp :db/ident (partial api-datomic/entity db) :db/id))

            (and (get acc ref-k) (vector? (get acc ref-k)))
            (update acc ref-k
                    #(mapv (comp :db/ident (partial api-datomic/entity db) :db/id) %))

            :else acc))
        arg
        refs)
       :else arg))
   arg))


(defn pull-*
  "Will either call d/pull or d/pull-many
  depending on if the input is sequential or not.
  Optionally: transform-fn applied to individual result(s)."
  ([db pattern eid-or-eids]
   ;; (tap> [::pull* {:ident?  (eql/ident? eid-or-eids)
   ;;                 :seq?    (sequential? eid-or-eids)
   ;;                 :many?   (and (not (eql/ident? eid-or-eids)) (sequential? eid-or-eids))
   ;;                 :pattern pattern
   ;;                 :eid     eid-or-eids
   ;;                 :res     (api-datomic/pull db pattern eid-or-eids)}])
   (->> (if (and (not (eql/ident? eid-or-eids)) (sequential? eid-or-eids))
          (api-datomic/pull-many db pattern eid-or-eids)
          (api-datomic/pull db pattern eid-or-eids))
        ;; TODO: Pull known enum ref types from schema
        (replace-ref-types db #{})))
  ([db pattern eid-or-eids transform-fn]
   (let [result (pull-* db pattern eid-or-eids)]
     (if (sequential? result)
       (mapv transform-fn result)
       (transform-fn result)))))
