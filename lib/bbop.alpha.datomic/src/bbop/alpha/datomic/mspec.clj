(ns bbop.alpha.datomic.mspec
  (:require
   [bbop.alpha.core.mspec.protocol :as mspec.protocol])
  (:import (datomic Database)))


(def db-peer
  (mspec.protocol/instance-of Database))


(def db
  db-peer)
