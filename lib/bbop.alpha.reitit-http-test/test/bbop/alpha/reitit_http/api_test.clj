(ns bbop.alpha.reitit-http.api-test
  (:require
   [bbop.alpha.core.system :as system]
   [bbop.alpha.test.api :as test :refer [specification component behavior assertions]]
   [bbop.alpha.reitit-http.api :as reitit-http]
   [bbop.alpha.reitit-http.comp.router :as reitit-http.comp.router]
   [bbop.alpha.reitit-http.comp.ring-handler :as reitit-http.comp.ring-handler]
   [bbop.alpha.http-server.comp.aleph :as http-server.comp.aleph]
   [bbop.alpha.http-client.comp.aleph-http :as http-client.comp.aleph-http]
   [bbop.alpha.http-client.api :as http-client]
   [muuntaja.core :as muuntaja]
   [byte-streams :as bytes]))


(defn check-handler [_]
  {:status 200 :body "OK"})

(defn dice-handler [req]
  (let [env    (reitit-http/route-env req)
        seed   (::random-seed env)
        roll   (get-in req [:query-params :roll])
        result (+ seed roll)]
    {:status 200 :body {:seed seed :roll roll :result result}}))

(defn ping-handler [req]
  {:status 200 :body (select-keys req [:body-params :query-params :form-params :path-params :headers])})

(defn crash-handler [_]
  (throw (ex-info "Crash" {:data {:crash 42}})))


(defn test-routes []
  [["/" {::reitit-http/allowed-accept       [#{:json :transit+json :edn}]
         ::reitit-http/allowed-content-type [#{:json :transit+json :edn}]}

    [["ping" {:post {:handler                ping-handler
                     ::reitit-http/env-specs '[[any?]]}}]

     ["pong" {:post {:handler                           ping-handler
                     ::reitit-http/allowed-accept       [#{:edn}]
                     ::reitit-http/allowed-content-type [#{:edn}]
                     ::reitit-http/env-specs            '[[any?]]}}]

     ["crash" {:get {:handler                crash-handler
                     ::reitit-http/env-specs '[[any?]]}}]

     ["health" {:get {:handler                check-handler
                      ::reitit-http/env-specs '[[any?]]}}]

     ["roll" {:get {:handler dice-handler
                    ::reitit-http/req-specs
                    [[:map [:query-params [:map [:roll pos-int?]]]]]
                    ::reitit-http/env-specs
                    [[:map [::random-seed pos-int?]]]}}]]]])


(defn test-config []
  {:config
   {:cfg/random-seed      42
    :cfg/test-server-port 9876
    :cfg/test-routes      (test-routes)}

   :components
   {:comp/test-router
    {:component reitit-http.comp.router/component
     :deps      {:routes       :cfg/test-routes
                 ::random-seed :cfg/random-seed}}

    :comp/test-handler
    {:component reitit-http.comp.ring-handler/component
     :deps      {:router :comp/test-router}}

    :comp/test-web-server
    {:component http-server.comp.aleph/component
     :deps      {:port    :cfg/test-server-port
                 :handler :comp/test-handler}}

    :comp/http-client
    {:component http-client.comp.aleph-http/component}
    }})


(def m-instance
  (muuntaja/create
   (assoc-in muuntaja/default-options
             [:formats "application/json" :opts :decode-key-fn]
             false)))


(specification "Router"
  (behavior "Content Negotiation"
    (test/with-system [system [(test-config) [:comp/test-handler]]]
      (let [router  (-> (system/state system) :comp/test-router)
            handler (-> (system/state system) :comp/test-handler)]
        (assertions

         "router component"
         router =fn=> some?


         "handler component"
         handler =fn=> fn?


         "request json with empty params and header"
         (->> {:request-method :post
               :uri            "/ping"
               :headers        {"accept"       "application/json"
                                "content-type" "application/json"}}
              (handler)
              :body
              bytes/to-string
              (muuntaja/decode m-instance "application/json"))
         => {"path-params" {}, "query-params" {}, "form-params" {},
             "headers"     {"accept" "application/json", "content-type" "application/json"}}


         "request json with params and header"
         (->> {:request-method :post
               :uri            "/ping"
               :headers        {"accept"       "application/json"
                                "content-type" "application/json"}
               :body           "{\"query\":{\"life\":42}}"}
              (handler)
              :body
              bytes/to-string
              (muuntaja/decode m-instance "application/json"))
         => {"path-params" {}, "query-params" {}, "form-params" {},
             "body-params" {"query" {"life" 42}}
             "headers"     {"accept" "application/json", "content-type" "application/json"}}


         "request edn with params and header"
         (->> {:request-method :post
               :uri            "/ping"
               :headers        {"accept"       "application/edn"
                                "content-type" "application/edn"}
               :body           "{:query {:life 42}}"}
              (handler)
              :body
              bytes/to-string
              (muuntaja/decode m-instance "application/edn"))
         => {:query-params {}, :form-params {}, :path-params {},
             :body-params  {:query {:life 42}},
             :headers      {"accept" "application/edn", "content-type" "application/edn"}}


         "request transit+json with params and header"
         (->> {:request-method :post
               :uri            "/ping"
               :headers        {"accept"       "application/transit+json"
                                "content-type" "application/transit+json"}
               :body           "[\"^ \", \"~:query\", [\"^ \", \"~:life\", 42]]"}
              (handler)
              :body
              bytes/to-string
              (muuntaja/decode m-instance "application/transit+json"))
         => {:query-params {}, :form-params {}, :path-params {},
             :body-params  {:query {:life 42}},
             :headers      {"accept" "application/transit+json", "content-type" "application/transit+json"}}


         "request transit+json with failed decode"
         (->> {:request-method :post
               :uri            "/ping"
               :headers        {"accept"       "application/transit+json"
                                "content-type" "application/transit+json"}
               :body           "[\"^ \" \"~:no\" [\"^ \", \"~:commas\" 42]]"}
              (handler))
         =>  {:status 400, :headers {"Content-Type" "text/plain"}, :body "Malformed \"application/transit+json\" request."}


         "request transit+json with invalid headers"
         (->> {:request-method :post
               :uri            "/ping"
               :headers        {"accept"       "application/invalid"
                                "content-type" "application/invalid"}
               :body           "[\"^ \" \"~:no\" [\"^ \", \"~:commas\" 42]]"}
              (handler))
         =>
         {:status 400, :headers {"Content-Type" "text/plain"}, :body "Unsupported Accept header: <application/invalid>. Allowed: <#{\"application/json\" \"application/transit+json\" \"application/edn\"}>"}


         "missing content-type header"
         (->> {:request-method :post
               :uri            "/ping"
               :headers        {"accept" "application/json"}}
              (handler))
         => {:status  400,
             :headers {"Content-Type" "text/plain"},
             :body    "Unsupported Content-Type header: <>. Allowed: <#{\"application/json\" \"application/transit+json\" \"application/edn\"}>"}


         "missing accept header"
         (->> {:request-method :post
               :uri            "/ping"
               :headers        {"content-type" "application/json"}}
              (handler))
         => {:status  400,
             :headers {"Content-Type" "text/plain"},
             :body    "Unsupported Accept header: <>. Allowed: <#{\"application/json\" \"application/transit+json\" \"application/edn\"}>"}


         "invalid content-type header"
         (->> {:request-method :post
               :uri            "/ping"
               :headers        {"accept"       "application/json"
                                "content-type" "application/invalid"}}
              (handler))
         => {:status  400,
             :headers {"Content-Type" "text/plain"},
             :body    "Unsupported Content-Type header: <application/invalid>. Allowed: <#{\"application/json\" \"application/transit+json\" \"application/edn\"}>"}


         "invalid accept header"
         (->> {:request-method :post
               :uri            "/ping"
               :headers        {"accept"       "application/invalid"
                                "content-type" "application/json"}}
              (handler))
         => {:status  400,
             :headers {"Content-Type" "text/plain"},
             :body    "Unsupported Accept header: <application/invalid>. Allowed: <#{\"application/json\" \"application/transit+json\" \"application/edn\"}>"}


         "override content-type header - valid"
         (->> {:request-method :post
               :uri            "/pong"
               :headers        {"accept"       "application/edn"
                                "content-type" "application/edn"}}
              (handler)
              :status)
         => 200


         "override content-type header - unsupported"
         (->> {:request-method :post
               :uri            "/pong"
               :headers        {"accept"       "application/edn"
                                "content-type" "application/json"}}
              (handler))
         => {:status  400,
             :headers {"Content-Type" "text/plain"},
             :body    "Unsupported Content-Type header: <application/json>. Allowed: <#{\"application/edn\"}>"}


         "override accept header - unsupported"
         (->> {:request-method :post
               :uri            "/pong"
               :headers        {"accept"       "application/json"
                                "content-type" "application/edn"}}
              (handler))
         => {:status  400,
             :headers {"Content-Type" "text/plain"},
             :body    "Unsupported Accept header: <application/json>. Allowed: <#{\"application/edn\"}>"}


         "crash handler"
         (->> {:request-method :get
               :uri            "/crash"
               :headers        {"accept"       "application/edn"
                                "content-type" "application/edn"}}
              (handler))
         => {:status 500,
             :body   {:message   "default",
                      :exception "clojure.lang.ExceptionInfo: Crash {:data {:crash 42}}",
                      :uri       "/crash"}}

         )))))



(specification "Router"
  (component "Basic routing scenarios"
    (test/with-system [system [(test-config) [:comp/test-handler]]]
      (let [router  (-> (system/state system) :comp/test-router)
            handler (-> (system/state system) :comp/test-handler)]
        (assertions

         "request OK"
         (handler {:request-method :get
                   :headers        {"accept"       "application/json"
                                    "content-type" "application/json"}
                   :uri            "/health"})
         => {:status 200 :body "OK"}


         "missing route method"
         (handler {:request-method :post
                   :headers        {"accept"       "application/json"
                                    "content-type" "application/json"}
                   :uri            "/health"})
         => {:status 405 :body "" :headers {}}


         "missing route uri"
         (handler {:request-method :get
                   :headers        {"accept"       "application/json"
                                    "content-type" "application/json"}
                   :uri            "/missing"})
         => {:status 404 :body "" :headers {}}


         "roll-dice"
         ((juxt :status
                (comp bytes/to-string :body))
          (handler {:request-method :get
                    :headers        {"accept"       "application/json"
                                     "content-type" "application/json"}
                    :query-params   {:roll 6}
                    :uri            "/roll"}))
         => [200 "{\"seed\":42,\"roll\":6,\"result\":48}"]


         "roll-dice missing params"
         ((juxt :status
                (comp ;#(re-find #"malli.core/missing-key" %)
                 bytes/to-string :body))
          (handler {:request-method :get
                    :headers        {"accept"       "application/json"
                                     "content-type" "application/json"}
                    :query-params   {}
                    :uri            "/roll"}))
         => [400 "{:query-params {:roll [\"missing required key\"]}}"]


         "roll-dice missing headers"
         ((juxt :status
                (comp #(re-find #"Unsupported Accept header" %)
                      bytes/to-string :body))
          (handler {:request-method :get
                    :query-params   {:roll 6}
                    :uri            "/roll"}))
         => [400 "Unsupported Accept header"]

         )))))


(specification "Web Server"
  (behavior "End to End"
    (test/with-system [system [(test-config) [:comp/test-web-server
                                              :comp/http-client]]]
      (let [router  (-> (system/state system) :comp/test-router)
            handler (-> (system/state system) :comp/test-handler)
            client  (-> (system/state system) :comp/http-client)]
        (assertions

         "components"
         router =fn=> some?
         handler =fn=> fn?
         client =fn=> some?


         "json response"
         ((juxt :status :body)
          (http-client/fetch
           client
           {:url         "http://localhost:9876/ping"
            :method      :post
            :as          :json
            :form-params {:query {:life 42}}}))
         => [200 {:body-params {:query {:life 42}}, :query-params {}, :form-params {}, :path-params {},
                  :headers     {:content-type   "application/json",
                                :accept         "application/json",
                                :content-length "21",
                                :host           "localhost:9876"}}]

         "edn response"
         ((juxt :status :body)
          (http-client/fetch
           client
           {:url         "http://localhost:9876/ping"
            :method      :post
            :as          :edn
            :form-params {:query {:life 42}}}))
         => [200 {:body-params {:query {:life 42}}, :query-params {}, :form-params {}, :path-params {},
                  :headers     {"content-type"   "application/edn",
                                "accept"         "application/edn",
                                "content-length" "19",
                                "host"           "localhost:9876"}}]


         "transit+json response"
         ((juxt :status :body)
          (http-client/fetch
           client
           {:url         "http://localhost:9876/ping"
            :method      :post
            :as          :transit+json
            :form-params {:query {:life 42}}}))
         => [200 {:body-params {:query {:life 42}}, :query-params {}, :form-params {}, :path-params {},
                  :headers     {"content-type"   "application/transit+json",
                                "accept"         "application/transit+json",
                                "content-length" "35",
                                "host"           "localhost:9876"}}]
         )))))
