(ns bbop.alpha.http-client.api
  (:require
   [bbop.alpha.core.guard :refer [>defn =>]]
   [bbop.alpha.http-client.protocol :as protocol]
   [bbop.alpha.http-client.mspec :as mspec.http-client]))


(>defn fetch
  [comp request]
  [mspec.http-client/http-client mspec.http-client/request
   => mspec.http-client/response]
  (protocol/-fetch comp request))
