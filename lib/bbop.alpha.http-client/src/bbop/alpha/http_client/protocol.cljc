(ns ^{:clojure.tools.namespace.repl/unload false
      :clojure.tools.namespace.repl/load   false}
    bbop.alpha.http-client.protocol)


(defprotocol IHttpClient
  (-fetch [this request]))
