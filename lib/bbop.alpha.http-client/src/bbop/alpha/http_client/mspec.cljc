(ns bbop.alpha.http-client.mspec
  (:require
   [bbop.alpha.core.mspec.string :as mspec.string]
   [bbop.alpha.core.mspec.protocol :as mspec.protocol]
   [bbop.alpha.http-client.protocol :as protocol]
   [bbop.alpha.core.malli :as malli]))


(def request
  [:map
   [:url mspec.string/string]
   [:method [:enum :get :post :put]]
   [:as [:enum :transit+json :edn :json]]
   [:form-params {:optional true} any?]])


(def response
  [:map
   [:status pos-int?]
   [:body any?]])


(def http-client
  (mspec.protocol/satisfies protocol/IHttpClient))
