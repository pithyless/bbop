(ns bbop.alpha.fulcro.app
  #?(:cljs (:require-macros [bbop.alpha.fulcro.app]))
  (:require
   [bbop.alpha.core.namespace :refer [import-vars]]
   [bbop.alpha.core.log :as log]
   [com.fulcrologic.fulcro.application]
   [com.fulcrologic.fulcro.components]
   [com.fulcrologic.fulcro.data-fetch]
   [com.fulcrologic.fulcro.mutations]
   [com.fulcrologic.fulcro.routing.dynamic-routing]
   [com.fulcrologic.fulcro.ui-state-machines :as uism]

   #_[com.fulcrologic.fulcro.algorithms.form-state :as fs]
   #_[com.fulcrologic.fulcro.dom :as dom]
   #_[com.fulcrologic.fulcro.algorithms.application-helpers :as ah]
   #_[com.fulcrologic.fulcro.algorithms.misc :as util]
   #_[com.fulcrologic.fulcro.algorithms.merge :as merge]
   #_[com.fulcrologic.fulcro.algorithms.data-targeting :as target]
   #_[com.fulcrologic.fulcro.algorithms.denormalize :as fdn]
   #_[com.fulcrologic.fulcro.algorithms.normalize :as fnorm]
   #_[com.fulcrologic.fulcro.routing.dynamic-router :as dr]
   #_[com.fulcrologic.fulcro.routing.legacy-ui-routers :as r]
   ))

(declare defsc)

#?(:clj (import-vars [com.fulcrologic.fulcro.components
                      defsc
                      ]))

(declare defmutation)

#?(:clj (import-vars [com.fulcrologic.fulcro.mutations
                      defmutation
                      ]))

(declare defrouter)

#?(:clj (import-vars [com.fulcrologic.fulcro.routing.dynamic-routing
                      defrouter
                      ]))

(defn returning [Form]
  {:com.fulcrologic.fulcro.mutations/returning Form})

(declare
  change-route
  path-to
  route-immediate
  route-deferred
  route-target
  router-for-pending-target
  target-ready)

(import-vars [com.fulcrologic.fulcro.routing.dynamic-routing
              change-route
              path-to
              route-immediate
              route-deferred
              route-target
              router-for-pending-target
              target-ready
              ])

(declare load!)

(import-vars [com.fulcrologic.fulcro.data-fetch
              load!
              ])

(declare
  component-options
  component-name
  component-class?
  component-instance?
  component?
  computed
  computed-factory
  external-config
  factory
  get-computed
  get-ident
  get-initial-state
  get-query
  ident
  props
  transact!
  any->app)

(import-vars [com.fulcrologic.fulcro.components
              component-options
              component-name
              component-class?
              component-instance?
              component?
              computed
              computed-factory
              external-config
              factory
              get-computed
              get-ident
              get-initial-state
              get-query
              ident
              props
              transact!
              any->app
              ])

(declare
  current-state
  fulcro-app
  mount!)

(import-vars [com.fulcrologic.fulcro.application
              current-state
              fulcro-app
              mount!
              ])


(declare any->app router-for-pending-target)


(defn any->runtime [x]
  (some-> x
          any->app
          :com.fulcrologic.fulcro.application/runtime-atom
          deref))


(defn uism-target-ready
  "Same as dynamic routing target-ready, but works in UISM via env."
  [{:keys [::uism/state-map] :as env} target]
  (let [router-id (router-for-pending-target state-map target)]
    (if router-id
      (do
        (tap> [::uism-target-ready {:router-id router-id}])
        (uism/trigger env router-id :ready!))
      (do
        (log/error ::uism-target-ready
                   {:msg (str "dr/target-ready! was called but there was no router waiting for the target listed: " target
                              "This could mean you sent one ident, and indicated ready on another.")})
        env))))
