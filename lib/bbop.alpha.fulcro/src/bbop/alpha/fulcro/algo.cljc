(ns bbop.alpha.fulcro.algo
  (:require
   [com.fulcrologic.fulcro.algorithms.denormalize]
   [com.fulcrologic.fulcro.algorithms.normalize]
   [com.fulcrologic.fulcro.algorithms.normalized-state]
   [com.fulcrologic.fulcro.algorithms.react-interop]
   [com.fulcrologic.fulcro.algorithms.tempid]
   [bbop.alpha.core.namespace :refer [import-vars]]))

(declare
  db->tree
  tree->db
  react-factory
  react-input-factory)

(import-vars [com.fulcrologic.fulcro.algorithms.denormalize
              db->tree]

             [com.fulcrologic.fulcro.algorithms.normalize
              tree->db]

             [com.fulcrologic.fulcro.algorithms.react-interop
              react-factory
              react-input-factory])

(declare
  swap!->
  ui->props
  tempid
  tempid?)


#?(:clj (import-vars [com.fulcrologic.fulcro.algorithms.normalized-state
                      swap!->
                      ]))


(import-vars [com.fulcrologic.fulcro.algorithms.normalized-state
              ui->props])


(import-vars [com.fulcrologic.fulcro.algorithms.tempid
              tempid
              tempid?
              ])
