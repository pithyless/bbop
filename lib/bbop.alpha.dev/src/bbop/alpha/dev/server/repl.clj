(ns bbop.alpha.dev.server.repl
  (:require
   #_[io.aviso.logging :as aviso.logging]
   #_[io.aviso.repl :as aviso.repl]
   [bbop.alpha.core.log :as log]
   [bbop.alpha.core.system :as system]
   [bbop.alpha.core.system.repl :as system.repl]
   [clojure.java.classpath :as java.cp]
   [clojure.spec.alpha :as s]
   [clojure.tools.namespace.repl :as c.t.n.repl]
   [expound.alpha :as expound]
   [fipp.repl]
   [shadow.cljs.devtools.server :as shadow-server]
   [unilog.config :as unilog])
  (:import [java.lang Thread$UncaughtExceptionHandler]))


(c.t.n.repl/disable-reload!)


(defn setup-logging []
  (println "Logging...")
  (let [config {:level     :debug
                :overrides {"datomic.process-monitor" :warn
                            "datomic.db"              :warn
                            "datomic.domain"          :warn
                            "io.netty"                :warn
                            "io.undertow"             :warn
                            "org.apache.http"         :warn
                            "org.xnio.nio"            :warn}}]
    (unilog/start-logging! config)))


(defn install-uncaught-exception-handler []
  (Thread/setDefaultUncaughtExceptionHandler
   (reify Thread$UncaughtExceptionHandler
     (uncaughtException [_ _ ex]
       (log/error ::uncaught-exception
                  {:msg (or (.getMessage ex)
                            (-> ex .getClass .getName))
                   ;; :ex ex
                   })))))


(defn setup-errors []
  (println "Errors...")
  (s/check-asserts true)

  ;; https://github.com/bhb/expound/blob/master/doc/faq.md#using-set
  (alter-var-root #'s/*explain-out* (constantly expound/printer))
  (set! s/*explain-out* expound/printer)

  ;;(aviso.repl/install-pretty-exceptions)
  ;;(aviso.logging/install-pretty-logging)
  (install-uncaught-exception-handler))


(defn start-shadow-inspect []
  (println "Starting shadow-server...")
  (shadow-server/start!))


(defn setup-system [config-path profile]
  (println "Loading config:" config-path profile)
  (system/read-config {:filename config-path
                       :profile  profile}))


(defn matches-regexps [fval val coll]
  (->> coll
       (map #(re-find % (fval val)))
       (filter some?)
       first))


;; Public API


(defn init-dev-env []
  (println "Configuring env")
  (setup-errors)
  (setup-logging)
  (start-shadow-inspect)
  :ready)


(defn set-refresh-dirs [{:keys [:excludes]}]
  (let [paths    (java.cp/classpath-directories)
        excluded (if excludes
                   (remove #(matches-regexps str % excludes) paths)
                   paths)]
    (apply c.t.n.repl/set-refresh-dirs excluded)))


(defn init-system-config [config-path]
  (system.repl/set-init-fn #(setup-system config-path :dev)))


(defn start [names {:keys [:success-fn :error-fn]
                    :or   {success-fn identity
                           error-fn   identity}}]
  (let [res (system.repl/start names)]
    (if (:bbop.alpha.core.system.impl/success res)
      (success-fn res)
      (error-fn res))))


(defn stop []
  (system.repl/stop))


(defn state []
  (system.repl/state))


(def pst fipp.repl/pst)
