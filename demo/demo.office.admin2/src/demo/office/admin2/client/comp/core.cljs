(ns demo.office.admin2.client.comp.core
  (:require
   [bbop.alpha.fulcro.app :as app :refer [defsc defrouter]]
   [com.fulcrologic.fulcro.dom :as dom]

   [com.fulcrologic.rad.report :as report :refer [defsc-report]]
   [com.fulcrologic.rad.form :as form :refer [defsc-form]]

   [demo.office.admin2.model.user :as user]

   ;; [bbop.alpha.core.uuid :as uuid]
   ;; [bbop.alpha.rad.radui.api :as radui.api]
   ;; [bbop.alpha.rad.radui.report :as radui.report :refer [defsc-report defsc-report-item]]
   ;; [bbop.alpha.rad.radui.form2.macro :refer [defsc-form]]
   ;; [bbop.alpha.rad.radui.form2.core :as radui.form2]
   ;; [bbop.alpha.rad.radui.rendering.twadmin.api :as twadmin]
   ))

;; (ns com.example.ui
;;   (:require
;;     [clojure.string :as str]
;;     [edn-query-language.core :as eql]
;;     [com.example.model :as model]
;;     [com.example.model.account :as acct]
;;     [com.example.model.address :as address]
;;     [com.example.model.item :as item]
;;     [com.example.model.line-item :as line-item]
;;     [com.example.model.invoice :as invoice]
;;     [com.example.ui.login-dialog :refer [LoginForm]]
;;     [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
;;     [com.fulcrologic.fulcro.algorithms.form-state :as fs]
;;     #?(:clj  [com.fulcrologic.fulcro.dom-server :as dom :refer [div label input]]
;;        :cljs [com.fulcrologic.fulcro.dom :as dom :refer [div label input]])
;;     [com.fulcrologic.fulcro.routing.dynamic-routing :refer [defrouter]]
;;     [com.fulcrologic.rad.authorization :as auth]
;;     [com.fulcrologic.rad.form :as form]
;;     [com.fulcrologic.rad.ids :refer [new-uuid]]
;;     [com.fulcrologic.rad.rendering.semantic-ui.components :refer [ui-wrapped-dropdown]]
;;     [com.fulcrologic.fulcro.routing.dynamic-routing :as dr]
;;     [com.fulcrologic.rad.type-support.decimal :as math]
;;     [com.fulcrologic.rad.type-support.date-time :as datetime]))

;; (form/defsc-form ItemForm [this props]
;;   {::form/id           item/id
;;    ::form/attributes   [item/item-name item/item-description item/item-in-stock item/item-price]
;;    ::form/cancel-route ["landing-page"]
;;    ::form/route-prefix "item"
;;    ::form/title        "Edit Item"})

;; (form/defsc-form AddressForm [this props]
;;   {::form/id                address/id
;;    ::form/attributes        [address/street address/city address/state address/zip]
;;    ::form/enumeration-order {:address/state (sort-by #(get address/states %) (keys address/states))}
;;    ::form/cancel-route      ["landing-page"]
;;    ::form/route-prefix      "address"
;;    ::form/title             "Edit Address"
;;    ::form/layout            [[:address/street]
;;                              [:address/city :address/state :address/zip]]})

;; (def account-validator (fs/make-validator (fn [form field]
;;                                             (case field
;;                                               :account/email (let [prefix (or
;;                                                                             (some-> form
;;                                                                               (get :account/name)
;;                                                                               (str/split #"\s")
;;                                                                               (first)
;;                                                                               (str/lower-case))
;;                                                                             "")]
;;                                                                (str/starts-with? (get form field) prefix))
;;                                               (= :valid (model/all-attribute-validator form field))))))

;; ;; NOTE: Limitation: Each "storage location" requires a form. The ident of the component matches the identity
;; ;; of the item being edited. Thus, if you want to edit things that are related to a given entity, you must create
;; ;; another form entity to stand in for it so that its ident is represented.  This allows us to use proper normalized
;; ;; data in forms when "mixing" server side "entities/tables/documents".
;; (form/defsc-form AccountForm [this props]
;;   {::form/id                  acct/id
;;    ::form/attributes          [acct/name
;;                                ;; Not working completely yet...
;;                                ;;acct/primary-address
;;                                acct/role acct/time-zone acct/email acct/active? acct/addresses]
;;    ::form/default             {:account/active?         true
;;                                ;;:account/primary-address {}
;;                                :account/addresses       [{}]}
;;    ::form/validator           account-validator
;;    ::form/validation-messages {:account/email (fn [_] "Must start with your lower-case first name")}
;;    ::form/cancel-route        ["landing-page"]
;;    ::form/route-prefix        "account"
;;    ::form/title               "Edit Account"
;;    ;; NOTE: any form can be used as a subform, but when you do so you must add addl config here
;;    ;; so that computed props can be sent to the form to modify its layout. Subforms, for example,
;;    ;; don't get top-level controls like "Save" and "Cancel".
;;    ::form/subforms            {#_#_:account/primary-address {::form/ui              AddressForm
;;                                                          ::form/can-delete-row? (fn [parent item] false)
;;                                                          ::form/can-add-row?    (fn [parent] false)}
;;                                :account/addresses       {::form/ui              AddressForm
;;                                                          ::form/can-delete-row? (fn [parent item] (< 1 (count (:account/addresses parent))))
;;                                                          ::form/can-add-row?    (fn [parent] (< (count (:account/addresses parent)) 2))
;;                                                          ::form/add-row-title   "Add Address"
;;                                                          ;; Use computed props to inform subform of its role.
;;                                                          ::form/subform-style   :inline}}})

;; (form/defsc-form LineItemForm [this props]
;;   {::form/id           line-item/id
;;    ::form/attributes   [line-item/item line-item/quantity]
;;    ::form/validator    model/all-attribute-validator
;;    ::form/cancel-route ["landing-page"]
;;    ::form/route-prefix "line-item"
;;    ::form/title        "Line Items"
;;    ::form/layout       [[:line-item/item :line-item/quantity]]
;;    ::form/subforms     {:line-item/item {::form/ui       form/ToOneEntityPicker
;;                                          ::form/pick-one {:options/query-key :item/all-items
;;                                                           :options/subquery  [:item/id :item/name :item/price]
;;                                                           :options/transform (fn [{:item/keys [id name price]}]
;;                                                                                {:text (str name " - " (math/numeric->currency-str price)) :value [:item/id id]})}
;;                                          ::form/label    "Item"}}})

;; (def invoice-validator (fs/make-validator (fn [form field]
;;                                             (let [value (get form field)]
;;                                               (case field
;;                                                 :invoice/customer (eql/ident? value)
;;                                                 :invoice/date (let [now (datetime/now)]
;;                                                                 (and value (< (inst-ms value) (inst-ms now))))
;;                                                 :invoice/line-items (> (count value) 0)
;;                                                 (= :valid (model/all-attribute-validator form field)))))))

;; (form/defsc-form InvoiceForm [this props]
;;   {::form/id           invoice/id
;;    ::form/attributes   [invoice/customer invoice/date invoice/line-items]
;;    ::form/default      {:invoice/date (datetime/now)}
;;    ::form/validator    invoice-validator
;;    ::form/layout       [[:invoice/customer :invoice/date]
;;                         [:invoice/line-items]]
;;    ::form/subforms     {:invoice/customer   {::form/ui            form/ToOneEntityPicker
;;                                              ::form/pick-one      {:options/query-key :account/all-accounts
;;                                                                    :options/subquery  [:account/id :account/name :account/email]
;;                                                                    :options/transform (fn [{:account/keys [id name email]}]
;;                                                                                         {:text (str name ", " email) :value [:account/id id]})}
;;                                              ::form/label         "Customer"
;;                                              ;; Use computed props to inform subform of its role.
;;                                              ::form/subform-style :inline}
;;                         :invoice/line-items {::form/ui              LineItemForm
;;                                              ::form/can-delete-row? (fn [parent item] true)
;;                                              ::form/can-add-row?    (fn [parent] true)
;;                                              ::form/add-row-title   "Add Item"
;;                                              ;; Use computed props to inform subform of its role.
;;                                              ::form/subform-style   :inline}}
;;    ::form/cancel-route ["landing-page"]
;;    ::form/route-prefix "invoice"
;;    ::form/title        "Edit Invoice"})


(defsc-form UserForm [this props]
  {::form/id           user/uid
   ::form/attributes   [user/first-name user/last-name user/email user/gender]
   ::form/cancel-route ["users"]
   ::form/route-prefix "user"
   ::form/title        "Edit User"
   ;; ::form/enumeration-order {:address/state (sort-by #(get address/states %) (keys address/states))}
   ;; ::form/layout       twadmin/form
   ;; ::form/enumeration-order {::address/state (sort-by #(get address/states %) (keys address/states))}
   ;; ::radui.form2/layout       [[::user/first-name ::user/last-name]
   ;;                             [::user/email]]
   })

;; (defsc InvoiceLineItem [this {:keys [:invoice/id :invoice/customer :invoice/line-items] :as props}]
;;   {:query [:invoice/id :invoice/customer :invoice/line-items]
;;    :ident :invoice/id})

;; (def ui-invoice-line-item (comp/factory InvoiceLineItem {:keyfn :invoice/id}))

;; #_(report/defsc-report InvoiceList [this props]
;;     {::report/BodyItem         InvoiceLineItem
;;      ::report/source-attribute :account/all-accounts
;;      ::report/parameters       {:ui/show-inactive? :boolean}
;;      ::report/route            "accounts"})


;; (defsc AccountListItem [this {:account/keys [id name active? last-login] :as props}]
;;   {::report/columns         [:account/name :account/active? :account/last-login]
;;    ::report/column-headings ["Name" "Active?" "Last Login"]
;;    ::report/row-actions     {:delete (fn [this id] (form/delete! this :account/id id))}
;;    ::report/edit-form       AccountForm
;;    :query                   [:account/id :account/name :account/active? :account/last-login]
;;    :ident                   :account/id}
;;   #_(dom/div :.item
;;       (dom/i :.large.github.middle.aligned.icon)
;;       (div :.content
;;         (dom/a :.header {:onClick (fn [] (form/edit! this AccountForm id))} name)
;;         (dom/div :.description
;;           (str (if active? "Active" "Inactive") ". Last logged in " last-login)))))

;; (def ui-account-list-item (comp/factory AccountListItem {:keyfn :account/id}))

;; (report/defsc-report AccountList [this props]
;;   {::report/BodyItem         AccountListItem
;;    ::report/source-attribute :account/all-accounts
;;    ::report/parameters       {:ui/show-inactive? :boolean}
;;    ::report/route            "accounts"})


(defsc UserListItem
  [this {:keys [::user/uid ::user/full-name ::user/email] :as props}]
  {:ident                   ::user/uid
   :query                   [::user/uid ::user/first-name ::user/last-name
                             ::user/full-name ::user/email ::user/gender]
   ::report/columns   [::user/first-name ::user/last-name ::user/email ::user/gender]
   ::report/edit-form UserForm
   ;; ::radui.report/column-options  {::user/email {}}
   ;; ::radui.report/column-headings ["Name" "Active?" "Last Login"]
   ;; ::radui.report/row-actions     {:delete (fn [this id] (form/delete! this ::acct/id id))}
   }
  #_(dom/article {:classes ["bg-white rounded-lg shadow-lg p-4 mx-4 my-2 sm:w-1/4"]}
      (dom/img {:classes ["h-16 w-16 rounded-full mx-auto"]
                :src     (str "https://randomuser.me/api/portraits/women/" (rand-int 30) ".jpg")})
      (dom/div {:classes ["text-center"]}
        (dom/h1 {:classes ["text-lg"]}
        full-name)
        (dom/a {:classes ["cursor-pointer text-blue-700"]
                :onClick (fn [] (radui.api/edit! this :main-controller UserForm uid))}
        email))))


(defsc-report UserList [this props]
  {::report/BodyItem         UserListItem
   ::report/source-attribute ::user/all-users
   ::report/parameters       {:ui/show-inactive? :boolean}
   ::report/route            "users"
   ;;::report/title            "Users"
   ;; ::report/layout           twadmin/report
   }
  #_(dom/div
      (dom/div "Custom UserList")))

(defsc LandingPage [this _]
  {:query         ['*]
   :ident         (fn [] [:component/id ::LandingPage])
   :initial-state {}
   :route-segment ["landing-page"]}
  (dom/div {:classes ["container mx-auto my-4 flex flex-grow flex-col bg-white sm:rounded sm:border shadow overflow-hidden"]}
    (dom/div {:classes ["border-b flex justify-between px-6 -mb-px"]}
      (dom/h3 {:classes ["text-teal-600 py-4 font-normal text-lg"]}
        "Welcome to the new jungle."))))


(defsc AboutPage [this _]
  {:query         ['*]
   :ident         (fn [] [:component/id ::LandingPage])
   :initial-state {}
   :route-segment ["about-us"]}
  (dom/div {:classes ["container mx-auto my-4 flex flex-grow flex-col bg-white sm:rounded sm:border shadow overflow-hidden"]}
    (dom/div {:classes ["border-b flex justify-between px-6 -mb-px"]}
      (dom/h3 {:classes ["text-teal-600 py-4 font-normal text-lg"]}
        "Meet the team."))))

;; (auth/defauthenticator Authenticator {:local LoginForm})

;; (def ui-authenticator (comp/factory Authenticator))


(defrouter MainRouter [_this _props]
  {:router-targets [LandingPage
                    AboutPage
                    UserList
                    UserForm
                    #_ItemForm #_InvoiceForm #_AccountList #_AccountForm]})


(def ui-main-router (app/factory MainRouter))


(defn navigation [this]
  (dom/nav
    {:classes ["flex items-center flex-wrap bg-teal-500 p-6"]}
    (dom/a {:classes ["text-white mr-6"]
            :onClick (fn [] (app/change-route this (app/path-to LandingPage)))} "Go Home")
    (dom/a {:classes ["text-white mr-6"]
            :onClick (fn [] (app/change-route this (app/path-to AboutPage)))} "About Us")
    (dom/a {:classes ["text-white mr-6"]
            :onClick (fn [] (app/change-route this (app/path-to UserList)))} "Users")
    ))


(defsc Root [this {:keys [#_:authenticator :router]}]
  {:query         [#_{:authenticator (app/get-query Authenticator)}
                   {:router (app/get-query MainRouter)}]
   :initial-state {;; :authenticator {}
                   :router {}
                   }}
  (dom/div
    (dom/div
      (navigation this))
    (dom/div
      ;; (ui-authenticator authenticator)
      (ui-main-router router))))


(def ui-root (app/factory Root))


;; (defsc Root [this {::auth/keys [authorization]
;;                    :keys       [authenticator router]}]
;;   {:query         [{:authenticator (comp/get-query Authenticator)}
;;                    {:router (comp/get-query MainRouter)}
;;                    ::auth/authorization]
;;    :initial-state {:router        {}
;;                    :authenticator {}}}
;;   (let [logged-in? (= :success (some-> authorization :local ::auth/status))
;;         username   (some-> authorization :local :account/name)]
;;     (dom/div
;;       (div :.ui.top.menu
;;         (div :.ui.item "Demo")
;;         (when true #_logged-in?
;;           (comp/fragment
;;             (dom/a :.ui.item {:onClick (fn [] (form/edit! this AccountForm (new-uuid 101)))} "My Account")
;;             (dom/a :.ui.item {:onClick (fn [] (form/edit! this ItemForm (new-uuid 200)))} "Some Item")
;;             (dom/a :.ui.item {:onClick (fn [] (form/create! this AccountForm))} "New Account")
;;             (dom/a :.ui.item {:onClick (fn [] (form/create! this InvoiceForm))} "New Invoice")
;;             (dom/a :.ui.item {:onClick (fn [] (form/delete! this :com.example.model.account/id (new-uuid 102)))}
;;               "Delete account 2")
;;             (dom/a :.ui.item {:onClick (fn []
;;                                          (dr/change-route this (dr/path-to AccountList)))} "List Accounts")))
;;         (div :.right.menu
;;           (if logged-in?
;;             (comp/fragment
;;               (div :.ui.item
;;                 (str "Logged in as " username))
;;               (div :.ui.item
;;                 (dom/button :.ui.button {:onClick #(auth/logout! this :local)}
;;                   "Logout")))
;;             (div :.ui.item
;;               (dom/button :.ui.primary.button {:onClick #(auth/authenticate! this :local nil)}
;;                 "Login")))))
;;       (div :.ui.container.segment
;;         (ui-authenticator authenticator)
;;         (ui-main-router router)))))

;; (def ui-root (comp/factory Root))
