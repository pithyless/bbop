(ns demo.office.admin2.client.env
  (:require
   ;; [bbop.alpha.rad.radui.controller :as radui.controller]
   ;; [demo.office.admin.model.user :as user]

   ;; [bbop.alpha.rad.radui.core :as radui]
   ;; [bbop.alpha.rad.attribute :as rad.attr]

   [bbop.alpha.core.log :as log]
   [bbop.alpha.fulcro.app :as app]

   [bbop.alpha.rad.radui.app :as radui.app]

   [demo.office.admin2.client.comp.core :as comp]

   ;; [com.fulcrologic.fulcro.ui-state-machines :as uism :refer [defstatemachine]]

   ;; [bbop.alpha.rad.authorization :as rad.auth]
   ;; [bbop.alpha.rad.attribute :as rad.attr]
   ;; [bbop.alpha.rad.client.controller :as rad.client.controller]
   ;; [demo.office.admin.client.comp.login-dialog :refer [LoginForm]]
   ;; [demo.office.admin.client.ui :refer [MainRouter]]
   ;; [com.fulcrologic.fulcro.ui-state-machines :as uism]
   ))


(defonce app
  (radui.app/rad-app
   {:api-url         "http://localhost:3001/api"
    :fulcro-app-opts {:client-did-mount
                      (fn [app]
                        ;; TODO: auth/start!
                        (app/change-route app (app/path-to comp/LandingPage)))}}))


(comment

  (tap> app)

  )
