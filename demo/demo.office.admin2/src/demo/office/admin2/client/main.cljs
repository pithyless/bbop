(ns demo.office.admin2.client.main
  (:require
   [bbop.alpha.core.log :as log]
   [bbop.alpha.fulcro.app :as app]

   [com.fulcrologic.rad.attributes :as attr]
   [com.fulcrologic.rad.form :as form]
   [com.fulcrologic.rad.rendering.semantic-ui.semantic-ui-controls :as sui]

   [demo.office.admin2.client.env :as env]
   [demo.office.admin2.model :as model]
   [demo.office.admin2.client.comp.core :as comp]))


(defn ^:export refresh []
  (app/mount! env/app comp/Root "app"))


(defn ^:export init []
  (form/install-ui-controls! env/app sui/all-controls)
  (attr/register-attributes! model/all-attributes)
  (app/mount! env/app comp/Root "app"))



(comment

  (tap> :wow)


  (log/info ::root comp/Root)

  (tap> env/app)


  )
