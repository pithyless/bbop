(ns demo.office.admin2.db
  (:require [bbop.alpha.core.aero :as aero]
            [bbop.alpha.rad.database.api :as rad.db]
            [bbop.alpha.core.uuid :as uuid]))


(defn seed-db [rdb dbname seed-file-path]
  (let [data  (aero/read-config seed-file-path :dev)
        users (into []
                    (comp (map-indexed
                           (fn [idx user] (assoc user :demo.office.admin2.model.user/uid
                                                 (uuid/fake-uuid (inc idx)))))
                          (map #(update % :demo.office.admin2.model.user/gender
                                        (fn [kw] (keyword "demo.office.admin2.model.user.gender" (name kw)))))
                          (map #(dissoc % :demo.office.admin2.model.user/avatar-url)))
                    (:users data))]
    (rad.db/transact! rdb dbname users)))
