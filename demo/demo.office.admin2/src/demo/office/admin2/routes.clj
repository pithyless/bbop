(ns demo.office.admin2.routes
  (:require
   [bbop.alpha.rad.routes :as rad.routes]))


(def eql-api
  (rad.routes/eql-api "/api"))
