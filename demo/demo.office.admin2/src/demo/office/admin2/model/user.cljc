(ns demo.office.admin2.model.user
  (:require
   [com.fulcrologic.rad.attributes :as attr :refer [defattr]]
   ;; [com.fulcrologic.rad.authorization :as auth]
   [bbop.alpha.rad.resolver.api :as rad.resolver]
   [bbop.alpha.rad.env :as rad.env]
   [bbop.alpha.rad.database.api :as rad.db]
   ))


(defattr uid ::uid :uuid
  {::rad.db/alias    :primary
   ::attr/identity? true})


(defattr email ::email :string
  {::rad.db/alias   :primary
   ::attr/identity? true})


(defattr first-name ::first-name :string
  {::rad.db/alias :primary})


(defattr last-name ::last-name :string
  {::rad.db/alias :primary})


(defattr gender ::gender :enum
  {::rad.db/alias           :primary
   ::attr/enumerated-values #{:demo.office.admin2.model.user.gender/male
                              :demo.office.admin2.model.user.gender/female
                              :demo.office.admin2.model.user.gender/unspecified}
   })


(def attributes
  [uid
   email
   first-name
   last-name
   gender])


#?(:clj
   (def resolve-user-attrs
     (rad.resolver/db-entity-attr-resolver
      :primary ::uid
      #{::email ::first-name ::last-name})))


#?(:clj
   (def resolve-user-enums
     (rad.resolver/db-entity-enum-resolver
      :primary ::uid #{::gender})))


#?(:clj
   (def resolve-user-via-email
     (rad.resolver/db-entity-attr-resolver
      :primary ::email #{::uid})))


#?(:clj
   (def resolve-user-full-name
     (rad.resolver/compute-attr-resolver
      ::full-name #{::first-name ::last-name}
      (fn [_ {:keys [::first-name ::last-name]}]
        (str first-name " " last-name)))))


#?(:clj
   (def resolve-all-users
     (rad.resolver/compute-attr-resolver
      ::all-users #{}
      (fn [{:keys [::rad.env/rdb]} _]
        (tap> ::resolve-all-users)
        (let [uids (rad.db/query
                    rdb :primary
                    '[:find [?uid ...]
                      :where [_ ::uid ?uid]])]
          (tap> [::resolve-all-users uids])
          (mapv #(hash-map ::uid %) uids))))))


#?(:clj
   (def resolvers
     [resolve-user-attrs
      resolve-user-enums
      resolve-user-via-email
      resolve-user-full-name
      resolve-all-users
      ]))
