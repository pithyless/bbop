(ns demo.office.admin2.model
  (:require
   [demo.office.admin2.model.user :as user]
   [com.fulcrologic.rad.attributes :as attr] ))


(def all-attributes (vec (concat
                          user/attributes)))


#?(:clj
   (def all-resolvers (vec (concat
                            user/resolvers))))


(def all-attribute-validator (attr/make-attribute-validator all-attributes))
