(ns demo.bbop.wksp.model
  (:require
   [demo.bbop.wksp.model.unit :as unit]
   [com.fulcrologic.rad.attributes :as attr]))


(def all-attributes (vec (concat
                          unit/attributes)))


#?(:clj
   (def all-resolvers (vec (concat
                            unit/resolvers))))


(def all-attribute-validator (attr/make-attribute-validator all-attributes))
