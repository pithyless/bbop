(ns demo.bbop.wksp.client.comp.core
  (:require
   [bbop.alpha.fulcro.app :as app :refer [defsc defrouter]]
   [com.fulcrologic.rad.report :as report :refer [defsc-report]]
   [com.fulcrologic.rad.form :as form :refer [defsc-form]]
   [demo.bbop.wksp.model.unit :as unit]
   #?(:clj  [com.fulcrologic.fulcro.dom-server :as dom]
      :cljs [com.fulcrologic.fulcro.dom :as dom])))


(defsc-form UnitForm [this props]
  {::form/id           unit/uid
   ::form/attributes   [unit/string unit/text unit/bool unit/decimal unit/enum unit/long unit/timestamp unit/simple-kw unit/qualified-kw]
   ::form/cancel-route ["units"]
   ::form/route-prefix "unit"
   ::form/title        "Edit Unit"

   ;; ::form/links {}
   ;; ::form/default-values {}
   ;; ::form/validator           account-validator
   ;; ::form/validation-messages {:account/email (fn [_] "Must start with your lower-case first name")}
   })


(defsc-report UnitList [this props]
  {;; required
   ::report/row-pk           unit/uid
   ::report/columns          [unit/uid unit/string unit/text unit/bool unit/decimal unit/enum
                              unit/long unit/timestamp unit/simple-kw unit/qualified-kw unit/parent]
   ::report/source-attribute ::unit/all-units
   ::report/route            "units"

   ;; Optional
   ::report/title        "All Users"
   ;; ::report/layout-style :list
   ;; ::report/row-style    :list
   ::report/run-on-mount?            true
   ::report/run-on-parameter-change? true

   ::report/parameters               {:include-disabled? {:type  :boolean
                                                          :label "Show Inactive Units?"}}
   ::report/initial-parameters       (fn [_] {:include-disabled? true})

   ;; Actions
   ::report/form-links               {unit/string UnitForm}


   ;; ::report/BodyItem                 UnitListItem
   ;; ::report/field-formatters         {:account/name (fn [v] v)}
   ;; ::report/column-headings          {:account/name "Account Name"}
   ;;    ::report/actions                  [{:label  "New Account"
   ;;                                     :action (fn [this] (form/create! this AccountForm))}]
   ;; ::report/row-actions              [{:label     "Enable"
   ;;                                     :action    (fn [report-instance {:account/keys [id]}]
   ;;                                                  (comp/transact! report-instance [(account/set-account-active {:account/id      id
   ;;                                                                                                                :account/active? true})]))
   ;;                                     ;:visible?  (fn [_ row-props] (not (:account/active? row-props)))
   ;;                                     :disabled? (fn [_ row-props] (:account/active? row-props))}
   ;;                                    {:label     "Disable"
   ;;                                     :action    (fn [report-instance {:account/keys [id]}]
   ;;                                                  (comp/transact! report-instance [(account/set-account-active {:account/id      id
   ;;                                                                                                                :account/active? false})]))
   ;;                                     ;:visible?  (fn [_ row-props] (:account/active? row-props))
   ;;                                     :disabled? (fn [_ row-props] (not (:account/active? row-props)))}]
   })


(defsc LandingPage [this _]
  {:query         ['*]
   :ident         (fn [] [:component/id ::LandingPage])
   :initial-state {}
   :route-segment ["landing-page"]}
  (dom/div {:classes ["container mx-auto my-4 flex flex-grow flex-col bg-white sm:rounded sm:border shadow overflow-hidden"]}
    (dom/div {:classes ["border-b flex justify-between px-6 -mb-px"]}
      (dom/h3 {:classes ["text-teal-600 py-4 font-normal text-lg"]}
        "Welcome to the new jungle."))))


(defrouter MainRouter [_this _props]
  {:router-targets [LandingPage
                    UnitList
                    UnitForm]})


(def ui-main-router (app/factory MainRouter))


(defn navigation [this]
  (dom/nav
    {:classes ["flex items-center flex-wrap bg-teal-500 p-6"]}
    (dom/a {:classes ["text-white mr-6"]
            :onClick (fn [] (app/change-route this (app/path-to LandingPage)))}
      "Go Home")
    (dom/a {:classes ["text-white mr-6"]
            :onClick (fn [] (app/change-route this (app/path-to UnitList)))}
      "Units")))


(defsc Root [this {:keys [#_:authenticator :router]}]
  {:query         [#_{:authenticator (app/get-query Authenticator)}
                   {:router (app/get-query MainRouter)}]
   :initial-state {;; :authenticator {}
                   :router {}}}
  (dom/div
    (dom/div
      (navigation this))
    (dom/div
      ;; (ui-authenticator authenticator)
      (ui-main-router router))))


(def ui-root (app/factory Root))
