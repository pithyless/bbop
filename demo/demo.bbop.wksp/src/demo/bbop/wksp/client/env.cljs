(ns demo.bbop.wksp.client.env
  (:require
   [bbop.alpha.fulcro.app :as app]
   [bbop.alpha.rad.radui.app :as radui.app]
   [demo.bbop.wksp.client.comp.core :as comp]))


(defonce app
  (radui.app/rad-app
   {:api-url         "http://localhost:3001/api"
    :fulcro-app-opts {:client-did-mount
                      (fn [app]
                        ;; TODO: auth/start!
                        (app/change-route app (app/path-to comp/LandingPage)))}}))


(comment

  (tap> app)

  )
