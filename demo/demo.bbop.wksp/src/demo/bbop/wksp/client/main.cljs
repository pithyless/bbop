(ns demo.bbop.wksp.client.main
  (:require
   [bbop.alpha.core.log :as log]
   [bbop.alpha.fulcro.app :as app]
   [bbop.alpha.rad.radui.rendering.semantic-ui.controls :as sui]

   [com.fulcrologic.rad.attributes :as attr]
   [com.fulcrologic.rad.form :as form]
   [com.fulcrologic.rad.type-support.date-time :as datetime]
   [com.fulcrologic.rad.application :as rad-app]

   [demo.bbop.wksp.client.comp.core :as comp]
   [demo.bbop.wksp.client.env :as env]
   [demo.bbop.wksp.model :as model]
   ))


(defn ^:export refresh []
  (app/mount! env/app comp/Root "app"))


(defn ^:export init []
  (datetime/set-timezone! "Europe/Warsaw")
  (rad-app/install-ui-controls! env/app sui/all-controls)
  (app/mount! env/app comp/Root "app"))
