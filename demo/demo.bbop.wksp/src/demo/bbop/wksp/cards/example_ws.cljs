(ns demo.bbop.wksp.cards.example-ws
  (:require [nubank.workspaces.card-types.fulcro3 :as ct.fulcro]
            [nubank.workspaces.card-types.react :as ct.react]
            [com.fulcrologic.fulcro.dom :as dom]
            [bbop.alpha.test.api :as test :refer [specification component behavior assertions is]]
            [nubank.workspaces.core :as ws]
            [com.fulcrologic.fulcro.components :as fc]))


(defn element [name props & children]
  (apply js/React.createElement name (clj->js props) children))


(ws/defcard hello-card
  (ct.react/react-card
   (element "div" {} "Hello World")))

;; (ws/deftest sample-test
;;   (is (= 1 1)))

;; (specification "hello"
;;   (assertions
;;    "wat"
;;    2 => 1))
