(ns demo.bbop.wksp.db
  (:require [aero.core :as aero]
            [clojure.java.io :as jio]
            [bbop.alpha.rad.database.api :as rad.db]
            [bbop.alpha.core.uuid :as uuid]))


(defn seed-db [rdb dbname seed-file-path]
  (let [file  (jio/resource seed-file-path)
        data  (aero/read-config file {:profile :dev})
        nodes (into []
                    (map #(assoc % :db/id (str (:demo.bbop.wksp.model.unit/uid %))))
                    (:units data))]
    (rad.db/transact! rdb dbname nodes)))
