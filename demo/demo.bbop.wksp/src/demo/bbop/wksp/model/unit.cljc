(ns demo.bbop.wksp.model.unit
  (:refer-clojure :exclude [long])
  (:require
   [com.fulcrologic.rad.attributes :as attr :refer [defattr]]
   [bbop.alpha.rad.resolver.api :as rad.resolver]
   [bbop.alpha.rad.env :as rad.env]
   [bbop.alpha.rad.database.api :as rad.db]))


(defattr uid ::uid :uuid
  {::rad.db/alias    :primary
   ::attr/identity? true})


(defattr string ::string :string
  {::rad.db/alias   :primary})


(defattr text ::text :text
  {::rad.db/alias :primary})


(defattr bool ::bool :boolean
  {::rad.db/alias :primary})


(defattr decimal ::decimal :decimal
  {::rad.db/alias :primary})


(defattr long ::long :long
  {::rad.db/alias :primary})


(defattr timestamp ::timestamp :instant
  {::rad.db/alias :primary})


(defattr enum ::enum :enum
  {::rad.db/alias :primary
   ::attr/enumerated-values #{:demo.bbop.wksp.model.unit.enum/yes
                              :demo.bbop.wksp.model.unit.enum/no
                              :demo.bbop.wksp.model.unit.enum/maybe}})


(defattr simple-kw ::simple-kw :keyword
  {::rad.db/alias :primary})


(defattr qualified-kw ::qualified-kw :keyword
  {::rad.db/alias :primary})


(defattr parent ::parent :ref
  {::rad.db/alias :primary
   ::attr/target  ::uid})


(def attributes
  [uid
   bool
   decimal
   enum
   long
   qualified-kw
   simple-kw
   string
   text
   timestamp
   parent])


#?(:clj
   (def resolve-unit-attrs
     (rad.resolver/db-entity-attr-resolver
      :primary ::uid
      #{::string ::text ::bool ::decimal ::long ::timestamp
        ::simple-kw ::qualified-kw})))


#?(:clj
   (def resolve-unit-enums
     (rad.resolver/db-entity-enum-resolver
      :primary ::uid #{::enum})))


#?(:clj
   (def resolve-unit-refs
     (rad.resolver/db-entity-ref-resolver
      :primary ::uid {::parent #{::uid}})))


#?(:clj
   (def resolve-all-units
     (rad.resolver/compute-attr-resolver
      ::all-units #{}
      (fn [{:keys [::rad.env/rdb]} _]
        (let [uids (rad.db/query
                    rdb :primary
                    '[:find [?uid ...]
                      :where [_ ::uid ?uid]])]
          (mapv #(hash-map ::uid %) uids))))))


#?(:clj
   (def resolvers
     [resolve-unit-attrs
      resolve-unit-enums
      resolve-unit-refs
      resolve-all-units]))
