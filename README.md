

```
❯ clojure -Sdeps '{:deps {olical/depot {:mvn/version "1.8.4"}}}' -m depot.outdated.main -a versions


❯ clojure -Sdeps '{:deps {org.clojure/tools.deps.graph {:mvn/version "0.2.30"}}}' -m clojure.tools.deps.graph -a versions
```


# bbop

## Dev Setup

### Client

1. Start shadow server

```
make dev-client
```

2. Start main build: http://localhost:9630/build/main

3. Open app: http://localhost:8081/

4. Open `fulcro-inspect` in Chrome DevTools.

5. Connect to Client REPL

In emacs, open buffer `bundle.client/src/user.clj`, connect via `cider-connect`,
then `cider-connect-sibling-clojurescript`, choose `shadow` and `:main` build.

6. Debug via Shadow Inspect

Test REPL by e.g. `(tap> :hello)` in CLJS buffer. Results should be visible in
http://localhost:9630/inspect


### Server

1. Start cider nrepl

```
make dev-server
```

2. Connect to Server REPL

In emacs, open buffer `bundler.server/src/user.clj`, connect via
`cider-connect`, and eval `(repl/blast-off "bundle.dev-server.config.edn")`


3. Debug via Shadow Inspect

This should also start a second instance of shadow-cljs server. If (recommended)
you started frontend first on http://localhost:9630/inspect, then this second
server will be available at http://localhost:9631/inspect

Test the REPL by e.g. `(tap> :hello)` in CLJ buffer. Results should be visible
in http://localhost:9631/inspect


## Running Tests

### TL;DR

The simplest way to make sure everything works, is to run the CI tests:

```
make test-ci
```

### CLJ Watch Mode

Running CLJ tests in the terminal and refreshing automatically on file change.

```
make test-clj-watch
```

### CLJS Watch Mode

Running CLJS tests in the browser and refreshing automatically on file change.

Make sure the shadow server is running:

```
make dev-client
```

Then, make sure the `:test` build is enabled: http://localhost:9630/build/test

Then, open any browser to run the tests: http://localhost:8082/


### CLJ + CLJS CI Mode

```
make test-clj-ci

make test-cljs-ci
```

Or simply run all tests, once:

```
make test-ci
```

## Notes

### Testing Workaround

The deps alias `:test-mods` is a classpath and dependency hack to allow kaocha
(CLJ) and shadow/karma (CLJS) run all tests found in `lib/*` without needing to
keep 3 different configuration in sync (location of src, tests, and deps). This
may slow down startup speed and file watchers (especially during first refresh),
but the upside is you get test autodetection for free. :shrug:

### Yarn Lock Error

If you see an error:

```
yarn install v1.16.0
[1/4] 🔍  Resolving packages...
error Your lockfile needs to be updated, but yarn was run with `--frozen-lockfile`.
```

Yarn has detected that `yarn.lock` has changed and cannot continue.
Probably, you've changed a dependency in `package.json`. If so:

1. Run `make yarn-update-deps`.
2. Commit the new `yarn.lock` in a separate commit named `Update yarn.lock`
