SHELL=/bin/bash

CLJ := CLJ_CONFIG='../../' clojure -Sforce
YARN := CLJ_CONFIG='../../' yarn
CLJ_TEST := $(CLJ) -A:test-ci:run-test-ci

clean:
		rm -rf bundle/*/.cpcache && \
		rm -rf bundle/*/.shadow-cljs && \
		rm -rf bundle/*/node_modules && \
		rm -rf lib/*/.cpcache && \
		rm -rf .clj-kondo/.cache && \
		rm -rf bundle/*/resources/*/*/public/js && \
		rm -rf bundle/*/resources/*/*/public/css


dev-alpha: bbop-modules.edn
	./update-deps

dev-alpha-server: dev-alpha
	cd bundle/bundle.alpha && \
		$(CLJ) -A:cider:run-cider


dev-alpha-client: dev-alpha
	cd bundle/bundle.alpha && \
		yarn install --frozen-lockfile && \
		$(YARN) develop


dev-alpha-update-client-deps: dev-alpha
	cd bundle/bundle.alpha && \
		$(YARN) install


test-clj-watch: dev-alpha
	cd bundle/bundle.kaocha-tests && \
		$(CLJ) -A:kaocha:run-test-kaocha --config-file resources/bundle.kaocha-tests/tests.edn --watch


test-ci-clj:
			cd lib/bbop.alpha.core-test && \
				$(CLJ_TEST) && \
			cd ../bbop.alpha.datomic-conn-test && \
				$(CLJ_TEST) && \
			cd ../bbop.alpha.rad-test && \
				$(CLJ_TEST) && \
			cd ../bbop.alpha.reitit-http-test && \
				$(CLJ_TEST)


test-ci-cljs:
		cd bundle/bundle.alpha && \
			yarn install --frozen-lockfile && \
		 $(YARN) test:ci


test-ci: test-ci-cljs test-ci-clj
