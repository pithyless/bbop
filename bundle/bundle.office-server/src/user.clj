(ns user
  (:require
   [com.wsscode.pathom.core] ;; TODO - temporarily need this, because of a loading order issue
   [com.wsscode.pathom.connect] ;; TODO - temporarily need this, because of a loading order issue

   [clojure.tools.namespace.repl :as c.t.n.repl]
   [bbop.alpha.core.contrib :as contrib]
   [bbop.alpha.dev.server.repl :as server.repl]))


(c.t.n.repl/disable-reload!)


(comment

  (do (server.repl/init-dev-env)
      (server.repl/set-refresh-dirs {:excludes [#"/.gitlibs/"]})
      (server.repl/init-system-config "bundle.office-server/config.edn")
      (println "ready")
      :ok)


  (let [components [:comp/rad-web-server]]
    (server.repl/start
     components
     {:success-fn #(do (contrib/fipp %) :ok)
      :error-fn   #(do (contrib/fipp %) :error)}))


  (do (tap> (server.repl/state))
      (keys (server.repl/state)))


  (server.repl/stop)


  (server.repl/pst)

  )
