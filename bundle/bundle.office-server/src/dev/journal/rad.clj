(ns dev.journal.rad
  (:require
   [bbop.alpha.core.contrib :as contrib]
   [bbop.alpha.dev.server.repl :as server.repl]
   [bbop.alpha.rad.database.api :as rad.db]
   [bbop.alpha.core.uuid :as uuid]
   [demo.office.admin2.db :as office.admin2.db]
   ))



(comment

  (do (server.repl/init-dev-env)
      (server.repl/set-refresh-dirs {:excludes [#"/.gitlibs/"]})
      (server.repl/init-system-config "bundle.office-server/config.edn")
      :ok)


  (server.repl/stop)


  (let [components [:comp/rad-web-server]]
    (server.repl/start
     components
     {:success-fn #(do (contrib/fipp %) :ok)
      :error-fn   #(do (contrib/fipp %) :error)}))


  (do (tap> [::state (server.repl/state)])
      (keys (server.repl/state)))


  (def rdb
    (-> (server.repl/state)
        :comp/rad-db-conn
        rad.db/connect-all))


  (office.admin2.db/seed-db rdb :primary "demo.office.admin2/seed-db.edn")


  (def rdb2
    (-> (server.repl/state)
        :comp/rad-db-conn
        rad.db/connect-all))


  (rad.db/pull-by-attr
   rdb :primary :demo.office.admin.model.user/uid (uuid/fake-uuid 1)
   [:demo.office.admin.model.user/first-name
    ;;:demo.office.admin.model.user/gender
    {:demo.office.admin.model.user/gender [:db/ident]}])

  (rad.db/pull-by-attr
   rdb :primary :demo.office.admin.model.user/uid (uuid/fake-uuid 1)
   [:demo.office.admin.model.user/first-name
    ;;:demo.office.admin.model.user/gender
    :demo.office.admin.model.user/gender])


  (rad.db/pull-by-attr
   rdb2 :primary :demo.office.admin.model.user/uid (uuid/fake-uuid 1)
   [:demo.office.admin.model.user/first-name])


  (rad.db/transact!
   rdb :primary
   [{:demo.office.admin.model.user/uid        (uuid/fake-uuid 1)
     :demo.office.admin.model.user/first-name "Jackie"}])


  ;; EQL:

  [{[:demo.office.admin.model.user/uid
     #uuid "ffffffff-ffff-ffff-ffff-000000000001"]
    [:demo.office.admin.model.user/first-name
     :demo.office.admin.model.user/full-name]}]


  [{[:demo.office.admin.model.user/email
     "pam@dm.com"]
    [:demo.office.admin.model.user/first-name
     :demo.office.admin.model.user/email
     :demo.office.admin.model.user/full-name]}]

  )
