(ns bbop.kaocha.plugin.cleanup
  (:require
   [kaocha.plugin :refer [defplugin]]
   [bbop.alpha.test.api :as test.api]))


(defn pre-run [test-plan]
  (test.api/pre-test-runs)
  test-plan)


(defplugin bbop.kaocha.plugin/cleanup
  (pre-run [test-plan]
           (pre-run test-plan)))
