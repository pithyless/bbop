module.exports = {
    theme: {
        extend: {
            textColor: {
                'grey-dark': '#8795a1',
                'primary': '#3490dc',
                'secondary': '#ffed4a',
                'danger': '#e3342f',
            }
        }
    },
    variants: {},
    plugins: [
        require('tailwind-forms')()
    ]
};
