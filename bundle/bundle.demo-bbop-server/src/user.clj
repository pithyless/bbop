(ns user
  (:require
   [com.wsscode.pathom.core] ;; TODO - temporarily need this, because of a loading order issue
   [com.wsscode.pathom.connect] ;; TODO - temporarily need this, because of a loading order issue

   [clojure.tools.namespace.repl :as c.t.n.repl]
   [bbop.alpha.core.contrib :as contrib]
   [bbop.alpha.dev.server.repl :as server.repl]

   [bbop.alpha.rad.database.api :as rad.db]
   [demo.bbop.wksp.db :as wksp.db]))


(c.t.n.repl/disable-reload!)


(comment

  (do (server.repl/init-dev-env)
      (server.repl/set-refresh-dirs {:excludes [#"/.gitlibs/"]})
      (server.repl/init-system-config "bundle.demo-bbop-server/config.edn")
      (println "ready")
      :ok)


  (let [components [:comp/rad-web-server]]
    (server.repl/start
     components
     {:success-fn #(do (contrib/fipp %) :ok)
      :error-fn   #(do (contrib/fipp %) :error)}))


  (do (tap> (server.repl/state))
      (keys (server.repl/state)))


  (server.repl/stop)


  (def rdb
    (let [rdb (-> (server.repl/state)
                  :comp/rad-db-conn
                  rad.db/connect-all)]
      ;; (rad.db/migrate-all! rdb demo.bbop.wksp.model/all-attributes)
      (wksp.db/seed-db rdb :primary "demo.bbop.wksp/seed-db.edn")
      rdb))



  )
