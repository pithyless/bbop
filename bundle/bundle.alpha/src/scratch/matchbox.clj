(ns scratch.matchbox
  (:require [clara.rules :as clara :refer [defrule defquery]]
            [clarax.rules :as clarax]
            [clara.rules.accumulators :as clara.acc]
            [bbop.alpha.core.contrib :as contrib]
            [clarax.macros-java :refer [->session]]))


(defrecord UserEvent [event-id event])

(defrecord QueuedEvent [event-id])

(defrecord NextQueuedEvent [event-id])


;; (defrecord HandlingUserEvent [event-id])

;; (defrecord NLUBelief [event-id kind value])


(defn new-session []
  (-> {:get-all-user-events
       (fn []
         (let [events [UserEvent]]
           (mapv :event events)))

       :get-all-queued-events
       (fn []
         (let [events [QueuedEvent]]
           events))

       :get-next-queued-event
       (fn []
         (let [event NextQueuedEvent]
           event))

       :set-next-queued-event
       (let [event [QueuedEvent (clara.acc/min :event-id :returns-fact true)]]
         (clara/insert! (->NextQueuedEvent (:event-id event))))
       }
      (->session)))


(defn status [session]
  (into {}
        (map #(vector % (clara/query session %)))
        [:get-all-user-events
         :get-all-queued-events
         :get-next-queued-event]))


(comment

  (let [s0 (-> (new-session))
        s1 (-> s0
               (clara/insert (->UserEvent 1 {:id 1 :msg "elo"})
                             (->UserEvent 2 {:id 2 :msg "time?"}))
               (clara/fire-rules))
        s2 (-> s1
               (clara/insert (->QueuedEvent 1)
                             (->QueuedEvent 2))
               (clara/fire-rules))
        s3 (-> s2
               (clara/retract (->QueuedEvent 1))
               (clara/fire-rules))
        ]

    (doseq [[idx session] (zipmap (range) [s0 s1 s2 s3])]
      (println (str"\n\n" "Session " idx))
      (contrib/fipp (status session))))


  )
