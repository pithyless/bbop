(ns scratch.malli
  (:require
   [malli.core :as m]
   [clojure.spec.alpha :as s]
   [bbop.alpha.core.malli :as malli]))


(comment

  (malli/generate [:fn {:gen/gen (s/gen inst?)} inst?])


  (defn fn-prop-schema [name f]
    (#'m/-leaf-schema
     name
     (fn [properties children _]
       (when (seq children)
         (m/fail! ::child-error {:name name, :properties properties, :children children, :min 0, :max 0}))
       [(partial f properties) children])))


  (defn my-int? [props value]
    (prn props)
    ;; (cond
    ;;   (:pos? props) (> value 0))

    (int? value))


  (def registry
    (merge
     m/class-registry
     m/comparator-registry
     m/base-registry
     {:int    (m/fn-schema :int int?)
      :int2   (fn-prop-schema :int2 my-int?)
      :string (m/fn-schema :string string?)}))

  (m/validate [:int2 {:pos? true}]  3234  {:registry registry})


  (malli/generate [:int] {:registry registry})


  (m/fn-schema :int int?)

  (m/into-schema :int {} [])


  (merge {})

  m/base-registry


  )
