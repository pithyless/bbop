(ns user
  (:require
   [bbop.alpha.kitchensink.preload]

   [clojure.tools.namespace.repl :as c.t.n.repl]
   [bbop.alpha.core.contrib :as contrib]
   [bbop.alpha.dev.server.repl :as server.repl]
   [bbop.alpha.core.malli :as malli]

   [demo.office.admin2.db :as office.admin2.db]
   [demo.bbop.wksp.db :as demo.wksp.db]
   [bbop.alpha.rad.database.api :as rad.db]))



(c.t.n.repl/disable-reload!)


(defn init-env [config-path]
  (server.repl/init-dev-env)
  (server.repl/set-refresh-dirs {:excludes [#"/.gitlibs/"]})
  (server.repl/init-system-config config-path)
  (println "ready")
  :ok)


(defn get-rdb []
  (some-> (server.repl/state)
          :comp/rad-db-conn
          rad.db/connect-all))


(comment

  (init-env "bundle.alpha/server/office-config.edn")


  (init-env "bundle.alpha/server/wksp-config.edn")


  (let [components [:comp/rad-web-server]]
    (server.repl/start
     components
     {:success-fn #(do (contrib/fipp %) :ok)
      :error-fn   #(do (contrib/fipp %) :error)}))


  (do (tap> (server.repl/state))
      (keys (server.repl/state)))


  (some-> (get-rdb)
          (office.admin2.db/seed-db :primary "demo.office.admin2/seed-db.edn"))


  (some-> (get-rdb)
          (demo.wksp.db/seed-db :primary "demo.bbop.wksp/seed-db.edn"))


  (server.repl/stop)


  (server.repl/pst)


  )
